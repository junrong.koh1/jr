# cloudmile-react-components

## Design
We use [Figma](https://www.figma.com/file/bb6myvo8JBf0FnLroo2eQm/UI-Component-Library?node-id=0%3A1) to cooperate with designers, ask Bevis or Sofia if you don't have access permission.

## Usage

### Install
This private package can be installed via yarn, but make sure you have SSH right to access this repo:
```
yarn add git+ssh://git@gitlab.com:cloudmile/cloudmile-react-components.git#0.0.1 --save
```
### Update
```
yarn upgrade cloudmile-react-components
```

### Automation CI/CD
- If you want to use user name and password when download the private package, the commands are below:

    - Commands
    ```shell
    git config --global url."https://${GIT_USER}:${GIT_PASSWORD}@gitlab.com".insteadOf "https://gitlab.com"
    yarn install
    ```

    - ${GIT_USER} means the user name in GitLab Deploy Tokens
    - ${GIT_PASSWORD} means the token in GitLab Deploy Tokens

- If you want to use ssh key when download the private package, the commands are below:

    - Commands
    ```shell
    mkdir -p ~/.ssh
    echo -e "StrictHostKeyChecking no\nUserKnownHostsFile /dev/null\n" > ~/.ssh/config
    echo -e ${GIT_SSH_KEY} > ~/.ssh/id_rsa
    chmod 400 ~/.ssh/id_rsa
    yarn install
    ```

    - ${GIT_SSH_KEY} means the deploy ssh key in GitLab Deploy Keys

- [Reference](https://gitlab.com/cloudmile_products/milelync-components/-/settings/repository)

### Components usage
#### Datepicker and DateRangePicker
Add `import 'react-datepicker/dist/react-datepicker.css'` in your _app.js if you use Nextjs project.

## Develop
### Run local story book
```
yarn storybook
```
### Deploy to public site
Create git tag, then gitlab-ci will run deploy process automatically. public [Component Website](https://cloudmile-components.mile.cloud/) will be updated.

### Build
```
yarn build
```
