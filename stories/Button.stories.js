import React from 'react'
import { action } from '@storybook/addon-actions'
import { object, select, boolean, text } from '@storybook/addon-knobs'
import Button from '../src/components/Button'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: Button,
    title: 'Button',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `More usage detail can be refered to https://material-ui.com/api/button/ `,
            source: false,
        },
    },
};

export const actionsData = {
    onClick: action('onClick'),
};

export const Default = () => (
    <Button
        theme={createMuiTheme(object('theme', theme))}
        variant={select('variant', ['text', 'outlined', 'contained'], 'contained')}
        color={select('color', ['primary', 'secondary'], 'primary')}
        label={text('label', "Click me")}
        loading={boolean('loading', false)}
        disabled={boolean('disabled', false)}
        onClick={actionsData.onClick}
    />
)