import React, { useState } from 'react'
import { action } from '@storybook/addon-actions'
import { boolean, text, object } from '@storybook/addon-knobs'
import DateRangePicker from '../src/components/DateRangePicker'
import { createMuiTheme, makeStyles } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: DateRangePicker,
    title: 'DateRangePicker',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: 'This component is made by DatePicker component.',
            source: false,
            propTables: [DateRangePicker]
        },
    },
};

export const actionsData = {
    startDateChange: action('startDateChange'),
    endDateChange: action('endDateChange'),
}

const DefaultPicker = () => {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    return (
        <DateRangePicker
            theme={createMuiTheme(object('theme', theme))}
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
        />
    )
}

const useStyles = makeStyles({
    input: {
        width: 130,
        minWidth: 'unset',
        '& input': {
            backgroundColor: 'lightPink'
        }
    }
})

const CustomInputStyle = () => {
    const classes = useStyles()
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    return (
        <DateRangePicker
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
            inputClass={classes.input}
        />
    )
}

const WithTimePeriod = () => {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    return (
        <DateRangePicker
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
            withTimePeriod
        />
    )
}

const CustomizedTimePeriodAndLocale = () => {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    return (
        <DateRangePicker
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
            withTimePeriod
            customTimePeriodLabel={{
                last7days: '過去7天',
                last30days: '過去30天',
                lastmonth: '上個月',
                thismonth: '這個月'
            }}
            locale='zh-TW'
        />
    )
}

const WithTimeSelection = () => {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)

    return (
        <DateRangePicker
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
            withTimePeriod
            showTimeSelect
            dateFormat='yyyy/MM/dd, h:mm aa'
        />
    )
}

const WithTimeSelectionAndLocale = () => {
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)

    return (
        <DateRangePicker
            startDate={startDate}
            endDate={endDate}
            startDateChange={(date) => {
                setStartDate(date)
                actionsData.startDateChange(date)
            }}
            endDateChange={(date) => {
                setEndDate(date)
                actionsData.endDateChange(date)
            }}
            withTimePeriod
            showTimeSelect
            dateFormat='yyyy/MM/dd, aa h:mm'
            timeCaption='時間'
            locale='zh-TW'
            customTimePeriodLabel={{
                last5minutes: '過去 5 分鐘',
                last15minutes: '過去 15 分鐘',
                last1hours: '過去 1 小時',
                last12hours: '過去 12 小時',
                last24hours: '過去 24 小時',
                last7days: '過去 7 天'
            }}
        />
    )
}

export const Default = () => <DefaultPicker />
export const WithCustomInputStyle = () => <CustomInputStyle />
export const withTimePeriod = () => <WithTimePeriod />
export const customizedTimePeriodAndLocale = () => <CustomizedTimePeriodAndLocale />
export const withTimeSelection = () => <WithTimeSelection />
export const withTimeSelectionAndLocale = () => <WithTimeSelectionAndLocale />