import React, { useState } from 'react'
import { action } from '@storybook/addon-actions'
import { boolean, text, object } from '@storybook/addon-knobs'
import DatePicker from '../src/components/DatePicker'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: DatePicker,
    title: 'DatePicker',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `
            More usage detail can be refered to https://reactdatepicker.com/.  
            #### Locale  
            Default support **[en, zh-TW]**, If you want to setup more locale,  refered to https://github.com/Hacker0x01/react-datepicker#localization.
            `,
            source: false,
            propTables: [DatePicker]
        },
    },
};

export const actionsData = {
    onChange: action('onChange'),
}

const DEFAULT_SELECTED = new Date('2020/05/28')

const DefaultPicker = () => {
    const [selectedDate, setSelectedDate] = useState(DEFAULT_SELECTED)
    return (
        <DatePicker
            // textInputProps={object('textInputProps', {
            //     helperText: '',
            //     error: false
            // })}
            label={text('label', 'Label')}
            theme={createMuiTheme(object('theme', theme))}
            selected={selectedDate}
            dateFormat={text('dateFormat', 'MMMM d, yyyy')}
            onChange={date => {
                setSelectedDate(date)
                actionsData.onChange(date)
            }}
            showMonthYearPicker={boolean('showMonthYearPicker', false)}
            showQuarterYearPicker={boolean('showQuarterYearPicker', false)}
            showYearPicker={boolean('showYearPicker', false)}
            isClearable={boolean('isClearable', false)}
        />
    )
}

const WithDateLimitation = () => {
    const [selectedDate, setSelectedDate] = useState(DEFAULT_SELECTED)
    return (
        <DatePicker
            selected={selectedDate}
            onChange={date => {
                setSelectedDate(date)
                actionsData.onChange(date)
            }}
            maxDate={DEFAULT_SELECTED.endOf('month')}
            minDate={DEFAULT_SELECTED.startOf('month')}
        />
    )
}

const WithDateRange = () => {
    const [startDate, setStartDate] = useState(DEFAULT_SELECTED.startOf('month'))
    const [endDate, setEndDate] = useState(DEFAULT_SELECTED.endOf('month'))
    return (
        <DatePicker
            selectsStart
            selected={startDate}
            onChange={date => {
                setStartDate(date)
                actionsData.onChange(date)
            }}
            startDate={startDate}
            endDate={endDate}
            maxDate={DEFAULT_SELECTED.endOf('month')}
            minDate={DEFAULT_SELECTED.startOf('month')}
        />
    )
}

export const Default = () => <DefaultPicker />
export const withDateLimitation = () => <WithDateLimitation />
export const withDateRange = () => <WithDateRange />


