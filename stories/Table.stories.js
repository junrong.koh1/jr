import React, { useState, useEffect } from 'react'
import { action } from '@storybook/addon-actions'
import { withKnobs, number, object, boolean, text, array } from '@storybook/addon-knobs'
import Table from '../src/components/Table'
import TableCell from '../src/components/Table/TableCell'
import Pagination from '../src/components/Pagination'
import { createMuiTheme, makeStyles } from '@material-ui/core/styles'
import theme from './theme'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default {
    component: Table,
    title: 'Table',
    excludeStories: /.*Data$/,
    decorators: [withKnobs],
    parameters: {
        info: {
            text: `More usage detail can be refered to https://material-ui.com/components/tables/#basic-table`,
            source: false,
        },
    },
};

export const actionsData = {
    onCheckItem: action('onCheckItem'),
    onFilterA: action('onFilterA'),
    onFilterB: action('onFilterB'),
    onSortA: action('onSortA'),
    onSortB: action('onSortB'),
    onSortC: action('onSortC'),
}

const HEADERS = [
    { title: 'header A' },
    { title: 'header B' },
    { title: 'header C' }
]
const DATALIST = [
    { A: 1, B: 'A', C: 'item 1' },
    { A: 2, B: 'B', C: 'item 2' },
    { A: 3, B: 'C', C: 'item 3' },
    { A: 4, B: 'D', C: 'item 4' },
    { A: 5, B: 'E', C: 'item 5' },
    { A: 6, B: 'F', C: 'item 6' }
]

export const Default = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        pageLength={number('pageLength', 3)}
        currentPage={number('currentPage', 1)}
        totalDataCount={number('totalDataCount', 6)}
        renderCells={(item) => (
            <>
                <TableCell >
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
    />
)

export const WithReadmoreCallback = () => {
    const [datalist, setDatalist] = useState(DATALIST)
    const FAKE_NEW_DATA = (id) => ({
        A: id,
        B: String.fromCharCode(64 + id),
        C: `item ${id}`
    })

    return (
        <Table
            theme={createMuiTheme(object('theme', theme))}
            headers={HEADERS}
            datalist={datalist}
            pageLength={number('pageLength', 3)}
            totalDataCount={number('totalDataCount', 20)}
            renderCells={(item) => (
                <>
                    <TableCell >
                        {item.A}
                    </TableCell>
                    <TableCell >
                        {item.B}
                    </TableCell>
                    <TableCell >
                        {item.C}
                    </TableCell>
                </>
            )}
            readMoreData={(limit, offset) => {
                const newList = Array.from(Array(limit).keys())
                    .map(id => FAKE_NEW_DATA(id + offset + 1))
                setTimeout(() => {
                    setDatalist([
                        ...datalist,
                        ...newList
                    ])
                }, 500)

            }}
        />
    )
}

export const WithCustomizedPagination = () => {
    const [pageLength, setPageLength] = useState(2)
    const [currentPage, setCurrentPage] = useState(1)
    const totalPage = Math.ceil(DATALIST.length / pageLength)

    return (
        <Table
            theme={createMuiTheme(object('theme', theme))}
            headers={HEADERS}
            datalist={DATALIST}
            pageLength={pageLength}
            currentPage={currentPage}
            renderCells={(item) => (
                <>
                    <TableCell >
                        {item.A}
                    </TableCell>
                    <TableCell >
                        {item.B}
                    </TableCell>
                    <TableCell >
                        {item.C}
                    </TableCell>
                </>
            )}
            onChangePage={(page) => {
                setCurrentPage(page)
            }}
            paginatation={<Pagination
                forcePage={currentPage - 1}
                pageCount={totalPage}
                rowsPerPage={[2, 3, 4]}
                onRowsPerPageChange={(rows) => {
                    setPageLength(rows)
                }}
                onPageChange={(data) => {
                    const page = data.selected + 1
                    setCurrentPage(page)
                }}
            />}
        />
    )
}

const customStyles = makeStyles({
    page: {
        borderColor: 'orange'
    },
    pageLink: {
        color: 'orange'
    },
    activePage: {
        backgroundColor: 'orange'
    }
})

export const WithPaginationProps = () => {
    const classes = customStyles()
    const [pageLength, setPageLength] = useState(2)
    const [currentPage, setCurrentPage] = useState(1)
    const totalPage = Math.ceil(DATALIST.length / pageLength)

    return (
        <Table
            theme={createMuiTheme(object('theme', theme))}
            headers={HEADERS}
            datalist={DATALIST}
            pageLength={pageLength}
            currentPage={currentPage}
            renderCells={(item) => (
                <>
                    <TableCell >
                        {item.A}
                    </TableCell>
                    <TableCell >
                        {item.B}
                    </TableCell>
                    <TableCell >
                        {item.C}
                    </TableCell>
                </>
            )}
            onChangePage={(page) => {
                setCurrentPage(page)
            }}
            paginationProps={{
                elementClasses: {
                    page: classes.page,
                    pageLink: classes.pageLink,
                    activePage: classes.activePage,
                    previous: classes.page,
                    next: classes.page
                },
                previousLabel: <FontAwesomeIcon icon={faChevronLeft} />,
                nextLabel: <FontAwesomeIcon icon={faChevronRight} />
            }}
        />
    )
}

export const WithInfiniteScroll = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        infiniteScrollHeight={200}
        renderCells={(item) => (
            <>
                <TableCell>
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
    />
)

export const WithFixedFirstColumn = () => {
    const FAKE_NEW_DATA = (id) => ({
        id,
        A: `A ${id}`,
        B: `B ${id}`,
        C: `C ${id}`,
        D: `D ${id}`,
        E: `E ${id}`,
        F: `F ${id}`,
        G: `G ${id}`,
        H: `H ${id}`,
        I: `I ${id}`,
        J: `J ${id}`,
        K: `K ${id}`,
        L: `L ${id}`,
        M: `M ${id}`,
        N: `N ${id}`,
        O: `O ${id}`,
        P: `P ${id}`,
        Q: `Q ${id}`,
        R: `R ${id}`,
        S: `S ${id}`,
        T: `T ${id}`,
    })
    const totalDataCount = 20
    const dataList = Array.from(Array(totalDataCount).keys())
        .map(id => FAKE_NEW_DATA(id + 1))
    const headers = Object.keys(dataList[0]).filter(k => k != 'id')
        .map((head, id) => ({
            key: head,
            title: `header ${head}`,
            sticky: (id <= 0),
            style: { minWidth: 100 }
        }))

    const renderRow = (head, value) => {
        return (
            <TableCell key={head.key} sticky={head.sticky}>
                {value}
            </TableCell>
        )
    }

    return (
        <Table
            theme={createMuiTheme(object('theme', theme))}
            pageLength={10}
            headers={headers}
            datalist={dataList}
            // infiniteScrollHeight={200}
            renderCells={(item) => (
                <>
                    {
                        headers.map(h => renderRow(h, item[h.key]))
                    }
                </>
            )}
        />
    )
}

export const Empty = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={[]}
        emptyMessage={text('emptyMessage', 'No data')}
        renderCells={(item) => null}
    />
)

export const WithFilters = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        pageLength={number('pageLength', 3)}
        currentPage={number('currentPage', 1)}
        totalDataCount={number('totalDataCount', 6)}
        renderCells={(item) => (
            <>
                <TableCell >
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
        filters={[
            {
                onFilter: actionsData.onFilterA,
                options: [
                    {
                        selected: true,
                        label: 'Cell A1',
                        value: 'A1'
                    },
                    {
                        selected: true,
                        label: 'Cell A2',
                        value: 'A2'
                    },
                    {
                        selected: false,
                        label: 'Cell A3',
                        value: 'A3'
                    }
                ]
            },
            {
                onFilter: actionsData.onFilterB,
                options: [
                    {
                        label: 'Cell B1',
                        value: 'B1'
                    },
                    {
                        label: 'Cell B2',
                        value: 'B2'
                    },
                    {
                        label: 'Cell B3',
                        value: 'B3'
                    }
                ]
            },
        ]}
    />
)

export const WithSorting = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        pageLength={number('pageLength', 3)}
        currentPage={number('currentPage', 1)}
        totalDataCount={number('totalDataCount', 6)}
        renderCells={(item) => (
            <>
                <TableCell >
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
        sorters={[
            {
                onSort: actionsData.onSortA,
                order: ''
            },
            {
                onSort: actionsData.onSortB,
                order: 'asc'
            },
            {
                onSort: actionsData.onSortC,
                order: 'desc'
            }
        ]}
    />
)

export const WithFilterAndSorting = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        pageLength={number('pageLength', 3)}
        currentPage={number('currentPage', 1)}
        totalDataCount={number('totalDataCount', 6)}
        renderCells={(item) => (
            <>
                <TableCell >
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
        sorters={[
            {
                onSort: actionsData.onSortA,
                order: ''
            },
            {
                onSort: actionsData.onSortB,
                order: 'asc'
            },
            {
                onSort: actionsData.onSortC,
                order: 'desc'
            }
        ]}
        filters={[
            {
                onFilter: actionsData.onFilterA,
                options: [
                    {
                        selected: true,
                        label: 'Cell A1',
                        value: 'A1'
                    },
                    {
                        selected: true,
                        label: 'Cell A2',
                        value: 'A2'
                    },
                    {
                        selected: false,
                        label: 'Cell A3',
                        value: 'A3'
                    }
                ]
            },
            {
                onFilter: actionsData.onFilterB,
                options: [
                    {
                        label: 'Cell B1',
                        value: 'B1'
                    },
                    {
                        label: 'Cell B2',
                        value: 'B2'
                    },
                    {
                        label: 'Cell B3',
                        value: 'B3'
                    }
                ]
            },
        ]}
    />
)

export const WithCheckItem = () => (
    <Table
        theme={createMuiTheme(object('theme', theme))}
        headers={HEADERS}
        datalist={DATALIST}
        pageLength={number('pageLength', 3)}
        currentPage={number('currentPage', 1)}
        totalDataCount={number('totalDataCount', 6)}
        onCheckItem={actionsData.onCheckItem}
        renderCells={(item) => (
            <>
                <TableCell >
                    {item.A}
                </TableCell>
                <TableCell >
                    {item.B}
                </TableCell>
                <TableCell >
                    {item.C}
                </TableCell>
            </>
        )}
    />
)





