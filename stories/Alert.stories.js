import React from 'react'
import { action } from '@storybook/addon-actions'
import { withKnobs, select, boolean, text, object } from '@storybook/addon-knobs'
import Alert from '../src/components/Alert'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: Alert,
    title: 'Alert',
    decorators: [withKnobs],
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            source: false,
        },
    },
};

export const actionsData = {
    onConfirm: action('onConfirm'),
    onClose: action('onClose'),
    onCancel: action('onCancel')
};

export const Default = () => (
    <Alert
        theme={createMuiTheme(object('theme', theme))}
        show={boolean('show', true)}
        title={text('title', 'Default title')}
        content={text('content', 'Default content...')}
        loading={boolean('loading', false)}
        type={select('type', ['success', 'error', 'warning', 'question'])}
        {...actionsData}
    />
)