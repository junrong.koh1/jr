import React from 'react'
import { action } from '@storybook/addon-actions'
import { object, boolean, text } from '@storybook/addon-knobs'
import TagsInput from '../src/components/TagsInput'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: TagsInput,
    title: 'TagsInput',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `
            More usage detail can be refered to https://react-select.com/props  
            #### Customized component
            If you want to customized your own menu, please refer to : https://react-select.com/components#adjusting-the-styling.
            `,
            source: false,
        },
    },
};

export const actionsData = {
    onChange: action('onChange'),
}

export const Default = () => (
    <TagsInput
        theme={createMuiTheme(object('theme', theme))}
        label={text('label', 'Label')}
        placeholder={text('placeholder', 'Type something and press enter...')}
        invalid={boolean('invalid', false)}
        disabled={boolean('disabled', false)}
        onChange={actionsData.onChange}
        defaultValues={object('defaultValues', ['option A', 'option B', 'option C'])}
    />
)
