const theme = {
    palette: {
        primary: {
            light: '#f0f4f9',
            main: '#3f51b5',
            dark: '#303f9f',
            contrastText: '#fff'
        },
        secondary: {
            main: '#000910',
            contrastText: '#fff'
        },
        error: {
            light: '#e57373',
            main: '#F56960'
        },
        success: {
            main: '#4be79c'
        },
        text: {
            primary: '#000910',
            secondary: 'rgba(0, 0, 0, 0.54)',
            placeholder: '#BFBFBF',
            disabled: '#CBCCD0'
        },
        action: {
            active: 'rgba(0, 0, 0, 0.54)',
            disabled: 'rgba(0, 0, 0, 0.3)',
            hover: 'rgba(0, 0, 0, 0.04)',
            selected: 'rgba(0, 0, 0, 0.08)'
        },
        divider: 'rgba(0, 0, 0, 0.12)',
        background: {
            paper: '#fff',
            default: '#fafafa'
        }
    },
}

const darkTheme = createMuiTheme({
    spacing: 5,
    palette: {
        type: 'dark',
        primary: {
            light: '#bfd7ed',
            main: '#60a3d9',
            dark: '#0074b7'
        },
        secondary: {
            light: '#FAF3F3',
            main: '#E1E5EA',
            dark: '#A7BBC7'
        },
        background: {
            hover: '#f0f4f9'
        },
        tag: {
            blue: '#6f8cb8',
            lightblue: '#6fb8b8',
            bluepurple: '#7e6fb8',
            green: '#6fb88c',
            greenyellow: '#7eb86f',
            yellow: '#a9b86f',
            lightyellow: '#c9b81e',
            orange: '#b89b6f',
            red: '#b86f6f'
        },
    },
});

export default theme