import React from 'react'
import { object, boolean, text } from '@storybook/addon-knobs'
import Pagination from '../src/components/Pagination'
import { createMuiTheme, makeStyles } from '@material-ui/core/styles'
import theme from './theme'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default {
    component: Pagination,
    title: 'Pagination',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `More usage detail can be refered to https://www.npmjs.com/package/react-paginate `,
            source: false,
        },
    },
};

export const Default = () => (
    <Pagination
        theme={createMuiTheme(object('theme', theme))}
        pageCount={10}
        onPageChange={(page) => {
            console.log('page = ', page)
        }}
    />
)

export const withRowsPerPage = () => (
    <Pagination
        theme={createMuiTheme(object('theme', theme))}
        pageCount={10}
        rowsPerPage={[10, 20, 30]}
        onRowsPerPageChange={(rows) => {
            console.log('rows = ', rows)
        }}
    />
)

const customStyles = makeStyles({
    page: {
        borderColor: 'orange'
    },
    pageLink: {
        color: 'orange'
    },
    activePage: {
        backgroundColor: 'orange'
    }
})

export const withCustomClassesAndLabel = () => {
    const classes = customStyles()
    return <Pagination
        theme={createMuiTheme(object('theme', theme))}
        pageCount={10}
        rowsPerPage={[10, 20, 30]}
        elementClasses={{
            page: classes.page,
            pageLink: classes.pageLink,
            activePage: classes.activePage,
            previous: classes.page,
            next: classes.page
        }}
        previousLabel={<FontAwesomeIcon icon={faChevronLeft} />}
        nextLabel={<FontAwesomeIcon icon={faChevronRight} />}
    />
}