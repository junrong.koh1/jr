import React, { useState } from 'react'
import { action } from '@storybook/addon-actions'
import DropdownMenu from '../src/components/DropdownMenu'
import TextInput from '../src/components/TextInput'
import { boolean, text, object } from '@storybook/addon-knobs'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: DropdownMenu,
    title: 'DropdownMenu',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `
            More usage detail can be refered to https://react-select.com/props.  
            #### Customized component
            If you want to customized your own menu, please refer to : https://react-select.com/components#adjusting-the-styling.
            `,
            source: false,
        },
    },
};

export const actionsData = {
    onClickItem: action('onClickItem'),
    onFocus: action('onFocus'),
}

const options = [
    { value: 1, label: 'one' },
    { value: 2, label: 'two' },
    { value: 3, label: 'three' },
    { value: 4, label: 'four' },
]

export const Default = () => (
    <DropdownMenu
        theme={createMuiTheme(object('theme', theme))}
        label={text('label', 'Label')}
        menuItems={options}
        defaultValue={object('defaultValue', options[0])}
        disabled={boolean('disabled', false)}
        invalid={boolean('invalid', false)}
        required={boolean('required', false)}
        multiple={boolean('multiple', false)}
        {...actionsData} />

)

export const CreatableSelect = () => (
    <DropdownMenu
        creatable={true}
        theme={createMuiTheme(object('theme', theme))}
        label={text('label', 'Label')}
        menuItems={options}
        defaultValue={object('defaultValue', options[0])}
        disabled={boolean('disabled', false)}
        invalid={boolean('invalid', false)}
        required={boolean('required', false)}
        multiple={boolean('multiple', false)}
        {...actionsData}
    />
)

