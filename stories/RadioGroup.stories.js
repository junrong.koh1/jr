import React, { useState } from 'react'
import { action } from '@storybook/addon-actions'
import { object, boolean, text } from '@storybook/addon-knobs'
import RadioGroup from '../src/components/Radio/RadioGroup'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: RadioGroup,
    title: 'RadioGroup',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `More usage detail can be refered to https://material-ui.com/components/radio-buttons/#radiogroup`,
            source: false,
        },
    },
};

const OPTIONS = [
    {
        value: 'A',
        label: 'A'
    },
    {
        value: 'B',
        label: 'B'
    },
    {
        value: 'C',
        label: 'C'
    },
    {
        value: 'D',
        label: 'D'
    },
]

export const actionsData = {
    onChange: action('onChange'),
}

export const Default = () => {
    return (
        <RadioGroup
            theme={createMuiTheme(object('theme', theme))}
            options={OPTIONS}
            horizontal={boolean('horizontal', false)}
            disabled={boolean('disabled', false)}
            onChange={(result) => {
                actionsData.onChange(result)
            }}
            defaultValue={OPTIONS[2].value}
        />
    )
}