import React, { useState } from 'react'
import { action } from '@storybook/addon-actions'
import { object, boolean, text } from '@storybook/addon-knobs'
import CheckboxGroup from '../src/components/Checkbox/CheckboxGroup'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: CheckboxGroup,
    title: 'CheckboxGroup',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `More usage detail can be refered to https://material-ui.com/components/checkboxes/ `,
            source: false,
        },
    },
};

const OPTIONS = [
    {
        checked: true,
        name: 'A',
        label: 'A'
    },
    {
        checked: false,
        name: 'B',
        label: 'B'
    },
    {
        checked: true,
        disabled: true,
        name: 'C',
        label: 'C'
    },
    {
        checked: false,
        disabled: true,
        name: 'D',
        label: 'D'
    },
]

export const actionsData = {
    onChange: action('onChange'),
}

export const Default = () => {
    const [options, setOptions] = useState(OPTIONS)
    return (
        <CheckboxGroup
            theme={createMuiTheme(object('theme', theme))}
            options={options}
            horizontal={boolean('horizontal', false)}
            onChange={(result) => {
                setOptions(result)
                actionsData.onChange(result)
            }}
        />
    )
}