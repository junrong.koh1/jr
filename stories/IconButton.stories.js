import React from 'react'
import { action } from '@storybook/addon-actions'
import { object, boolean, text } from '@storybook/addon-knobs'
import { createMuiTheme } from '@material-ui/core/styles'
import IconButton from '../src/components/IconButton'
import theme from './theme'

export default {
    component: IconButton,
    title: 'IconButton',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            source: false,
            propTables: [IconButton]
        },
    },
};

export const actionsData = {
    onClick: action('onClick'),
};

export const Default = () => (
    <IconButton
        theme={createMuiTheme(object('theme', theme))}
        variant={text('variant', "contained")}
        loading={boolean('loading', false)}
        disabled={boolean('disabled', false)}
        onClick={actionsData.onClick}
    >
        <div className="icon icono-cross" style={{ width: 20 }} />
    </IconButton>
)