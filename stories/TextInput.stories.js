import React from 'react'
import { action } from '@storybook/addon-actions'
import TextInput from '../src/components/TextInput'
import { object, boolean, text, number } from '@storybook/addon-knobs'
import { createMuiTheme } from '@material-ui/core/styles'
import theme from './theme'

export default {
    component: TextInput,
    title: 'TextInput',
    excludeStories: /.*Data$/,
    parameters: {
        info: {
            text: `More usage detail can be refered to https://material-ui.com/api/text-field/#props`,
            source: false,
            propTables: [TextInput]
        },
    },
};

export const actionsData = {
    onChange: action('onChange'),
    onFocus: action('onFocus'),
};

export const Default = () => (
    <TextInput
        theme={createMuiTheme(object('theme', theme))}
        label={text('label', 'Label')}
        value={text('value', 'Default value')}
        placeholder={text('placeholder', 'placeholder')}
        helperText={text('helperText', '')}
        disabled={boolean('disabled', false)}
        error={boolean('error', false)}
        multiline={boolean('multiline', false)}
        onChange={(e) => {
            actionsData.onChange(e)
            setValue(e.target.value)
        }}
        onFocus={(e) => {
            actionsData.onFocus(e)
        }}
        fullWidth
    />
)

export const CustomizeInputProps = () => (
    <TextInput
        // theme={createMuiTheme(object('theme', theme))}
        value={text('value', 'Default value')}
        placeholder={text('placeholder', 'placeholder')}
        helperText={text('helperText', '')}
        disabled={boolean('disabled', false)}
        error={boolean('error', false)}
        multiline={boolean('multiline', false)}
        onChange={(e) => {
            actionsData.onChange(e)
            setValue(e.target.value)
        }}
        onFocus={(e) => {
            actionsData.onFocus(e)
        }}
        fullWidth
        InputProps={{
            startAdornment: <div style={{ margin: 5 }} ><div className="icon icono-search" /></div>
        }}
    />
)
