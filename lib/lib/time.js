"use strict";

exports.__esModule = true;
exports.LAST_MONTH = exports.TODAY = void 0;

Date.prototype.endOf = function (unit) {
  var result = new Date(this.getTime());
  var y = result.getFullYear();
  var m = result.getMonth();

  switch (unit) {
    case 'day':
      break;

    case 'month':
      result = new Date(y, m + 1, 0);
      break;

    case 'year':
      result = new Date(y + 1, 0, 0);
      break;

    default:
      return;
  }

  result.setHours(23, 59, 59, 999);
  return result;
};

Date.prototype.startOf = function (unit) {
  var result = new Date(this.getTime());
  var y = result.getFullYear();
  var m = result.getMonth();

  switch (unit) {
    case 'day':
      break;

    case 'month':
      result = new Date(y, m, 1);
      break;

    case 'year':
      result = new Date(y, 0, 1);
      break;

    default:
      return;
  }

  result.setHours(0, 0, 0, 0);
  return result;
};

Date.prototype.afterDays = function (day) {
  var ONE_DAY_SECONDS = 60 * 60 * 24 * 1000;
  var time = this.getTime();
  var result = new Date(time);
  result.setTime(time + day * ONE_DAY_SECONDS);
  return result;
};

var TODAY = new Date();
exports.TODAY = TODAY;
var LAST_MONTH = new Date(TODAY.getTime()); // A month may have 28, 29, 30 or 31 days, reset date to 1 to avoid error.

exports.LAST_MONTH = LAST_MONTH;
LAST_MONTH.setDate(1);
LAST_MONTH.setMonth(TODAY.getMonth() - 1);