"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _styles = require("@material-ui/core/styles");

var theme = (0, _styles.createMuiTheme)({
  palette: {
    primary: {
      light: '#A8D2FF',
      main: '#0E7CF4',
      dark: '#0A5FBC'
    },
    secondary: {
      light: '#BFBFBF',
      main: '#000910',
      dark: '#000000'
    },
    error: {
      main: '#F56960'
    },
    success: {
      main: '#4be79c'
    },
    text: {
      primary: '#000910',
      placeholder: '#BFBFBF'
    }
  },
  typography: {
    fontSize: 15,
    fontFamily: "'Noto Sans TC', sans-serif",
    h1: {
      fontSize: '2.5rem',
      fontWeight: 400,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    h2: {
      fontSize: '2.25rem',
      fontWeight: 400,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    h3: {
      fontSize: '1.5rem',
      fontWeight: 700,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    h4: {
      fontSize: '1.3125rem',
      fontWeight: 700,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    h5: {
      fontSize: '1.125rem',
      fontWeight: 700,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    h6: {
      fontSize: '0.9375rem',
      fontWeight: 700,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    },
    body1: {
      fontSize: '0.9375rem',
      lineHeight: 'normal'
    },
    button: {
      fontSize: '0.9375rem',
      fontWeight: 700,
      letterSpacing: 'normal',
      textTransform: 'none',
      lineHeight: 'normal'
    },
    caption: {
      fontSize: 12,
      lineHeight: 'normal',
      letterSpacing: 'normal'
    }
  },
  spacing: 5
});
var _default = theme;
exports["default"] = _default;
module.exports = exports.default;