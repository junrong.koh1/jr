"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _Checkbox = _interopRequireDefault(require("@material-ui/core/Checkbox"));

var _theme = _interopRequireDefault(require("../../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var CheckboxIcon = function CheckboxIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("rect", {
    x: ".5",
    y: ".5",
    width: "17",
    height: "17",
    rx: "1.5",
    fill: "#fff",
    stroke: "#CECECE"
  }));
};

CheckboxIcon.defaultProps = {
  width: "18",
  height: "18",
  viewBox: "0 0 18 18",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var StyledCheckbox = (0, _styles.withStyles)(function (theme) {
  return {
    root: {
      '&:hover': {
        backgroundColor: 'transparent',
        '& rect': {
          stroke: theme.palette.primary.main
        }
      }
    },
    checked: {
      '& span': {
        color: theme.palette.primary.main
      }
    },
    disabled: {
      '& span': {
        color: theme.palette.action.disabled,
        '&.MuiIconButton-label': {
          pointerEvents: 'initial',
          cursor: 'not-allowed',
          '&:hover': {
            '& rect': {
              stroke: theme.palette.action.disabled
            }
          }
        }
      }
    }
  };
})(_Checkbox["default"]);

var CMCheckbox = function CMCheckbox(props) {
  var option = props.option,
      index = props.index;

  if (!option) {
    return null;
  }

  return /*#__PURE__*/_react["default"].createElement(StyledCheckbox, {
    className: props.className,
    icon: /*#__PURE__*/_react["default"].createElement(CheckboxIcon, {
      style: {
        margin: 3
      }
    }),
    disableRipple: true,
    onChange: props.onChange,
    onClick: function onClick(e) {
      e.stopPropagation();
    },
    checked: !!option.checked,
    disabled: option.disabled,
    name: option.name,
    id: index.toString()
  });
};

var Checkbox = function Checkbox(props) {
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: props.theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(CMCheckbox, props));
};

Checkbox.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Callback function when select box. */
  onChange: _propTypes["default"].func,
  option: _propTypes["default"].shape({
    disabled: _propTypes["default"].bool,
    checked: _propTypes["default"].bool,
    name: _propTypes["default"].any
  }),
  index: _propTypes["default"].number
} : {};
Checkbox.defaultProps = {
  className: '',
  onChange: null,
  option: null,
  index: 0
};
var _default = Checkbox;
exports["default"] = _default;
module.exports = exports.default;