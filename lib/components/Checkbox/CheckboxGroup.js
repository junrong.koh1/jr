"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _FormGroup = _interopRequireDefault(require("@material-ui/core/FormGroup"));

var _FormControlLabel = _interopRequireDefault(require("@material-ui/core/FormControlLabel"));

var _index = _interopRequireDefault(require("./index"));

var _theme = _interopRequireDefault(require("../../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var CheckboxGroup = function CheckboxGroup(props) {
  var options = props.options,
      horizontal = props.horizontal;
  var theme = props.theme || _theme["default"];

  var handleChange = function handleChange(event) {
    var id = event.target.id;
    var newOptions = [].concat(options);
    newOptions[id] = _extends({}, newOptions[id], {
      checked: !newOptions[id].checked
    });

    if (props.onChange) {
      props.onChange([].concat(newOptions));
    }
  };

  var renderOptions = (0, _react.useCallback)(function () {
    return options.map(function (option, index) {
      return /*#__PURE__*/_react["default"].createElement(_FormControlLabel["default"], {
        key: index,
        control: /*#__PURE__*/_react["default"].createElement(_index["default"], {
          theme: theme,
          option: option,
          index: index,
          onChange: handleChange
        }),
        label: option.label
      });
    });
  }, [options, theme]);
  return /*#__PURE__*/_react["default"].createElement(_FormControl["default"], {
    component: "fieldset"
  }, /*#__PURE__*/_react["default"].createElement(_FormGroup["default"], null, horizontal ? /*#__PURE__*/_react["default"].createElement("div", null, renderOptions()) : renderOptions()));
};

CheckboxGroup.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Callback function when select box. */
  onChange: _propTypes["default"].func,
  options: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    checked: _propTypes["default"].bool,
    label: _propTypes["default"].string
  })),
  horizontal: _propTypes["default"].bool
} : {};
CheckboxGroup.defaultProps = {
  className: '',
  onChange: null,
  options: [],
  horizontal: false
};
var _default = CheckboxGroup;
exports["default"] = _default;
module.exports = exports.default;