"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _styles = require("@material-ui/styles");

var _theme = _interopRequireDefault(require("../theme"));

var _DatePicker = _interopRequireDefault(require("./DatePicker"));

var _DropdownMenu = _interopRequireDefault(require("./DropdownMenu"));

var _time = require("../lib/time");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var defaultTimePeriodsLabel = {
  last5minutes: 'Last 5 minutes',
  last15minutes: 'Last 15 minutes',
  last1hours: 'Last 1 hours',
  last12hours: 'Last 12 hours',
  last24hours: 'Last 24 hours',
  last7days: 'Last 7 days',
  last30days: 'Last 30 days',
  lastmonth: 'Last month',
  thismonth: 'This month'
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      '& span': {
        padding: theme.spacing(1)
      }
    },
    filter: {
      width: 165,
      marginLeft: theme.spacing(3)
    }
  };
});

var CMDateRangePicker = function CMDateRangePicker(props) {
  var classes = useStyles();
  var theme = props.theme || _theme["default"];
  var startDate = props.startDate,
      endDate = props.endDate,
      maxDate = props.maxDate,
      minDate = props.minDate,
      locale = props.locale,
      startDateChange = props.startDateChange,
      endDateChange = props.endDateChange;
  var withTimePeriod = props.withTimePeriod,
      timePeriodPlaceholder = props.timePeriodPlaceholder,
      defaultTimePeriod = props.defaultTimePeriod,
      customTimePeriodLabel = props.customTimePeriodLabel;
  var showTimeSelect = props.showTimeSelect,
      timeIntervals = props.timeIntervals,
      dateFormat = props.dateFormat,
      defaultTimeSelectionPeriod = props.defaultTimeSelectionPeriod,
      timeCaption = props.timeCaption,
      inputClass = props.inputClass;
  var TimePeriods = showTimeSelect ? [{
    value: 'last5minutes',
    label: customTimePeriodLabel.last5minutes || defaultTimePeriodsLabel.last5minutes,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 5),
    endDate: new Date()
  }, {
    value: 'last15minutes',
    label: customTimePeriodLabel.last15minutes || defaultTimePeriodsLabel.last15minutes,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 15),
    endDate: new Date()
  }, {
    value: 'last1hours',
    label: customTimePeriodLabel.last1hours || defaultTimePeriodsLabel.last1hours,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 60),
    endDate: new Date()
  }, {
    value: 'last12hours',
    label: customTimePeriodLabel.last12hours || defaultTimePeriodsLabel.last12hours,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 12),
    endDate: new Date()
  }, {
    value: 'last24hours',
    label: customTimePeriodLabel.last24hours || defaultTimePeriodsLabel.last24hours,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 24),
    endDate: new Date()
  }, {
    value: 'last7days',
    label: customTimePeriodLabel.last7days || defaultTimePeriodsLabel.last7days,
    startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 7),
    endDate: new Date()
  }] : [{
    value: 'last7days',
    label: customTimePeriodLabel.last7days || defaultTimePeriodsLabel.last7days,
    startDate: _time.TODAY.afterDays(-6).startOf('day'),
    endDate: _time.TODAY.endOf('day')
  }, {
    value: 'last30days',
    label: customTimePeriodLabel.last30days || defaultTimePeriodsLabel.last30days,
    startDate: _time.TODAY.afterDays(-29).startOf('day'),
    endDate: _time.TODAY.endOf('day')
  }, {
    value: 'lastmonth',
    label: customTimePeriodLabel.lastmonth || defaultTimePeriodsLabel.lastmonth,
    startDate: _time.LAST_MONTH.startOf('month'),
    endDate: _time.LAST_MONTH.endOf('month')
  }, {
    value: 'thismonth',
    label: customTimePeriodLabel.thismonth || defaultTimePeriodsLabel.thismonth,
    startDate: _time.TODAY.startOf('month'),
    endDate: _time.TODAY.endOf('day')
  }];
  var selectedTimePeriod = withTimePeriod && TimePeriods.find(function (t) {
    return t.value === (showTimeSelect ? defaultTimeSelectionPeriod : defaultTimePeriod);
  });

  var _useState = (0, _react.useState)(selectedTimePeriod || null),
      filterValue = _useState[0],
      setFilterValue = _useState[1];

  (0, _react.useEffect)(function () {
    if (filterValue) {
      startDateChange(filterValue.startDate);
      endDateChange(filterValue.endDate);
    }
  }, [filterValue]);

  var onStartDateChange = function onStartDateChange(date) {
    var selectedStartDate = showTimeSelect ? date : date.startOf('day');
    var matchedTimePeriod = TimePeriods.find(function (t) {
      return t.startDate.getTime() === selectedStartDate.getTime() && t.endDate.getTime() === endDate.getTime();
    });

    if (matchedTimePeriod) {
      setFilterValue(matchedTimePeriod);
    } else {
      startDateChange(selectedStartDate);

      if (selectedStartDate > endDate) {
        endDateChange(showTimeSelect ? new Date(date.getTime() + 1000 * 60 * timeIntervals) : date.endOf('day'));
      }

      setFilterValue(null);
    }
  };

  var onEndDateChange = function onEndDateChange(date) {
    var selectedEndDate = showTimeSelect ? date : date.endOf('day');
    var matchedTimePeriod = TimePeriods.find(function (t) {
      return t.startDate.getTime() === startDate.getTime() && t.endDate.getTime() === selectedEndDate.getTime();
    });

    if (matchedTimePeriod) {
      setFilterValue(matchedTimePeriod);
    } else {
      setFilterValue(null);

      if (showTimeSelect && selectedEndDate < startDate) {
        return endDateChange(new Date(startDate.getTime() + 1000 * 60 * timeIntervals));
      }

      endDateChange(selectedEndDate);
    }
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: classes.wrapper
  }, /*#__PURE__*/_react["default"].createElement(_DatePicker["default"], {
    theme: theme,
    selectsStart: true,
    selected: startDate,
    startDate: startDate,
    endDate: endDate,
    onChange: onStartDateChange,
    minDate: minDate,
    maxDate: maxDate,
    locale: locale,
    showTimeSelect: showTimeSelect,
    timeIntervals: timeIntervals,
    dateFormat: dateFormat,
    timeCaption: timeCaption,
    className: inputClass || null
  }), /*#__PURE__*/_react["default"].createElement("span", null, "~"), /*#__PURE__*/_react["default"].createElement(_DatePicker["default"], {
    theme: theme,
    selectsEnd: true,
    selected: endDate,
    startDate: startDate,
    endDate: endDate,
    onChange: onEndDateChange,
    minDate: startDate,
    maxDate: maxDate,
    locale: locale,
    showTimeSelect: showTimeSelect,
    timeIntervals: timeIntervals,
    dateFormat: dateFormat,
    timeCaption: timeCaption,
    className: inputClass || null
  }), withTimePeriod && /*#__PURE__*/_react["default"].createElement(_DropdownMenu["default"], {
    theme: theme,
    className: classes.filter,
    menuItems: TimePeriods,
    value: filterValue,
    onClickItem: function onClickItem(item) {
      setFilterValue(item);
    },
    placeholder: timePeriodPlaceholder
  }));
};

var DateRangePicker = function DateRangePicker(props) {
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: props.theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(CMDateRangePicker, props));
};

DateRangePicker.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** Used for setting start date of selected range. */
  startDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting end date of selected range. */
  endDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting start date of available date range. */
  minDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting end of available date range. */
  maxDate: _propTypes["default"].instanceOf(Date),

  /** Setup calendar language of month and week day. */
  locale: _propTypes["default"].string,

  /** Callback function when start date changed. */
  startDateChange: _propTypes["default"].func.isRequired,

  /** Callback function when end date changed. */
  endDateChange: _propTypes["default"].func.isRequired,

  /** Show time periods button. */
  withTimePeriod: _propTypes["default"].bool,

  /** Placeholder of dropdown menu of time periods. */
  timePeriodPlaceholder: _propTypes["default"].string,

  /** Show time selection column in datePicker popper. */
  showTimeSelect: _propTypes["default"].bool,

  /** Time intervals for time selection values. */
  timeIntervals: _propTypes["default"].number,

  /** Default value of time periods for pickers without time selection. */
  defaultTimePeriod: _propTypes["default"].oneOf(['last7days', 'last30days', 'lastmonth', 'thismonth']),

  /** Default value of time periods for pickers with time selection. */
  defaultTimeSelectionPeriod: _propTypes["default"].oneOf(['last5minutes', 'last15minutes', 'last1hours', 'last12hours', 'last24hours', 'last7days']),

  /** Customize label of time periods. */
  customTimePeriodLabel: _propTypes["default"].shape({
    last5minutes: _propTypes["default"].string,
    last15minutes: _propTypes["default"].string,
    last1hours: _propTypes["default"].string,
    last12hours: _propTypes["default"].string,
    last24hours: _propTypes["default"].string,
    last7days: _propTypes["default"].string,
    last30days: _propTypes["default"].string,
    lastmonth: _propTypes["default"].string,
    thismonth: _propTypes["default"].string
  }),

  /** Customize time format in input. */
  dateFormat: _propTypes["default"].string,

  /** Customize title of time selection column */
  timeCaption: _propTypes["default"].string,

  /** Add style to DatePicker container (inputs) **/
  inputClass: _propTypes["default"].string
} : {};
DateRangePicker.defaultProps = {
  withTimePeriod: false,
  timeIntervals: 5,
  defaultTimePeriod: 'last7days',
  defaultTimeSelectionPeriod: 'last5minutes',
  timePeriodPlaceholder: 'Time Period',
  customTimePeriodLabel: defaultTimePeriodsLabel
};
var _default = DateRangePicker;
exports["default"] = _default;
module.exports = exports.default;