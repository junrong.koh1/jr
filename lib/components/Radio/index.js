"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _Radio = _interopRequireDefault(require("@material-ui/core/Radio"));

var _theme = _interopRequireDefault(require("../../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var StyledRadio = (0, _styles.withStyles)(function (theme) {
  return {
    root: {
      '&:hover': {
        backgroundColor: 'transparent',
        '& span': {
          color: theme.palette.primary.main
        }
      }
    },
    checked: {
      '& span': {
        color: theme.palette.primary.main
      }
    },
    disabled: {
      '& span': {
        color: theme.palette.action.disabled
      }
    }
  };
})(_Radio["default"]);

var CMRadio = function CMRadio(props) {
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: props.theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(StyledRadio, _extends({
    disableRipple: true
  }, props)));
};

CMRadio.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Callback function when select box. */
  onChange: _propTypes["default"].func,
  option: _propTypes["default"].shape({
    disabled: _propTypes["default"].bool,
    checked: _propTypes["default"].bool,
    name: _propTypes["default"].any
  }),
  index: _propTypes["default"].number
} : {};
CMRadio.defaultProps = {
  className: '',
  onChange: null,
  option: null,
  index: 0
};
var _default = CMRadio;
exports["default"] = _default;
module.exports = exports.default;