"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _index = _interopRequireDefault(require("./index"));

var _RadioGroup = _interopRequireDefault(require("@material-ui/core/RadioGroup"));

var _FormControlLabel = _interopRequireDefault(require("@material-ui/core/FormControlLabel"));

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _theme = _interopRequireDefault(require("../../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var CMRadioGroup = function CMRadioGroup(props) {
  var options = props.options,
      horizontal = props.horizontal,
      defaultValue = props.defaultValue,
      disabled = props.disabled;

  var _useState = (0, _react.useState)(defaultValue),
      value = _useState[0],
      setValue = _useState[1];

  var theme = props.theme || _theme["default"];

  var handleChange = function handleChange(event) {
    var _event$target;

    var result = event === null || event === void 0 ? void 0 : (_event$target = event.target) === null || _event$target === void 0 ? void 0 : _event$target.value;
    setValue(result);

    if (props.onChange) {
      props.onChange(result);
    }
  };

  var renderOptions = (0, _react.useCallback)(function () {
    return options.map(function (option, index) {
      return /*#__PURE__*/_react["default"].createElement(_FormControlLabel["default"], {
        key: index,
        value: option.value,
        label: option.label,
        disabled: disabled,
        control: /*#__PURE__*/_react["default"].createElement(_index["default"], {
          theme: theme
        })
      });
    });
  }, [options, disabled, theme]);
  return /*#__PURE__*/_react["default"].createElement(_FormControl["default"], {
    component: "fieldset"
  }, /*#__PURE__*/_react["default"].createElement(_RadioGroup["default"], {
    value: value,
    onChange: handleChange
  }, horizontal ? /*#__PURE__*/_react["default"].createElement("div", null, renderOptions()) : renderOptions()));
};

CMRadioGroup.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Callback function when select box. */
  onChange: _propTypes["default"].func,
  options: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    label: _propTypes["default"].string,
    value: _propTypes["default"].any
  })),

  /** Disabled this group. */
  disabled: _propTypes["default"].bool,
  horizontal: _propTypes["default"].bool,

  /** Default checked option. */
  defaultValue: _propTypes["default"].any
} : {};
CMRadioGroup.defaultProps = {
  className: '',
  onChange: null,
  options: [],
  horizontal: false,
  defaultValue: null
};
var _default = CMRadioGroup;
exports["default"] = _default;
module.exports = exports.default;