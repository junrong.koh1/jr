"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("./Button"));

var _styles = require("@material-ui/styles");

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Button = (0, _styles.withStyles)({
  root: {
    minWidth: 36,
    height: 36,
    padding: 0
  }
})(_Button["default"]);

var IconButton = function IconButton(props) {
  return /*#__PURE__*/_react["default"].createElement(Button, props, props.children);
};

IconButton.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Button container style. */
  variant: _propTypes["default"].oneOf(['text', 'outlined', 'contained']),

  /** Label on button. */
  label: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].object]),

  /** Show loading icon after label. */
  loading: _propTypes["default"].bool,

  /** Disable button. */
  disabled: _propTypes["default"].bool,

  /** Callback function when click this button. */
  onClick: _propTypes["default"].func
} : {};
IconButton.defaultProps = {
  className: '',
  variant: 'contained',
  label: '',
  loading: false,
  disabled: false,
  onClick: null
};
var _default = IconButton;
exports["default"] = _default;
module.exports = exports.default;