"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _ClickAwayListener = _interopRequireDefault(require("@material-ui/core/ClickAwayListener"));

var _Popper = _interopRequireDefault(require("@material-ui/core/Popper"));

var _Fade = _interopRequireDefault(require("@material-ui/core/Fade"));

var _Paper = _interopRequireDefault(require("@material-ui/core/Paper"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _CheckboxGroup = _interopRequireDefault(require("../Checkbox/CheckboxGroup"));

var _Button = _interopRequireDefault(require("../Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    paper: {
      marginTop: 15,
      marginLeft: 40
    },
    arrowUpContainer: {
      position: 'absolute',
      top: 0,
      left: 90
    },
    arrowUp: {
      width: 0,
      height: 0,
      borderLeft: '10px solid transparent',
      borderRight: '10px solid transparent',
      borderBottom: '17px solid white'
    },
    optionsContainer: {
      padding: 20
    },
    actionContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      padding: 20,
      borderTop: '0.5px solid',
      borderColor: theme.palette.divider
    },
    button: {
      minWidth: 65,
      '&:first-of-type': {
        marginRight: 5
      }
    }
  };
});

var FilterPopper = function FilterPopper(props) {
  var classes = useStyles();
  var anchorEl = props.anchorEl,
      filters = props.filters;
  var show = Boolean(anchorEl);

  var _useState = (0, _react.useState)(filters),
      checkOptions = _useState[0],
      setCheckOptions = _useState[1];

  var onClose = function onClose() {
    if (show) {
      setCheckOptions(filters);
      props.onClose();
    }
  };

  if (show) {
    return /*#__PURE__*/_react["default"].createElement(_ClickAwayListener["default"], {
      onClickAway: onClose
    }, /*#__PURE__*/_react["default"].createElement(_Popper["default"], {
      open: true,
      anchorEl: anchorEl,
      transition: true
    }, function (_ref) {
      var TransitionProps = _ref.TransitionProps;
      return /*#__PURE__*/_react["default"].createElement(_Fade["default"], _extends({}, TransitionProps, {
        timeout: 350
      }), /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
        className: classes.arrowUpContainer
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: classes.arrowUp
      })), /*#__PURE__*/_react["default"].createElement(_Paper["default"], {
        className: classes.paper
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: classes.optionsContainer
      }, /*#__PURE__*/_react["default"].createElement(_CheckboxGroup["default"], {
        options: checkOptions,
        onChange: function onChange(result) {
          setCheckOptions(result);
        }
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: classes.actionContainer
      }, /*#__PURE__*/_react["default"].createElement(_Button["default"], {
        className: classes.button,
        variant: "text",
        label: "Cancel",
        onClick: onClose
      }), /*#__PURE__*/_react["default"].createElement(_Button["default"], {
        className: classes.button,
        label: "Filter",
        onClick: function onClick() {
          props.onFilter(checkOptions);
        }
      })))));
    }));
  }

  return null;
};

FilterPopper.propTypes = process.env.NODE_ENV !== "production" ? {
  anchorEl: _propTypes["default"].object,
  filters: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    selected: _propTypes["default"].bool,
    label: _propTypes["default"].string,
    value: _propTypes["default"].any
  })).isRequired,
  onFilter: _propTypes["default"].func.isRequired,
  onClose: _propTypes["default"].func.isRequired
} : {};
var _default = FilterPopper;
exports["default"] = _default;
module.exports = exports.default;