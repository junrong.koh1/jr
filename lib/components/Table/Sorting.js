"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var OrderUpIcon = function OrderUpIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M4.5 0l3.897 5.25H.603L4.5 0z",
    fill: "#A6A6A6"
  }));
};

OrderUpIcon.defaultProps = {
  width: "9",
  height: "6",
  viewBox: "0 0 9 6",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var OrderDownIcon = function OrderDownIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M4.5 6L.603.75h7.794L4.5 6z",
    fill: "#A6A6A6"
  }));
};

OrderDownIcon.defaultProps = {
  width: "9",
  height: "6",
  viewBox: "0 0 9 6",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      minHeight: 20
    },
    icon: {
      cursor: 'pointer',
      '&:first-of-type': {
        marginBottom: 3
      }
    },
    activeIcon: {
      cursor: 'defualt',
      pointerEvents: 'none',
      '&:first-of-type': {
        marginBottom: 3
      },
      color: theme.palette.primary.main,
      '& path': {
        fill: theme.palette.primary.main
      }
    }
  };
});

var Sorting = function Sorting(props) {
  var classes = useStyles();
  var order = props.order,
      onSort = props.onSort;

  var _useState = (0, _react.useState)(order),
      currentOrder = _useState[0],
      setCurrentOrder = _useState[1];

  var handleAsc = (0, _react.useCallback)(function (e) {
    e.stopPropagation();
    setCurrentOrder('asc');
    onSort && onSort('asc');
  }, [onSort]);
  var handleDesc = (0, _react.useCallback)(function (e) {
    e.stopPropagation();
    setCurrentOrder('desc');
    onSort && onSort('desc');
  }, [onSort]);
  (0, _react.useEffect)(function () {
    if (currentOrder != order) {
      setCurrentOrder(order);
    }
  }, [order]);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: classes.root
  }, /*#__PURE__*/_react["default"].createElement(OrderUpIcon, {
    className: currentOrder === 'asc' ? classes.activeIcon : classes.icon,
    onClick: handleAsc
  }), /*#__PURE__*/_react["default"].createElement(OrderDownIcon, {
    className: currentOrder === 'desc' ? classes.activeIcon : classes.icon,
    onClick: handleDesc
  }));
};

Sorting.propTypes = process.env.NODE_ENV !== "production" ? {
  order: _propTypes["default"].oneOf([null, undefined, '', 'asc', 'desc']),
  onSort: _propTypes["default"].func.isRequired
} : {};
Sorting.defaultProps = {
  order: null,
  onSort: null
};
var _default = Sorting;
exports["default"] = _default;
module.exports = exports.default;