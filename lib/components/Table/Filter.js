"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _Badge = _interopRequireDefault(require("@material-ui/core/Badge"));

var _FilterPopper = _interopRequireDefault(require("./FilterPopper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var TableFilterImage = function TableFilterImage(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M0 0h9L6.833 6H2.167L0 0zM2 7h5v3H2z",
    fill: "#A6A6A6"
  }));
};

TableFilterImage.defaultProps = {
  width: "9",
  height: "10",
  viewBox: "0 0 9 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      cursor: 'pointer',
      '&:hover': {
        '& path': {
          fill: theme.palette.primary.main
        }
      }
    },
    active: {
      cursor: 'pointer',
      '& path': {
        fill: theme.palette.primary.main
      }
    }
  };
});
var StyledBadge = (0, _styles.withStyles)(function () {
  return {
    badge: {
      transform: 'scale(0.8)',
      right: -12,
      top: -12,
      padding: '0'
    }
  };
})(_Badge["default"]);

var Filter = function Filter(props) {
  var classes = useStyles();
  var show = props.show,
      options = props.options;

  var _useState = (0, _react.useState)(),
      anchorEl = _useState[0],
      setAnchorEl = _useState[1];

  var _useState2 = (0, _react.useState)((options || []).map(function (o) {
    return {
      checked: !!o.selected,
      label: o.label,
      value: o.value
    };
  })),
      filters = _useState2[0],
      setFilters = _useState2[1];

  var handleClick = function handleClick(e) {
    setAnchorEl(anchorEl ? null : e.currentTarget);
    e.stopPropagation();
  };

  if (show) {
    var badge = filters.filter(function (o) {
      return !!o.checked;
    }).length;
    return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(StyledBadge, {
      badgeContent: badge,
      color: "error"
    }, /*#__PURE__*/_react["default"].createElement(TableFilterImage, {
      className: badge ? classes.active : classes.root,
      onClick: handleClick
    })), /*#__PURE__*/_react["default"].createElement(_FilterPopper["default"], {
      anchorEl: anchorEl,
      filters: filters,
      onClose: function onClose() {
        setAnchorEl(null);
      },
      onFilter: function onFilter(result) {
        setFilters(result);
        setAnchorEl(null);
        props.onFilter(result);
      }
    }));
  }

  return null;
};

Filter.propTypes = process.env.NODE_ENV !== "production" ? {
  show: _propTypes["default"].bool,
  options: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    selected: _propTypes["default"].bool,
    label: _propTypes["default"].string,
    value: _propTypes["default"].any
  })).isRequired,
  onFilter: _propTypes["default"].func.isRequired
} : {};
var _default = Filter;
exports["default"] = _default;
module.exports = exports.default;