"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _styles = require("@material-ui/core/styles");

var _TableCell = _interopRequireDefault(require("@material-ui/core/TableCell"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var StickyTableCell = (0, _styles.withStyles)(function () {
  return {
    root: {
      top: 0,
      left: 0,
      position: "sticky",
      // position: '-webkit-sticky',
      zIndex: 3,
      padding: 0,
      '& th': {
        border: 'none'
      },
      '& td': {
        border: 'none'
      },
      boxShadow: '2px 0px 4px rgba(0, 0, 0, 0.15)',
      backgroundColor: 'inherit'
    }
  };
})(_TableCell["default"]);

var CMTableCell = function CMTableCell(props) {
  if (props.sticky) {
    return /*#__PURE__*/_react["default"].createElement(StickyTableCell, props);
  }

  return /*#__PURE__*/_react["default"].createElement(_TableCell["default"], props);
};

var _default = CMTableCell;
exports["default"] = _default;
module.exports = exports.default;