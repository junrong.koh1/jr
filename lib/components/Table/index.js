"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _styles = require("@material-ui/core/styles");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Table = _interopRequireDefault(require("@material-ui/core/Table"));

var _TableBody = _interopRequireDefault(require("@material-ui/core/TableBody"));

var _TableCell = _interopRequireDefault(require("@material-ui/core/TableCell"));

var _TableHead = _interopRequireDefault(require("@material-ui/core/TableHead"));

var _TableRow = _interopRequireDefault(require("@material-ui/core/TableRow"));

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _CircularProgress = _interopRequireDefault(require("@material-ui/core/CircularProgress"));

var _Pagination = _interopRequireDefault(require("../Pagination"));

var _TableCell2 = _interopRequireDefault(require("./TableCell"));

var _Filter = _interopRequireDefault(require("./Filter"));

var _Sorting = _interopRequireDefault(require("./Sorting"));

var _Checkbox = _interopRequireDefault(require("../Checkbox"));

var _lodash = _interopRequireDefault(require("lodash"));

var _theme = _interopRequireDefault(require("../../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var EmptyTableImage = function EmptyTableImage(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("rect", {
    width: "118",
    height: "118",
    rx: "2",
    fill: "#F7F7F7"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M39.78 58.476L20.97 81.883c-1.052 1.31-.12 3.253 1.558 3.253h73.535c1.565 0 2.523-1.717 1.702-3.05l-23.18-37.613a2 2 0 0 0-3.404-.002l-14.23 23.02a2 2 0 0 1-3.008.463l-11.299-9.74a2 2 0 0 0-2.865.262z",
    fill: "#DEDBDD"
  }), /*#__PURE__*/_react["default"].createElement("circle", {
    cx: "43.601",
    cy: "40.2",
    r: "8.078",
    fill: "#DEDBDD"
  }));
};

EmptyTableImage.defaultProps = {
  width: "118",
  height: "118",
  viewBox: "0 0 118 118",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    infiniteTableBody: {
      display: 'block',
      overflow: 'auto',
      '& tr': {
        display: 'table',
        width: '100%',
        tableLayout: 'fixed'
      }
    },
    infiniteTableHead: {
      display: 'table',
      width: '100%',
      tableLayout: 'fixed'
    },
    headerRow: {
      backgroundColor: theme.palette.primary.light
    },
    headerCell: {
      fontFamily: 'Noto Sans TC',
      fontWeight: 'bold',
      fontSize: '14px',
      lineHeight: '20px',
      color: theme.palette.text.primary,
      '&:first-of-type': {
        paddingLeft: '32px'
      },
      '&:last-of-type': {
        paddingRight: '32px'
      }
    },
    itemRow: {
      backgroundColor: theme.palette.background["default"],
      '&:hover': {
        backgroundColor: theme.palette.background.paper
      },
      '& td': {
        color: theme.palette.text.primary,
        '&:first-of-type': {
          paddingLeft: '32px'
        },
        '&:last-of-type': {
          paddingRight: '32px'
        }
      }
    },
    emptyTable: {
      height: 520,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      '& h6': {
        marginTop: theme.spacing(3)
      }
    },
    loading: {
      margin: 10,
      display: 'flex',
      width: 'inherit',
      justifyContent: 'center',
      '& .MuiCircularProgress-svg': {
        color: theme.palette.action.active
      }
    },
    checkboxCell: {
      width: 10,
      '& span': {
        padding: 0
      }
    }
  };
});

var CMTableHeader = function CMTableHeader(props) {
  var classes = useStyles();
  var headers = props.headers,
      sorters = props.sorters,
      filters = props.filters,
      checkAll = props.checkAll;

  if (!headers || !headers.length) {
    return null;
  }

  var isScrollY = !!props.infiniteScrollHeight;
  return /*#__PURE__*/_react["default"].createElement(_TableHead["default"], {
    className: isScrollY ? classes.infiniteTableHead : ''
  }, /*#__PURE__*/_react["default"].createElement(_TableRow["default"], {
    className: classes.headerRow
  }, props.onCheckAll ? /*#__PURE__*/_react["default"].createElement(_TableCell2["default"], {
    key: "check-box",
    className: classes.checkboxCell
  }, /*#__PURE__*/_react["default"].createElement(_Checkbox["default"], {
    theme: props.theme,
    option: {
      checked: !!checkAll
    },
    onChange: function onChange() {
      props.onCheckAll(!checkAll);
    }
  })) : null, headers.map(function (header, idx) {
    var filter = filters[idx] || null;
    var sorter = sorters[idx] || null;

    if (!header) {
      return;
    }

    var title = header.title,
        component = header.component,
        sticky = header.sticky;

    if (component) {
      return /*#__PURE__*/_react["default"].createElement(_TableCell2["default"], {
        className: classes.headerCell,
        key: idx,
        sticky: sticky
      }, component);
    }

    return /*#__PURE__*/_react["default"].createElement(_TableCell2["default"], {
      className: classes.headerCell,
      key: idx,
      style: header.style || null,
      sticky: sticky
    }, /*#__PURE__*/_react["default"].createElement("div", {
      style: {
        display: 'flex'
      }
    }, header.title, sorter ? /*#__PURE__*/_react["default"].createElement("div", {
      style: {
        marginLeft: 5
      }
    }, /*#__PURE__*/_react["default"].createElement(_Sorting["default"], {
      order: sorter.order,
      onSort: function onSort(order) {
        sorter.onSort(order);
      }
    })) : null, filter ? /*#__PURE__*/_react["default"].createElement("span", {
      style: {
        marginLeft: 5
      }
    }, /*#__PURE__*/_react["default"].createElement(_Filter["default"], {
      show: true,
      options: filter.options,
      onFilter: filter.onFilter
    })) : null));
  })));
};

var CMTable = function CMTable(props) {
  var classes = useStyles();
  var headers = props.headers,
      datalist = props.datalist,
      pageLength = props.pageLength,
      totalDataCount = props.totalDataCount;

  var _useState = (0, _react.useState)(),
      pageIndex = _useState[0],
      setPageIndex = _useState[1];

  var _useState2 = (0, _react.useState)({}),
      selected = _useState2[0],
      setSelected = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      loading = _useState3[0],
      setLoading = _useState3[1];

  var _useState4 = (0, _react.useState)(props.onCheckItem ? false : undefined),
      checkAll = _useState4[0],
      setCheckAll = _useState4[1];

  var _useState5 = (0, _react.useState)(props.onCheckItem ? [] : undefined),
      checkList = _useState5[0],
      setCheckList = _useState5[1];

  var totalPage = Math.ceil((totalDataCount || datalist.length) / pageLength);
  var startIndex = pageLength * (pageIndex - 1);
  var endIndex = startIndex + pageLength - 1;

  if (endIndex >= datalist.length) {
    endIndex = datalist.length - 1;
  }

  var showList = datalist.length ? datalist.slice(startIndex, endIndex + 1) : null;
  (0, _react.useEffect)(function () {
    if (loading) {
      setLoading(false);
    }

    if (showList) {
      if (!showList[0]) {
        setLoading(true);
      }

      if (props.readMoreData) {
        var limit, offset;

        if (showList.length === 0) {
          offset = startIndex;
          limit = pageLength;
        } else if (showList.length < pageLength) {
          offset = startIndex + showList.length;
          limit = pageLength - showList.length + pageLength;
        }

        if (offset && limit) {
          setLoading(true);
          props.readMoreData(limit, offset);
        }
      }
    }
  }, [showList]);
  (0, _react.useEffect)(function () {
    if (props.currentPage) {
      setPageIndex(props.currentPage);
    }
  }, [props.currentPage]);
  (0, _react.useEffect)(function () {
    if (props.checkItems) {
      setCheckList(props.checkItems);
    }
  }, [props.checkItems]);
  (0, _react.useEffect)(function () {
    if (checkList === null || checkList === void 0 ? void 0 : checkList.length) {
      var check = true;

      for (var i = startIndex; i <= endIndex; i++) {
        check = check && !!checkList[i];
      }

      setCheckAll(check);
    } else {
      setCheckAll(false);
    }

    if (!props.checkItems && props.onCheckItem) {
      props.onCheckItem(checkList);
    }
  }, [checkList]);
  (0, _react.useEffect)(function () {
    if (checkList === null || checkList === void 0 ? void 0 : checkList.length) {
      var check = true;

      for (var i = startIndex; i <= endIndex; i++) {
        check = check && !!checkList[i];
      }

      setCheckAll(check);
    } else {
      setCheckAll(false);
    }

    if (!props.currentPage && props.onChangePage) {
      props.onChangePage(pageIndex);
    }
  }, [pageIndex]);
  (0, _react.useEffect)(function () {
    if (pageIndex > totalPage) {
      if (props.currentPage && props.onChangePage) {
        props.onChangePage(1);
      } else {
        setPageIndex(1);
      }
    }
  }, [pageIndex, totalPage]);

  var handlePageClick = function handlePageClick(data) {
    var page = data.selected + 1;

    if (props.currentPage && props.onChangePage) {
      props.onChangePage(page);
    } else {
      setPageIndex(page);
    }
  };

  var onClickRow = function onClickRow(item) {
    if (props.onClickItem) {
      props.onClickItem(item);
    }

    if (selected.id === item.id) {
      setSelected({});
    } else {
      setSelected(item);
    }
  };

  var onCheckAll = function onCheckAll(checkAll) {
    if (showList) {
      for (var i = startIndex; i <= endIndex; i++) {
        checkList[i] = !!checkAll;
      }

      if (props.checkItems && props.onCheckItem) {
        props.onCheckItem([].concat(checkList));
      } else {
        setCheckList([].concat(checkList));
      }
    }
  };

  var isScrollY = !!props.infiniteScrollHeight;
  var theme = props.theme || _theme["default"];
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/_react["default"].createElement(_Table["default"], {
    className: props.className
  }, /*#__PURE__*/_react["default"].createElement(CMTableHeader, _extends({}, props, {
    onCheckAll: checkList ? onCheckAll : null,
    checkAll: checkAll
  })), /*#__PURE__*/_react["default"].createElement(_TableBody["default"], {
    className: isScrollY ? classes.infiniteTableBody : '',
    style: {
      maxHeight: isScrollY ? props.infiniteScrollHeight : 'default'
    }
  }, !showList ? /*#__PURE__*/_react["default"].createElement(_TableRow["default"], {
    className: classes.itemRow
  }, /*#__PURE__*/_react["default"].createElement(_TableCell["default"], {
    colSpan: headers ? headers.length : 1
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: classes.emptyTable
  }, props.emptyImage || /*#__PURE__*/_react["default"].createElement(EmptyTableImage, null), /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
    variant: "h6"
  }, props.emptyMessage || 'Oops! Something wrong happen.')))) : showList.map(function (item, index) {
    if (!item) {
      return null;
    }

    if (props.renderRow) {
      return props.renderRow(item);
    }

    if (props.renderCells) {
      return /*#__PURE__*/_react["default"].createElement(_TableRow["default"], {
        className: classes.itemRow,
        key: item.id || index,
        onClick: function onClick() {
          onClickRow(item);
        },
        style: {
          cursor: props.onClickItem ? 'pointer' : 'default',
          backgroundColor: checkList && checkList[startIndex + index] ? theme.palette.action.selected : theme.palette.background.paper
        }
      }, checkList ? /*#__PURE__*/_react["default"].createElement(_TableCell["default"], {
        key: "check-box",
        className: classes.checkboxCell
      }, /*#__PURE__*/_react["default"].createElement(_Checkbox["default"], {
        theme: props.theme,
        option: {
          checked: checkList[startIndex + index]
        },
        onChange: function onChange(e) {
          e.stopPropagation();
          checkList[startIndex + index] = !checkList[startIndex + index];

          if (props.checkItems && props.onCheckItem) {
            props.onCheckItem([].concat(checkList));
          } else {
            setCheckList([].concat(checkList));
          }
        }
      })) : null, props.renderCells(item));
    }
  }))), loading ? /*#__PURE__*/_react["default"].createElement("div", {
    className: classes.loading
  }, /*#__PURE__*/_react["default"].createElement(_CircularProgress["default"], {
    color: "inherit"
  })) : null, /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
    container: true,
    justify: "flex-end"
  }, /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
    item: true,
    sm: 12,
    md: 7
  }, /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
    container: true,
    justify: "flex-end"
  }, props.pagination || (totalPage > 1 ? /*#__PURE__*/_react["default"].createElement(_Pagination["default"], _extends({
    theme: props.theme || _theme["default"],
    pageCount: totalPage,
    forcePage: pageIndex - 1,
    onPageChange: handlePageClick
  }, props.paginationProps)) : null)))));
};

CMTable.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** Table headers */
  headers: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    title: _propTypes["default"].string.isRequired,
    style: _propTypes["default"].object,
    component: _propTypes["default"].node
  })).isRequired,

  /** Source data array. */
  datalist: _propTypes["default"].arrayOf(_propTypes["default"].object).isRequired,

  /** Render function of customized row */
  renderRow: _propTypes["default"].func,

  /** Render function of cells in row */
  renderCells: _propTypes["default"].func,

  /** Fetch function to read more data. */
  readMoreData: _propTypes["default"].func,

  /** How many rows to show in one page. */
  pageLength: _propTypes["default"].number,

  /** The amount of ALL data for rendering pagination and show total page. */
  totalDataCount: _propTypes["default"].number,

  /** Force table to show specified page. */
  currentPage: _propTypes["default"].number,

  /** Force check box of row. */
  checkItems: _propTypes["default"].arrayOf(_propTypes["default"].bool),

  /** Callback function when page changed. */
  onChangePage: _propTypes["default"].func,

  /** Callback function when click row. */
  onClickItem: _propTypes["default"].func,

  /** Callback function when check row. */
  onCheckItem: _propTypes["default"].func,

  /** Sorter for columns */
  sorters: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    onSort: _propTypes["default"].func,
    order: _propTypes["default"].oneOf(['asc', 'desc', ''])
  })),

  /** Filters for columns */
  filters: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    onFilter: _propTypes["default"].func,
    options: _propTypes["default"].arrayOf(_propTypes["default"].shape({
      selected: _propTypes["default"].bool,
      label: _propTypes["default"].string,
      value: _propTypes["default"].any
    }))
  })),

  /** Message when table is empty */
  emptyMessage: _propTypes["default"].string,

  /** Image of empty table */
  emptyImage: _propTypes["default"].node,

  /** Materail UI class from makeStyles for customized style */
  className: _propTypes["default"].string,

  /** Customized pagination*/
  pagination: _propTypes["default"].node,

  /** Props applied to the pagination component */
  paginationProps: _propTypes["default"].object,

  /** Max height of infinite scroll table body */
  infiniteScrollHeight: _propTypes["default"].number
} : {};
CMTable.defaultProps = {
  headers: [],
  datalist: [],
  pageLength: 10,
  currentPage: 1,
  sorters: [],
  filters: []
};
var _default = CMTable;
exports["default"] = _default;
module.exports = exports.default;