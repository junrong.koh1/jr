"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _reactDatepicker = _interopRequireWildcard(require("react-datepicker"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _styles = require("@material-ui/core/styles");

var _InputLabel = _interopRequireDefault(require("@material-ui/core/InputLabel"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _clsx2 = _interopRequireDefault(require("clsx"));

var _theme = _interopRequireDefault(require("../theme"));

var _TextInput = _interopRequireDefault(require("./TextInput"));

var _IconButton = _interopRequireDefault(require("./IconButton"));

var _moment = _interopRequireDefault(require("moment"));

var _zhTW = _interopRequireDefault(require("date-fns/locale/zh-TW"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var AngleLeftIcon = function AngleLeftIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M.186 4.642L5.614.154c.248-.205.64-.205.867 0 .248.204.248.529 0 .716L1.486 5l4.995 4.13c.248.204.248.529 0 .716a.7.7 0 0 1-.434.154.7.7 0 0 1-.433-.154L.186 5.358c-.248-.204-.248-.512 0-.716z",
    fill: "#000910"
  }));
};

AngleLeftIcon.defaultProps = {
  width: "7",
  height: "10",
  viewBox: "0 0 7 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var DoubleAngleLeftIcon = function DoubleAngleLeftIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M5.186 4.642L10.614.154c.248-.205.64-.205.867 0 .248.204.248.529 0 .716L6.486 5l4.995 4.13c.248.204.248.529 0 .716a.7.7 0 0 1-.433.154.701.701 0 0 1-.434-.154L5.186 5.358c-.248-.204-.248-.512 0-.716z",
    fill: "#000910"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M.186 4.642L5.614.154c.248-.205.64-.205.867 0 .248.204.248.529 0 .716L1.486 5l4.995 4.13c.248.204.248.529 0 .716a.7.7 0 0 1-.433.154.7.7 0 0 1-.434-.154L.186 5.358c-.248-.204-.248-.512 0-.716z",
    fill: "#000910"
  }));
};

DoubleAngleLeftIcon.defaultProps = {
  width: "12",
  height: "10",
  viewBox: "0 0 12 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var AngleRightIcon = function AngleRightIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M6.48 5.358L1.054 9.846c-.248.205-.64.205-.867 0-.248-.204-.248-.529 0-.716L5.18 5 .186.87C-.062.666-.062.341.186.154A.7.7 0 0 1 .619 0a.7.7 0 0 1 .434.154L6.48 4.642c.248.204.248.512 0 .716z",
    fill: "#000910"
  }));
};

AngleRightIcon.defaultProps = {
  width: "7",
  height: "10",
  viewBox: "0 0 7 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var DoubleAngleRightIcon = function DoubleAngleRightIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M1.053 9.846L6.48 5.358c.248-.204.248-.512 0-.716L1.053.154A.7.7 0 0 0 .619 0a.7.7 0 0 0-.433.154C-.062.34-.062.666.186.87L5.18 5 .186 9.13c-.248.187-.248.512 0 .716.227.205.619.205.867 0zm5 0l5.428-4.488c.248-.204.248-.512 0-.716L6.053.154A.7.7 0 0 0 5.619 0a.7.7 0 0 0-.433.154c-.248.187-.248.512 0 .716L10.18 5 5.186 9.13c-.248.187-.248.512 0 .716.227.205.619.205.867 0z",
    fill: "#000910"
  }));
};

DoubleAngleRightIcon.defaultProps = {
  width: "12",
  height: "10",
  viewBox: "0 0 12 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
(0, _reactDatepicker.registerLocale)('zh-TW', _zhTW["default"]);
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      minWidth: 180,
      display: 'inline-block',
      '& .react-datepicker-popper': {
        zIndex: 10
      },
      '& .react-datepicker__input-container': {
        '& .react-datepicker__close-icon::after': {
          color: theme.palette.primary.main,
          backgroundColor: theme.palette.background["default"],
          borderRadius: 5
        }
      },
      '& .react-datepicker': {
        border: 'none',
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.background["default"],
        fontSize: '0.8125rem',
        fontFamily: '"Noto Sans TC", sans-serif',
        boxShadow: '0 0 20px 0 rgba(0, 0, 0, 0.1)',
        '& .react-datepicker__header': {
          backgroundColor: theme.palette.background["default"],
          borderBottom: 'none',
          padding: 0,
          '& .react-datepicker__day-name': {
            width: '2rem',
            lineHeight: '2rem',
            margin: '0.15625rem',
            color: theme.palette.text.secondary
          }
        },
        '& .react-datepicker__year': {
          margin: '0 0.84375rem 0.84375rem 0.84375rem',
          '& .react-datepicker__year-wrapper': {
            maxWidth: '15rem',
            textAlign: 'center',
            display: 'flex',
            flexWrap: 'wrap',
            '& .react-datepicker__year-text': {
              cursor: 'pointer',
              borderRadius: '3.4375rem',
              width: '3.4375rem',
              boxSizing: 'content-box',
              margin: '0.15625rem',
              padding: '0.4375rem 0.625rem',
              '&:hover': {
                backgroundColor: theme.palette.action.hover
              },
              '&.react-datepicker__year-text--today': {
                color: theme.palette.primary.main
              },
              '&.react-datepicker__year-text--selected': {
                backgroundColor: theme.palette.primary.main,
                color: theme.palette.primary.contrastText,
                '&.react-datepicker__year-text--keyboard-selected': {
                  backgroundColor: theme.palette.primary.main
                }
              },
              '&.react-datepicker__year-text--disabled': {
                color: '#CBCCD0',
                pointerEvents: 'none'
              },
              '&.react-datepicker__year-text--keyboard-selected': {
                color: theme.palette.primary.contrastText,
                backgroundColor: theme.palette.primary.light,
                '&:hover': {
                  backgroundColor: theme.palette.primary.dark
                }
              }
            }
          }
        },
        '& .react-datepicker__month': {
          margin: '0 0.84375rem 0.84375rem 0.84375rem',
          '& .react-datepicker__month-wrapper': {
            marginBottom: theme.spacing(1),
            '&:last-of-type': {
              marginBottom: 0
            },
            '& .react-datepicker__month-text': {
              borderRadius: '3.4375rem',
              width: '3.4375rem',
              boxSizing: 'content-box',
              margin: '0.15625rem',
              padding: '0.4375rem 0.625rem',
              '&:hover': {
                backgroundColor: theme.palette.action.hover
              },
              '&.react-datepicker__month--selected': {
                backgroundColor: theme.palette.primary.main,
                color: theme.palette.primary.contrastText,
                '&.react-datepicker__month-text--keyboard-selected': {
                  backgroundColor: theme.palette.primary.main
                }
              },
              '&.react-datepicker__month--disabled': {
                color: theme.palette.text.disabled,
                pointerEvents: 'none'
              },
              '&.react-datepicker__month-text--keyboard-selected': {
                color: theme.palette.primary.contrastText,
                backgroundColor: theme.palette.primary.light,
                '&:hover': {
                  backgroundColor: theme.palette.primary.dark
                }
              }
            }
          },
          '& .react-datepicker__quarter-wrapper': {
            '& .react-datepicker__quarter-text': {
              borderRadius: '2.25rem',
              width: '2.25rem',
              boxSizing: 'content-box',
              margin: '0.15625rem',
              padding: '0.4375rem 0.625rem',
              '&:hover': {
                backgroundColor: theme.palette.action.hover
              },
              '&.react-datepicker__quarter--selected': {
                backgroundColor: theme.palette.primary.main,
                color: theme.palette.primary.contrastText,
                '&.react-datepicker__quarter-text--keyboard-selected': {
                  backgroundColor: theme.palette.primary.main
                }
              },
              '&.react-datepicker__quarter--disabled': {
                color: theme.palette.text.disabled,
                pointerEvents: 'none'
              },
              '&.react-datepicker__quarter-text--keyboard-selected': {
                color: theme.palette.primary.contrastText,
                backgroundColor: theme.palette.primary.light,
                '&:hover': {
                  backgroundColor: theme.palette.primary.dark
                }
              }
            }
          },
          '& .react-datepicker__week': {
            '& .react-datepicker__day': {
              width: '2rem',
              lineHeight: '2rem',
              margin: '0.15625rem',
              borderRadius: '50%',
              color: theme.palette.text.primary,
              outline: 'none',
              '&:hover': {
                backgroundColor: theme.palette.action.hover
              },
              '&.react-datepicker__day--keyboard-selected': {
                backgroundColor: theme.palette.primary.light,
                '&:hover': {
                  color: theme.palette.primary.contrastText,
                  backgroundColor: theme.palette.primary.dark
                }
              },
              '&.react-datepicker__day--today': {
                color: theme.palette.primary.main,
                '&.react-datepicker__day--in-selecting-range': {
                  color: theme.palette.primary.contrastText
                },
                '&.react-datepicker__day--keyboard-selected': {
                  backgroundColor: 'transparent'
                }
              },
              '&.react-datepicker__day--selected, &.react-datepicker__day--in-range': {
                color: theme.palette.primary.contrastText,
                backgroundColor: theme.palette.primary.main
              },
              '&.react-datepicker__day--in-selecting-range': {
                '&.react-datepicker__day--selecting-range-start, &.react-datepicker__day--selecting-range-end': {
                  backgroundColor: theme.palette.primary.main
                },
                backgroundColor: theme.palette.primary.light
              },
              '&.react-datepicker__day--outside-month': {
                color: '#B3B3B3',
                '&.react-datepicker__day--selected': {
                  color: theme.palette.primary.contrastText,
                  backgroundColor: theme.palette.primary.light
                }
              },
              '&.react-datepicker__day--disabled': {
                color: theme.palette.text.disabled,
                pointerEvents: 'none'
              }
            }
          },
          '&.react-datepicker__month--selecting-range': {
            '& .react-datepicker__week': {
              '& .react-datepicker__day': {
                '&.react-datepicker__day--in-range': {
                  backgroundColor: theme.palette.primary.light
                },
                '&.react-datepicker__day--in-range:not(.react-datepicker__day--in-selecting-range)': {
                  color: theme.palette.common.black,
                  backgroundColor: theme.palette.primary.light
                }
              }
            }
          }
        },
        '& .react-datepicker__time-container': {
          width: 90,
          '& .react-datepicker__header--time': {
            paddingTop: 6,
            paddingBottom: 6,
            '& .react-datepicker-time__header': {
              fontSize: '0.9375rem'
            }
          },
          '& .react-datepicker__time': {
            '& .react-datepicker__time-box': {
              width: 90,
              '& .react-datepicker__time-list': {
                '&::-webkit-scrollbar': {
                  display: 'none'
                },
                '-ms-overflow-style': 'none',
                '& .react-datepicker__time-list-item': {
                  height: 25,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  '&:hover': {
                    backgroundColor: theme.palette.primary.light
                  },
                  '&.react-datepicker__time-list-item--selected': {
                    backgroundColor: theme.palette.primary.main
                  }
                }
              }
            }
          }
        }
      }
    },
    header: {
      margin: '0.625rem 1.285rem 0.25rem 1.285rem',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& h6': {
        color: theme.palette.text.primary,
        fontSize: '0.875rem',
        fontWeight: 500,
        fontFamily: '"Noto Sans TC", sans-serif',
        padding: '0 30px',
        flexGrow: 1
      },
      '& .icon': {
        height: 10,
        width: 10,
        '& path': {
          fill: theme.palette.primary.main
        }
      }
    },
    arrowButton: {
      minWidth: 20,
      '&:hover': {
        backgroundColor: theme.palette.action.hover
      }
    },
    inputLabel: {
      transition: 'none',
      color: theme.palette.text.primary,
      fontSize: 18
    }
  };
});

var CMDatePicker = function CMDatePicker(props) {
  var _clsx;

  var classes = useStyles();
  var theme = props.theme || _theme["default"];

  var label = props.label,
      className = props.className,
      textInputProps = props.textInputProps,
      selected = props.selected,
      startDate = props.startDate,
      endDate = props.endDate,
      maxDate = props.maxDate,
      minDate = props.minDate,
      _onChange = props.onChange,
      dateFormat = props.dateFormat,
      showMonthYearPicker = props.showMonthYearPicker,
      showQuarterYearPicker = props.showQuarterYearPicker,
      showYearPicker = props.showYearPicker,
      arrowLabels = props.arrowLabels,
      _props$yearItemNumber = props.yearItemNumber,
      yearItemNumber = _props$yearItemNumber === void 0 ? 12 : _props$yearItemNumber,
      others = _objectWithoutPropertiesLoose(props, ["label", "className", "textInputProps", "selected", "startDate", "endDate", "maxDate", "minDate", "onChange", "dateFormat", "showMonthYearPicker", "showQuarterYearPicker", "showYearPicker", "arrowLabels", "yearItemNumber"]);

  var CustomInput = (0, _react.forwardRef)(function (_ref2, _ref) {
    var value = _ref2.value,
        onClick = _ref2.onClick;
    return /*#__PURE__*/_react["default"].createElement(_TextInput["default"], _extends({
      defaultValue: value,
      theme: theme,
      inputProps: {
        ref: _ref,
        style: {
          marginTop: 0,
          textAlign: 'center',
          cursor: 'pointer'
        }
      },
      onFocus: onClick
    }, textInputProps));
  });

  var getYearsPeriod = function getYearsPeriod(date, itemNumber) {
    var currentDate = new Date(date);
    var endYear = Math.ceil(currentDate.getFullYear() / itemNumber) * itemNumber;
    var startYear = endYear - (itemNumber - 1);
    return {
      startYear: startYear,
      endYear: endYear
    };
  };

  var customHeader = function customHeader(props) {
    var date = props.date,
        decreaseMonth = props.decreaseMonth,
        increaseMonth = props.increaseMonth,
        prevMonthButtonDisabled = props.prevMonthButtonDisabled,
        nextMonthButtonDisabled = props.nextMonthButtonDisabled;
    var decreaseYear = props.decreaseYear,
        increaseYear = props.increaseYear,
        prevYearButtonDisabled = props.prevYearButtonDisabled,
        nextYearButtonDisabled = props.nextYearButtonDisabled;
    var d = (0, _moment["default"])(date);
    var varianted = showMonthYearPicker || showQuarterYearPicker || showYearPicker;
    var yearsPeriod = getYearsPeriod(date, yearItemNumber);
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: classes.header
    }, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      theme: theme,
      variant: "text",
      onClick: decreaseYear,
      disabled: prevYearButtonDisabled,
      className: classes.arrowButton
    }, prevYearButtonDisabled ? null : varianted ? (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.left) || /*#__PURE__*/_react["default"].createElement(AngleLeftIcon, {
      className: "icon"
    }) : (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.doubleLeft) || /*#__PURE__*/_react["default"].createElement(DoubleAngleLeftIcon, {
      className: "icon"
    })), varianted ? /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
      variant: "h6"
    }, showYearPicker ? yearsPeriod.startYear + " - " + yearsPeriod.endYear : d.year()) : /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      theme: theme,
      variant: "text",
      onClick: decreaseMonth,
      disabled: prevMonthButtonDisabled,
      className: classes.arrowButton
    }, prevMonthButtonDisabled ? null : (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.left) || /*#__PURE__*/_react["default"].createElement(AngleLeftIcon, {
      className: "icon"
    })), /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
      variant: "h6"
    }, d.format('MMM YYYY')), /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      theme: theme,
      variant: "text",
      onClick: increaseMonth,
      disabled: nextMonthButtonDisabled,
      className: classes.arrowButton
    }, nextMonthButtonDisabled ? null : (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.right) || /*#__PURE__*/_react["default"].createElement(AngleRightIcon, {
      className: "icon"
    }))), /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      theme: theme,
      variant: "text",
      disabled: nextYearButtonDisabled,
      onClick: increaseYear,
      className: classes.arrowButton
    }, nextYearButtonDisabled ? null : varianted ? (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.right) || /*#__PURE__*/_react["default"].createElement(AngleRightIcon, {
      className: "icon"
    }) : (arrowLabels === null || arrowLabels === void 0 ? void 0 : arrowLabels.doubleRight) || /*#__PURE__*/_react["default"].createElement(DoubleAngleRightIcon, {
      className: "icon"
    })));
  };

  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, label ? /*#__PURE__*/_react["default"].createElement(_InputLabel["default"], {
    shrink: true,
    className: classes.inputLabel
  }, label) : null, /*#__PURE__*/_react["default"].createElement("div", {
    className: (0, _clsx2["default"])(classes.root, (_clsx = {}, _clsx[className] = !!className, _clsx))
  }, /*#__PURE__*/_react["default"].createElement(_reactDatepicker["default"], _extends({
    showPopperArrow: false,
    customInput: /*#__PURE__*/_react["default"].createElement(CustomInput, null),
    renderCustomHeader: customHeader,
    popperPlacement: "bottom-start",
    popperModifiers: {
      offset: {
        enabled: true,
        offset: '0px, 5px'
      }
    },
    onChange: function onChange(date) {
      if (_onChange) {
        _onChange(date);
      }
    },
    dateFormat: dateFormat || "yyyy/MM/dd",
    selected: selected,
    startDate: startDate,
    endDate: endDate,
    maxDate: maxDate,
    minDate: minDate,
    showMonthYearPicker: showMonthYearPicker,
    showQuarterYearPicker: showQuarterYearPicker,
    showYearPicker: showYearPicker
  }, others))));
};

var DatePicker = function DatePicker(props) {
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: props.theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(CMDatePicker, props));
};

DatePicker.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** Label text. */
  label: _propTypes["default"].string,

  /** Add style to DatePicker container **/
  className: _propTypes["default"].string,

  /** Add style to DatePicker input **/
  textInputProps: _propTypes["default"].shape({
    /** Input with error. */
    error: _propTypes["default"].bool,

    /** Helper text of input. */
    helperText: _propTypes["default"].string
  }),

  /** Selected date. */
  selected: _propTypes["default"].instanceOf(Date),

  /** Used for setting start date of selected range. */
  startDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting end date of selected range. */
  endDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting start date of available date range. */
  minDate: _propTypes["default"].instanceOf(Date),

  /** Used for setting end of available date range. */
  maxDate: _propTypes["default"].instanceOf(Date),

  /** Format of selected date on input. */
  dateFormat: _propTypes["default"].string,

  /** Callback function when click date. */
  onChange: _propTypes["default"].func.isRequired,

  /** Used for setting variant of DatePicker **/
  showMonthYearPicker: _propTypes["default"].bool,

  /** Used for setting variant of DatePicker **/
  showQuarterYearPicker: _propTypes["default"].bool,

  /** Used for setting variant of DatePicker **/
  showYearPicker: _propTypes["default"].bool,

  /** Used for setting custom arrow labels */
  arrowLabels: _propTypes["default"].shape({
    left: _propTypes["default"].element,
    doubleLeft: _propTypes["default"].element,
    right: _propTypes["default"].element,
    doubleRight: _propTypes["default"].element
  }),

  /** Used for setting number of year items per page in YearPicker */
  yearItemNumber: _propTypes["default"].number
} : {};
DatePicker.defaultProps = {
  dateFormat: "yyyy/MM/dd",
  yearItemNumber: 12
};
var _default = DatePicker;
exports["default"] = _default;
module.exports = exports.default;