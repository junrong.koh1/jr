"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _styles = require("@material-ui/core/styles");

var _TextField = _interopRequireDefault(require("@material-ui/core/TextField"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _clsx2 = _interopRequireDefault(require("clsx"));

var _theme = _interopRequireDefault(require("../theme"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  var _hover;

  return {
    inputLabel: {
      transition: 'none',
      color: theme.palette.text.primary,
      fontSize: 18
    },
    inputRoot: {
      fontSize: 15,
      border: '1px solid',
      borderColor: theme.palette.action.active,
      '&:hover': {
        border: '1.4px solid',
        borderColor: theme.palette.primary.main
      },
      '&:focus': {
        border: '1.4px solid',
        borderColor: theme.palette.primary.main
      },
      '&.Mui-error': {
        border: '1.4px solid',
        borderColor: theme.palette.error.main,
        '& input': {},
        '& textarea': {}
      },
      '&.Mui-disabled': {
        backgroundColor: theme.palette.background.paper,
        cursor: 'not-allowed',
        '&:hover': (_hover = {
          border: '1px solid'
        }, _hover["border"] = theme.palette.action.disabled, _hover)
      },
      '&.MuiInputBase-multiline': {
        padding: 0
      }
    },
    inputRootWithLabel: {
      marginTop: '18px !important'
    },
    input: {
      padding: '8px 10px',
      backgroundColor: theme.palette.background.paper,
      '&::placeholder': {
        /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: theme.palette.text.placeholder,
        opacity: 1
        /* Firefox */

      },
      '&:-ms-input-placeholder': {
        /* Internet Explorer 10-11 */
        color: theme.palette.text.placeholder
      },
      '&::-ms-input-placeholder': {
        /* Microsoft Edge */
        color: theme.palette.text.placeholder
      }
    }
  };
});

var CMTextInput = function CMTextInput(props) {
  var _clsx;

  var classes = useStyles();

  var children = props.children,
      InputLabelProps = props.InputLabelProps,
      InputProps = props.InputProps,
      others = _objectWithoutPropertiesLoose(props, ["children", "InputLabelProps", "InputProps"]);

  var customInputLabelProps = _extends({
    shrink: true,
    classes: {
      shrink: classes.inputLabel
    }
  }, InputLabelProps);

  var customInputProps = _extends({
    disableUnderline: true,
    classes: {
      root: (0, _clsx2["default"])(classes.inputRoot, (_clsx = {}, _clsx[classes.inputRootWithLabel] = !!props.label, _clsx)),
      input: classes.input
    }
  }, props.InputProps); // const customSelectProps = {
  //     classes: {
  //         select: classes.select,
  //         icon: classes.selectIcon,
  //         selectMenu: classes.selectMenu
  //     }
  // }


  return /*#__PURE__*/_react["default"].createElement(_TextField["default"], _extends({
    variant: "standard",
    InputLabelProps: customInputLabelProps,
    InputProps: customInputProps // SelectProps={customSelectProps}

  }, others), children);
};

var TextInput = function TextInput(props) {
  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: props.theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(CMTextInput, props));
};

TextInput.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: _propTypes["default"].string,

  /** Props for input element */
  inputProps: _propTypes["default"].any,

  /** Label text. */
  label: _propTypes["default"].string,

  /** Field value. */
  value: _propTypes["default"].any,

  /** Placeholder text. */
  placeholder: _propTypes["default"].string,

  /** Helper text of input. */
  helperText: _propTypes["default"].string,

  /** Disabled. */
  disabled: _propTypes["default"].bool,

  /** Text area. */
  multiline: _propTypes["default"].bool,

  /** Input with error. */
  error: _propTypes["default"].bool
} : {};
var _default = TextInput;
exports["default"] = _default;
module.exports = exports.default;