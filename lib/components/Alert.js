"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _Dialog = _interopRequireDefault(require("@material-ui/core/Dialog"));

var _DialogTitle = _interopRequireDefault(require("@material-ui/core/DialogTitle"));

var _DialogContent = _interopRequireDefault(require("@material-ui/core/DialogContent"));

var _DialogContentText = _interopRequireDefault(require("@material-ui/core/DialogContentText"));

var _DialogActions = _interopRequireDefault(require("@material-ui/core/DialogActions"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _reactFontawesome = require("@fortawesome/react-fontawesome");

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _Button = _interopRequireDefault(require("./Button"));

var _theme = _interopRequireDefault(require("../theme"));

var _Button2 = _interopRequireDefault(require("@material-ui/core/Button"));

var _Box = _interopRequireDefault(require("@material-ui/core/Box"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var CloseIcon = function CloseIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M8.022 6.002l3.032-3.032a.953.953 0 0 0 0-1.348l-.673-.674a.953.953 0 0 0-1.348 0L6 3.981 2.969.947a.953.953 0 0 0-1.348 0l-.674.673a.953.953 0 0 0 0 1.349L3.98 6.002.946 9.034a.953.953 0 0 0 0 1.348l.673.674a.953.953 0 0 0 1.348 0l3.034-3.033 3.032 3.033a.953.953 0 0 0 1.348 0l.673-.674a.954.954 0 0 0 0-1.348L8.022 6.002z",
    fill: "#000910"
  }));
};

CloseIcon.defaultProps = {
  width: "12",
  height: "12",
  viewBox: "0 0 12 12",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      '& .MuiDialog-paper': {
        padding: 8
      }
    },
    successTitle: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      '& svg': {
        fontSize: 25,
        marginRight: theme.spacing(3),
        color: theme.palette.success.main
      }
    },
    errorTitle: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      '& svg': {
        fontSize: 25,
        marginRight: theme.spacing(3),
        color: theme.palette.error.main
      }
    },
    closeButton: {
      backgroundColor: '#F2F2F2',
      maxWidth: 30,
      maxHeight: 30,
      minWidth: 30,
      minHeight: 30
    },
    dialogHead: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  };
});

var DialogCloseButton = function DialogCloseButton(props) {
  var onClose = props.onClose;
  var classes = useStyles();
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_Button2["default"], {
    className: classes.closeButton,
    onClick: onClose
  }, /*#__PURE__*/_react["default"].createElement(CloseIcon, null)));
};

var DialogTitle = function DialogTitle(props) {
  var classes = useStyles();

  var type = props.type,
      children = props.children,
      other = _objectWithoutPropertiesLoose(props, ["type", "children"]);

  var getIcon = function getIcon(type) {
    switch (type) {
      case 'error':
        return /*#__PURE__*/_react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
          icon: _freeSolidSvgIcons.faTimesCircle,
          className: "icon"
        });

      case 'warning':
        return /*#__PURE__*/_react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
          icon: _freeSolidSvgIcons.faExclamationCircle,
          className: "icon"
        });

      case 'question':
        return /*#__PURE__*/_react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
          icon: _freeSolidSvgIcons.faQuestionCircle,
          className: "icon"
        });

      case 'success':
      default:
        return /*#__PURE__*/_react["default"].createElement(_reactFontawesome.FontAwesomeIcon, {
          icon: _freeSolidSvgIcons.faCheckCircle,
          className: "icon"
        });
    }
  };

  return /*#__PURE__*/_react["default"].createElement(_DialogTitle["default"], _extends({
    disableTypography: true,
    className: type === 'success' ? classes.successTitle : classes.errorTitle
  }, other), getIcon(type), /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
    variant: "h4"
  }, children));
};

var DialogContentText = (0, _styles.withStyles)(function (theme) {
  return {
    root: {
      fontSize: '15px',
      fontWeight: 'normal',
      lineHeight: 'normal',
      letterSpacing: 'normal',
      color: theme.palette.text.primary,
      marginBottom: 0,
      marginLeft: theme.spacing(8)
    }
  };
})(_DialogContentText["default"]);

var Alert = function Alert(props) {
  var theme = props.theme;
  var classes = useStyles();
  var type = props.type,
      title = props.title,
      content = props.content,
      show = props.show,
      loading = props.loading,
      confirmText = props.confirmText,
      cancelText = props.cancelText,
      onConfirm = props.onConfirm,
      onClose = props.onClose,
      onCancel = props.onCancel;

  var handleClose = function handleClose() {
    onClose && onClose();
  };

  var handleConfirm = function handleConfirm() {
    onConfirm && onConfirm();
  };

  var handleCancel = function handleCancel() {
    onCancel && onCancel();
  };

  return /*#__PURE__*/_react["default"].createElement(_styles.ThemeProvider, {
    theme: theme || _theme["default"]
  }, /*#__PURE__*/_react["default"].createElement(_Dialog["default"], {
    onClose: handleClose,
    open: show,
    className: classes.root
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: classes.dialogHead
  }, /*#__PURE__*/_react["default"].createElement(DialogTitle, {
    id: "customized-dialog-title",
    onClose: handleClose,
    type: type
  }, title), onClose && /*#__PURE__*/_react["default"].createElement(DialogCloseButton, {
    onClose: handleClose
  })), /*#__PURE__*/_react["default"].createElement(_DialogContent["default"], null, /*#__PURE__*/_react["default"].createElement(DialogContentText, null, content)), /*#__PURE__*/_react["default"].createElement(_DialogActions["default"], null, onCancel && /*#__PURE__*/_react["default"].createElement(_Button["default"], {
    theme: theme || _theme["default"],
    variant: "outlined",
    disabled: loading,
    onClick: handleCancel,
    label: cancelText
  }), onConfirm && /*#__PURE__*/_react["default"].createElement(_Button["default"], {
    theme: theme || _theme["default"],
    variant: "contained",
    disabled: loading,
    onClick: handleConfirm,
    label: confirmText,
    loading: loading
  }))));
};

Alert.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: _propTypes["default"].object,

  /** Define title icon. */
  type: _propTypes["default"].oneOf(['success', 'error', 'warning', 'question']).isRequired,

  /** Title text. */
  title: _propTypes["default"].string,

  /** Content text. */
  content: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].object]),

  /** Confirm button label. */
  confirmText: _propTypes["default"].string,

  /** Cancel button label. */
  cancelText: _propTypes["default"].string,

  /** Show alert. */
  show: _propTypes["default"].bool,

  /** Disabled and show loading icon on confirm button. */
  loading: _propTypes["default"].bool,

  /** Callback function when click confirm button. */
  onConfirm: _propTypes["default"].func,

  /** Callback function when click close button (x icon). */
  onClose: _propTypes["default"].func,

  /** Callback function when click cancel button. */
  onCancel: _propTypes["default"].func
} : {};
Alert.defaultProps = {
  type: 'success',
  title: '',
  content: '',
  confirmText: 'OK',
  cancelText: 'Close',
  show: false,
  loading: false,
  onConfirm: null,
  onClose: null,
  onCancel: null
};
var _default = Alert;
exports["default"] = _default;
module.exports = exports.default;