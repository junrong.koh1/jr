import initStoryshots from '@storybook/addon-storyshots'
import ReactDOM from 'react-dom'
import React from 'react'

ReactDOM.createPortal = jest.fn((element, node) => {
    if (!element.style) {
        return React.cloneElement(element, { style: { webkitTransition: '' } })
    }
    return element
})

// Mock Mui Dialog for passing test.
// Skip Error: Uncaught [TypeError: Cannot set property 'scrollTop' of null] of react-transition-group/cjs/Transition.js
jest.mock('@material-ui/core/Dialog')
import Dialog from '@material-ui/core/Dialog'
Dialog.render = () => null

initStoryshots();