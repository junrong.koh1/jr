module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: false
  },
  babel: {
    plugins: [
      [
        "inline-react-svg",
        {
          "svgo": {
            "plugins": [
              {
                "removeAttrs": {
                  "attrs": "(data-name)"
                }
              },
              {
                "cleanupIDs": true
              }
            ]
          }
        }
      ]
    ],
  }
}
