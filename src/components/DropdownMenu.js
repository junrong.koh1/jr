import Select, { components } from 'react-select'
import CreatableSelect from 'react-select/creatable'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel';
import React, { useMemo, useEffect } from 'react'
import PropTypes from 'prop-types'
import CMTheme from '../theme'

const useStyles = makeStyles(theme => ({
    inputLabel: {
        transition: 'none',
        color: theme.palette.text.primary,
        fontSize: 18,
    },
    arrowDown: {
        width: 0,
        height: 0,
        borderLeft: '4px solid transparent',
        borderRight: '4px solid transparent',
        borderTop: `8.2px solid`,
        borderTopColor: theme.palette.text.primary,
    },
    disabledArrowDown: {
        width: 0,
        height: 0,
        borderLeft: '4px solid transparent',
        borderRight: '4px solid transparent',
        borderTop: `8.2px solid`,
        borderTopColor: theme.palette.action.disabled
    }
}))

const DropdownIndicator = (props) => {
    const classes = useStyles()
    return (
        <components.DropdownIndicator {...props} >
            <div className={
                props.isDisabled ? classes.disabledArrowDown : classes.arrowDown
            } />
        </components.DropdownIndicator>
    )
}

const makeInlineStyle = (theme, invalid) => ({
    container: (base, state) => ({
        ...base,
        pointerEvents: 'auto',
        cursor: state.isDisabled ? 'not-allowed' : 'auto',
    }),
    control: (base, state) => ({
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 2,
        outline: 0,
        border: ((state.isFocused || invalid) && '1.4px solid')
            || '1px solid'
        ,
        borderColor: (state.isFocused && theme.palette.primary.main)
            || ((state.isDisabled && theme.palette.action.disabled))
            || (invalid && theme.palette.error.main)
            || theme.palette.action.active
        ,
        position: 'relative',
        // boxSizing: 'border-box',
        '&:hover': {
            border: '1.4px solid',
            borderColor: theme.palette.primary.main,
        }
    }),
    valueContainer: (base, state) => ({
        ...base,
        padding: '6px 10px',
    }),
    indicatorSeparator: () => null,
    indicatorsContainer: (base, state) => ({
        ...base,
        paddingRight: 10
    }),
    placeholder: (base) => ({
        ...base,
        color: theme.palette.text.placeholder,
    }),
    input: (base) => ({
        ...base,
        color: theme.palette.text.primary,
        margin: 0,
        paddingBottom: 0,
        paddingTop: 0
    }),
    menu: (base) => ({
        ...base,
        borderLeft: `1.4px solid ${theme.palette.primary.main}`,
        borderRight: `1.4px solid ${theme.palette.primary.main}`,
        borderBottom: `1.4px solid ${theme.palette.primary.main}`,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2,
        zIndex: 99,
        marginTop: -3,
        boxShadow: 'none',
        backgroundColor: theme.palette.background.paper
    }),
    menuList: (base) => ({
        ...base,
        marginTop: 2,
        marginBottom: 3,
        padding: 0,
    }),
    option: (base, state) => ({
        // ...base,
        borderTop: '1px solid',
        borderTopColor: theme.palette.divider,
        padding: '8px 15px',
        backgroundColor: (state.isFocused && theme.palette.action.hover)
            || (state.isSelected && theme.palette.primary.light)
            || theme.palette.background.paper,
        color: state.isDisabled ? theme.palette.text.placeholder : theme.palette.text.primary,
        cursor: state.isDisabled ? 'not-allowed' : 'default'
    }),
    singleValue: (base, state) => ({
        ...base,
        color: state.isDisabled ? theme.palette.text.placeholder : theme.palette.text.primary
    }),
    multiValue: (base) => ({
        ...base,
        borderRadius: 5,
        overflow: 'hidden',
        marginLeft: '5px',
        '&:first-of-type': {
            marginLeft: '8px'
        },
    }),
    multiValueLabel: (base) => ({
        ...base,
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        fontSize: 12,
        padding: '1.5px 10px',
        paddingLeft: 10,
        backgroundColor: theme.palette.primary.main
    }),
    multiValueRemove: (base) => ({
        ...base,
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.dark,
        cursor: 'pointer',
        padding: 0,
        '& svg': {
            width: 16,
            height: 16
        },
        ':hover': {
            ...base,
            padding: 0,
            color: theme.palette.error.light
        }
    }),
})

const DropdownMenu = (props) => {
    const theme = props.theme || CMTheme
    const classes = useStyles()
    const {
        label,
        required,
        menuItems,
        defaultValue,
        disabled,
        multiple,
        invalid,
        creatable,
        onReachEnd,
        onClickItem,
        components,
        ...others
    } = props

    const customStyles = useMemo(() => makeInlineStyle(theme, invalid), [theme, invalid])
    const options = useMemo(() => menuItems.map(item => {
        if (item.value && item.label) {
            return item
        }
        return ({
            value: item,
            label: item
        })
    }), [menuItems])

    return (
        <>
            {
                label
                    ? <InputLabel shrink={true} required={required} className={classes.inputLabel}>{label}</InputLabel>
                    : null
            }
            {
                creatable
                    ? <CreatableSelect
                        placeholder='Select or enter new value...'
                        options={options}
                        styles={customStyles}
                        defaultValue={defaultValue}
                        isDisabled={disabled}
                        isMulti={multiple}
                        components={{
                            DropdownIndicator,
                            ...components
                        }}
                        onChange={(items) => {
                            if (onClickItem) {
                                onClickItem(items || [])
                            }
                        }}
                        onMenuScrollToBottom={() => {
                            if (onReachEnd) {
                                onReachEnd()
                            }
                        }}
                        {...others}
                    />
                    : <Select
                        options={options}
                        styles={customStyles}
                        defaultValue={defaultValue}
                        isDisabled={disabled}
                        isMulti={multiple}
                        components={{
                            DropdownIndicator,
                            ...components
                        }}
                        onChange={(items) => {
                            if (onClickItem) {
                                onClickItem(items || [])
                            }
                        }}
                        onMenuScrollToBottom={() => {
                            if (onReachEnd) {
                                onReachEnd()
                            }
                        }}
                        isClearable={false}
                        {...others}
                    />
            }

        </>
    )
}

DropdownMenu.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Label text. */
    label: PropTypes.string,
    /** Set true if want to show required indicator in label */
    required: PropTypes.bool,
    /** Set options in menu */
    menuItems: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.any,
        label: PropTypes.string
    })),
    /** Show default value. */
    defaultValue: PropTypes.shape({
        value: PropTypes.any,
        label: PropTypes.string
    }),
    /** Set true if allowed select multiple options. */
    multiple: PropTypes.bool,
    /** Set true if disabled. */
    disabled: PropTypes.bool,
    /** Set true if invalid. */
    invalid: PropTypes.bool,
    /** Set true if allow to create new option. */
    creatable: PropTypes.bool,
    /** Callback function when click option in menu. */
    onClickItem: PropTypes.func,
    /** Callback function when a menu reach the end. */
    onReachEnd: PropTypes.func,
    /** Customized component. detailed in https://react-select.com/components#components*/
    components: PropTypes.object,
}

DropdownMenu.defaultProps = {
    menuItems: [],
    defaultValue: null,
    disabled: false,
    multiple: false,
    invalid: false,
    creatable: false,
    onReachEnd: null,
    onClickItem: null,
    components: {},
}

export default DropdownMenu
