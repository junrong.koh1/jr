import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider, withStyles, makeStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogContentText from '@material-ui/core/DialogContentText'
import MuiDialogActions from '@material-ui/core/DialogActions'
import Typography from '@material-ui/core/Typography'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle, faQuestionCircle, faExclamationCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import Button from './Button'
import CMTheme from '../theme'
import CloseButton from '@material-ui/core/Button'
import CloseIcon from '../static/images/close_icon.svg'
import Box from '@material-ui/core/Box'

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiDialog-paper': {
            padding: 8
        }
    },
    successTitle: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        '& svg': {
            fontSize: 25,
            marginRight: theme.spacing(3),
            color: theme.palette.success.main
        }
    },
    errorTitle: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        '& svg': {
            fontSize: 25,
            marginRight: theme.spacing(3),
            color: theme.palette.error.main
        }
    },
    closeButton: {
        backgroundColor: '#F2F2F2',
        maxWidth: 30,
        maxHeight: 30,
        minWidth: 30,
        minHeight: 30
    },
    dialogHead: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
}))

const DialogCloseButton = props => {
    const { onClose } = props
    const classes = useStyles()
    return <>
        <CloseButton className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
        </CloseButton>
    </>
}

const DialogTitle = props => {
    const classes = useStyles()
    const { type, children, ...other } = props

    const getIcon = (type) => {
        switch (type) {
            case 'error':
                return <FontAwesomeIcon icon={faTimesCircle} className='icon' />
            case 'warning':
                return <FontAwesomeIcon icon={faExclamationCircle} className='icon' />
            case 'question':
                return <FontAwesomeIcon icon={faQuestionCircle} className='icon' />
            case 'success':
            default:
                return <FontAwesomeIcon icon={faCheckCircle} className='icon' />
        }
    }

    return (
        <MuiDialogTitle
            disableTypography
            className={(type === 'success') ? classes.successTitle : classes.errorTitle}
            {...other}
        >
            {getIcon(type)}
            <Typography variant="h4">
                {children}
            </Typography>
        </MuiDialogTitle>
    )
}

const DialogContentText = withStyles(theme => ({
    root: {
        fontSize: '15px',
        fontWeight: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: theme.palette.text.primary,
        marginBottom: 0,
        marginLeft: theme.spacing(8)
    }
}))(MuiDialogContentText)

const Alert = (props) => {
    const { theme } = props
    const classes = useStyles()
    const { type, title, content, show, loading, confirmText, cancelText, onConfirm, onClose, onCancel } = props

    const handleClose = () => {
        onClose && onClose()
    }

    const handleConfirm = () => {
        onConfirm && onConfirm()
    }
    
    const handleCancel = () => {
        onCancel && onCancel()
    }
    return (
        <ThemeProvider theme={theme || CMTheme}>
            <Dialog onClose={handleClose} open={show} className={classes.root}>
                <div className={classes.dialogHead}>
                    <DialogTitle
                        id="customized-dialog-title"
                        onClose={handleClose}
                        type={type}
                    >
                        {title}
                    </DialogTitle>
                    {onClose && <DialogCloseButton onClose={handleClose}/>}
                </div>
                <MuiDialogContent>
                    <DialogContentText>
                        {content}
                    </DialogContentText>
                </MuiDialogContent>
                <MuiDialogActions>
                    {
                        onCancel &&
                        <Button
                            theme={theme || CMTheme}
                            variant="outlined"
                            disabled={loading}
                            onClick={handleCancel}
                            label={cancelText}
                        />
                    }
                    {
                        onConfirm &&
                        <Button
                            theme={theme || CMTheme}
                            variant="contained"
                            disabled={loading}
                            onClick={handleConfirm}
                            label={confirmText}
                            loading={loading}
                        />

                    }
                </MuiDialogActions>
            </Dialog>
        </ThemeProvider>
    )
}

Alert.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Define title icon. */
    type: PropTypes.oneOf(['success', 'error', 'warning', 'question']).isRequired,
    /** Title text. */
    title: PropTypes.string,
    /** Content text. */
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    /** Confirm button label. */
    confirmText: PropTypes.string,
    /** Cancel button label. */
    cancelText: PropTypes.string,
    /** Show alert. */
    show: PropTypes.bool,
    /** Disabled and show loading icon on confirm button. */
    loading: PropTypes.bool,
    /** Callback function when click confirm button. */
    onConfirm: PropTypes.func,
    /** Callback function when click close button (x icon). */
    onClose: PropTypes.func,
    /** Callback function when click cancel button. */
    onCancel: PropTypes.func,
}
Alert.defaultProps = {
    type: 'success',
    title: '',
    content: '',
    confirmText: 'OK',
    cancelText: 'Close',
    show: false,
    loading: false,
    onConfirm: null,
    onClose: null,
    onCancel: null
}

export default Alert
