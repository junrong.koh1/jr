import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import { ThemeProvider, makeStyles } from '@material-ui/styles'
import CMTheme from '../theme'
import DatePicker from './DatePicker'
import DropdownMenu from './DropdownMenu'
import { TODAY, LAST_MONTH } from '../lib/time'

const defaultTimePeriodsLabel = {
    last5minutes: 'Last 5 minutes',
    last15minutes: 'Last 15 minutes',
    last1hours: 'Last 1 hours',
    last12hours: 'Last 12 hours',
    last24hours: 'Last 24 hours',
    last7days: 'Last 7 days',
    last30days: 'Last 30 days',
    lastmonth: 'Last month',
    thismonth: 'This month'
}

const useStyles = makeStyles(theme => ({
    wrapper: {
        display: 'flex',
        alignItems: 'center',
        '& span': {
            padding: theme.spacing(1)
        }
    },
    filter: {
        width: 165,
        marginLeft: theme.spacing(3)
    }
}))

const CMDateRangePicker = (props) => {
    const classes = useStyles()
    const theme = props.theme || CMTheme

    const { startDate, endDate, maxDate, minDate, locale, startDateChange, endDateChange } = props
    const { withTimePeriod, timePeriodPlaceholder, defaultTimePeriod, customTimePeriodLabel } = props
    const { showTimeSelect, timeIntervals, dateFormat, defaultTimeSelectionPeriod, timeCaption, inputClass } = props

    const TimePeriods = showTimeSelect ? [
        {
            value: 'last5minutes',
            label: customTimePeriodLabel.last5minutes || defaultTimePeriodsLabel.last5minutes,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 5),
            endDate: new Date()
        },
        {
            value: 'last15minutes',
            label: customTimePeriodLabel.last15minutes || defaultTimePeriodsLabel.last15minutes,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 15),
            endDate: new Date()
        },
        {
            value: 'last1hours',
            label: customTimePeriodLabel.last1hours || defaultTimePeriodsLabel.last1hours,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 60),
            endDate: new Date()
        },
        {
            value: 'last12hours',
            label: customTimePeriodLabel.last12hours || defaultTimePeriodsLabel.last12hours,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 12),
            endDate: new Date()
        },
        {
            value: 'last24hours',
            label: customTimePeriodLabel.last24hours || defaultTimePeriodsLabel.last24hours,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 24),
            endDate: new Date()
        },
        {
            value: 'last7days',
            label: customTimePeriodLabel.last7days || defaultTimePeriodsLabel.last7days,
            startDate: new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 7),
            endDate: new Date()
        }
    ]
        : [
            {
                value: 'last7days',
                label: customTimePeriodLabel.last7days || defaultTimePeriodsLabel.last7days,
                startDate: TODAY.afterDays(-6).startOf('day'),
                endDate: TODAY.endOf('day')
            },
            {
                value: 'last30days',
                label: customTimePeriodLabel.last30days || defaultTimePeriodsLabel.last30days,
                startDate: TODAY.afterDays(-29).startOf('day'),
                endDate: TODAY.endOf('day')
            },
            {
                value: 'lastmonth',
                label: customTimePeriodLabel.lastmonth || defaultTimePeriodsLabel.lastmonth,
                startDate: LAST_MONTH.startOf('month'),
                endDate: LAST_MONTH.endOf('month')
            },
            {
                value: 'thismonth',
                label: customTimePeriodLabel.thismonth || defaultTimePeriodsLabel.thismonth,
                startDate: TODAY.startOf('month'),
                endDate: TODAY.endOf('day')
            }
        ]

    const selectedTimePeriod = withTimePeriod && TimePeriods.find(t => t.value === (showTimeSelect ? defaultTimeSelectionPeriod : defaultTimePeriod))
    const [filterValue, setFilterValue] = useState(selectedTimePeriod || null)


    useEffect(() => {
        if (filterValue) {
            startDateChange(filterValue.startDate)
            endDateChange(filterValue.endDate)
        }
    }, [filterValue])

    const onStartDateChange = (date) => {
        const selectedStartDate = showTimeSelect ? date : date.startOf('day')
        const matchedTimePeriod = TimePeriods.find(t => t.startDate.getTime() === selectedStartDate.getTime() && t.endDate.getTime() === endDate.getTime())
        if (matchedTimePeriod) {
            setFilterValue(matchedTimePeriod)
        } else {
            startDateChange(selectedStartDate)
            if (selectedStartDate > endDate) {
                endDateChange(showTimeSelect ? new Date(date.getTime() + 1000 * 60 * timeIntervals) : date.endOf('day'))
            }
            setFilterValue(null)
        }
    }

    const onEndDateChange = (date) => {
        const selectedEndDate = showTimeSelect ? date : date.endOf('day')
        const matchedTimePeriod = TimePeriods.find(t => t.startDate.getTime() === startDate.getTime() && t.endDate.getTime() === selectedEndDate.getTime())
        if (matchedTimePeriod) {
            setFilterValue(matchedTimePeriod)
        } else {
            setFilterValue(null)
            if (showTimeSelect && selectedEndDate < startDate) {
                return endDateChange(new Date(startDate.getTime() + 1000 * 60 * timeIntervals))
            }
            endDateChange(selectedEndDate)
        }
    }

    return (
        <div className={classes.wrapper}>
            <DatePicker
                theme={theme}
                selectsStart={true}
                selected={startDate}
                startDate={startDate}
                endDate={endDate}
                onChange={onStartDateChange}
                minDate={minDate}
                maxDate={maxDate}
                locale={locale}
                showTimeSelect={showTimeSelect}
                timeIntervals={timeIntervals}
                dateFormat={dateFormat}
                timeCaption={timeCaption}
                className={inputClass || null}
            />
            <span>~</span>
            <DatePicker
                theme={theme}
                selectsEnd
                selected={endDate}
                startDate={startDate}
                endDate={endDate}
                onChange={onEndDateChange}
                minDate={startDate}
                maxDate={maxDate}
                locale={locale}
                showTimeSelect={showTimeSelect}
                timeIntervals={timeIntervals}
                dateFormat={dateFormat}
                timeCaption={timeCaption}
                className={inputClass || null}
            />
            {withTimePeriod &&
                <DropdownMenu
                    theme={theme}
                    className={classes.filter}
                    menuItems={TimePeriods}
                    value={filterValue}
                    onClickItem={(item) => {
                        setFilterValue(item)
                    }}
                    placeholder={timePeriodPlaceholder} />
            }
        </div>
    )
}

const DateRangePicker = (props) => (
    <ThemeProvider theme={props.theme || CMTheme}>
        <CMDateRangePicker {...props} />
    </ThemeProvider>
)

DateRangePicker.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Used for setting start date of selected range. */
    startDate: PropTypes.instanceOf(Date),
    /** Used for setting end date of selected range. */
    endDate: PropTypes.instanceOf(Date),
    /** Used for setting start date of available date range. */
    minDate: PropTypes.instanceOf(Date),
    /** Used for setting end of available date range. */
    maxDate: PropTypes.instanceOf(Date),
    /** Setup calendar language of month and week day. */
    locale: PropTypes.string,
    /** Callback function when start date changed. */
    startDateChange: PropTypes.func.isRequired,
    /** Callback function when end date changed. */
    endDateChange: PropTypes.func.isRequired,
    /** Show time periods button. */
    withTimePeriod: PropTypes.bool,
    /** Placeholder of dropdown menu of time periods. */
    timePeriodPlaceholder: PropTypes.string,
    /** Show time selection column in datePicker popper. */
    showTimeSelect: PropTypes.bool,
    /** Time intervals for time selection values. */
    timeIntervals: PropTypes.number,
    /** Default value of time periods for pickers without time selection. */
    defaultTimePeriod: PropTypes.oneOf(['last7days', 'last30days', 'lastmonth', 'thismonth']),
    /** Default value of time periods for pickers with time selection. */
    defaultTimeSelectionPeriod: PropTypes.oneOf(['last5minutes', 'last15minutes', 'last1hours', 'last12hours', 'last24hours', 'last7days']),
    /** Customize label of time periods. */
    customTimePeriodLabel: PropTypes.shape({
        last5minutes: PropTypes.string,
        last15minutes: PropTypes.string,
        last1hours: PropTypes.string,
        last12hours: PropTypes.string,
        last24hours: PropTypes.string,
        last7days: PropTypes.string,
        last30days: PropTypes.string,
        lastmonth: PropTypes.string,
        thismonth: PropTypes.string
    }),
    /** Customize time format in input. */
    dateFormat: PropTypes.string,
    /** Customize title of time selection column */
    timeCaption: PropTypes.string,
    /** Add style to DatePicker container (inputs) **/
    inputClass: PropTypes.string
}

DateRangePicker.defaultProps = {
    withTimePeriod: false,
    timeIntervals: 5,
    defaultTimePeriod: 'last7days',
    defaultTimeSelectionPeriod: 'last5minutes',
    timePeriodPlaceholder: 'Time Period',
    customTimePeriodLabel: defaultTimePeriodsLabel
}

export default DateRangePicker