import React from 'react';
import PropTypes from 'prop-types'
import { ThemeProvider, withStyles } from '@material-ui/core/styles'
import MuiCheckbox from '@material-ui/core/Checkbox'
import { ReactComponent as CheckboxIcon } from '../../static/images/checkbox.svg'
import CMTheme from '../../theme'

const StyledCheckbox = withStyles(theme => ({
    root: {
        '&:hover': {
            backgroundColor: 'transparent',
            '& rect': {
                stroke: theme.palette.primary.main
            },
        },
    },
    checked: {
        '& span': {
            color: theme.palette.primary.main
        },
    },
    disabled: {
        '& span': {
            color: theme.palette.action.disabled,
            '&.MuiIconButton-label': {
                pointerEvents: 'initial',
                cursor: 'not-allowed',
                '&:hover': {
                    '& rect': {
                        stroke: theme.palette.action.disabled,
                    },
                },
            }
        },
    }
}))(MuiCheckbox)

const CMCheckbox = (props) => {
    const { option, index } = props
    if (!option) {
        return null
    }
    return (
        <StyledCheckbox
            className={props.className}
            icon={<CheckboxIcon style={{ margin: 3 }} />}
            disableRipple
            onChange={props.onChange}
            onClick={e => {
                e.stopPropagation()
            }}
            checked={!!(option.checked)}
            disabled={option.disabled}
            name={option.name}
            id={index.toString()}
        />
    )
}

const Checkbox = (props) => (
    <ThemeProvider theme={props.theme || CMTheme}>
        <CMCheckbox {...props} />
    </ThemeProvider>
)

Checkbox.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Callback function when select box. */
    onChange: PropTypes.func,
    option: PropTypes.shape({
        disabled: PropTypes.bool,
        checked: PropTypes.bool,
        name: PropTypes.any,
    }),
    index: PropTypes.number
}

Checkbox.defaultProps = {
    className: '',
    onChange: null,
    option: null,
    index: 0
}

export default Checkbox