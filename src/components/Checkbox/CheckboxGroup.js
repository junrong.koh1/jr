import React, { useCallback } from 'react';
import PropTypes from 'prop-types'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from './index'
import CMTheme from '../../theme'

const CheckboxGroup = (props) => {
    const { options, horizontal } = props
    const theme = props.theme || CMTheme

    const handleChange = (event) => {
        const { id } = event.target
        const newOptions = [...options]
        newOptions[id] = {
            ...newOptions[id],
            checked: !(newOptions[id].checked)
        }

        if (props.onChange) {
            props.onChange([...newOptions])
        }
    }

    const renderOptions = useCallback(() => (
        options.map((option, index) => (
            <FormControlLabel
                key={index}
                control={<Checkbox
                    theme={theme}
                    option={option}
                    index={index}
                    onChange={handleChange}
                />}
                label={option.label}
            />
        ))
    ), [options, theme])

    return (
        <FormControl component="fieldset" >
            <FormGroup>
                {
                    horizontal
                        ? <div>
                            {renderOptions()}
                        </div>
                        : renderOptions()
                }

            </FormGroup>
        </FormControl>
    )
}

CheckboxGroup.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Callback function when select box. */
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(PropTypes.shape({
        checked: PropTypes.bool,
        label: PropTypes.string
    })),
    horizontal: PropTypes.bool
}
CheckboxGroup.defaultProps = {
    className: '',
    onChange: null,
    options: [],
    horizontal: false
}

export default CheckboxGroup