import React from 'react'
import ButtonBase from './Button'
import { withStyles } from '@material-ui/styles'
import PropTypes from 'prop-types'

const Button = withStyles({
    root: {
        minWidth: 36,
        height: 36,
        padding: 0
    },
})(ButtonBase)

const IconButton = (props) => {
    return (
        <Button
            {...props}
        >
            {props.children}
        </Button>
    )
}

IconButton.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Button container style. */
    variant: PropTypes.oneOf(['text', 'outlined', 'contained']),
    /** Label on button. */
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    /** Show loading icon after label. */
    loading: PropTypes.bool,
    /** Disable button. */
    disabled: PropTypes.bool,
    /** Callback function when click this button. */
    onClick: PropTypes.func,
}

IconButton.defaultProps = {
    className: '',
    variant: 'contained',
    label: '',
    loading: false,
    disabled: false,
    onClick: null,
}

export default IconButton
