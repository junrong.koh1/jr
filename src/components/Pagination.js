import Paginate from 'react-paginate'
import React from 'react'
import { ThemeProvider, makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { ReactComponent as AngleLeftIcon } from '../static/images/angle_left.svg'
import { ReactComponent as AngleRightIcon } from '../static/images/angle_right.svg'
import DropdownMenu from './DropdownMenu'
import CMTheme from '../theme'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        listStyle: 'none',
        borderRadius: 5,
        // marginBottom: 0,
        paddingLeft: 0
    },
    page: {
        width: 36,
        height: 36,
        fontSize: '0.9375rem',
        border: 'solid 1px',
        borderColor: theme.palette.action.active,
        marginLeft: -1,
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        outline: 'none',
        cursor: 'pointer',
        '&:hover': {
            border: '1px solid',
            borderColor: theme.palette.primary.main
        },
    },
    pageLink: {
        outline: 'none',
        color: theme.palette.text.primary,
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        textDecoration: 'none',
        '&:hover': {
            color: theme.palette.primary.main
        },
    },
    activePage: {
        backgroundColor: theme.palette.primary.main,
        '& $pageLink': {
            color: theme.palette.background.paper
        },
    },
    previous: {
        width: 36,
        height: 36,
        boxSizing: 'border-box',
        outline: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        border: 'solid 1px',
        borderColor: theme.palette.action.active,
        '& svg path': {
            fill: theme.palette.action.active,
        },
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        '&:hover': {
            borderColor: theme.palette.primary.main,
            '& svg path': {
                fill: theme.palette.primary.main
            }
        },
    },
    previousLink: {
        '&:focus': {
            outline: 'none',
        },
    },
    next: {
        width: 36,
        height: 36,
        boxSizing: 'border-box',
        outline: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        border: 'solid 1px',
        borderColor: theme.palette.action.active,
        '& svg path': {
            fill: theme.palette.action.active,
        },
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        '&:hover': {
            borderColor: theme.palette.primary.main,
            '& svg path': {
                fill: theme.palette.primary.main,
            }
        },
    },
    nextLink: {
        '&:focus': {
            outline: 'none',
        },
    },
    disabled: {
        pointerEvents: 'none',
        '& svg path': {
            fill: theme.palette.action.disabled
        }
    }
}))

const CMPageing = (props) => {
    const { forcePage, pageCount, onPageChange, rowsPerPage, onRowsPerPageChange, previousLabel, nextLabel, elementClasses } = props
    const classes = useStyles()

    const rowPerPageOptions =
        rowsPerPage
            ? rowsPerPage.map(rows => ({
                label: `${rows} Rows/Page`,
                value: rows
            }))
            : null

    return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
            {
                rowPerPageOptions
                    ? <div style={{ marginRight: 10, minWidth: 180 }}>
                        <DropdownMenu
                            theme={props.theme || CMTheme}
                            menuItems={rowPerPageOptions}
                            defaultValue={rowPerPageOptions[0]}
                            onClickItem={(item => {
                                onRowsPerPageChange(item.value)
                            })}
                        />
                    </div>
                    : null
            }
            <Paginate
                containerClassName={classes.container}
                pageClassName={clsx(classes.page, {[elementClasses?.page]: Boolean(elementClasses?.page)})}
                pageLinkClassName={clsx(classes.pageLink, {[elementClasses?.pageLink]: Boolean(elementClasses?.pageLink)})}
                activeClassName={clsx(classes.activePage, {[elementClasses?.activePage]: Boolean(elementClasses?.activePage)})}
                previousClassName={clsx(classes.previous, {[elementClasses?.previous]: Boolean(elementClasses?.previous)})}
                previousLinkClassName={classes.previousLink}
                nextClassName={clsx(classes.next, {[elementClasses?.next]: Boolean(elementClasses?.next)})}
                nextLinkClassName={classes.nextLink}
                disabledClassName={clsx(classes.disabled, {[elementClasses?.disabled]: Boolean(elementClasses?.disabled)})}
                previousLabel={previousLabel || <AngleLeftIcon />}
                nextLabel={nextLabel || <AngleRightIcon />}
                pageCount={pageCount}
                onPageChange={onPageChange}
                forcePage={forcePage}
            />
        </div>
    )
}

const Pageing = (props) => (
    <ThemeProvider theme={props.theme || CMTheme}>
        <CMPageing {...props} />
    </ThemeProvider>
)

Pageing.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** The total number of pages. */
    pageCount: PropTypes.number.isRequired,
    /** Set current page forcely */
    forcePage: PropTypes.number,
    /** Select for rows per page. */
    rowsPerPage: PropTypes.arrayOf(PropTypes.number),
    /** Callback function when a page is clicked. */
    onPageChange: PropTypes.func,
    /** Callback function when a rows per page is changed. */
    onRowsPerPageChange: PropTypes.func,
    /** Icon used as previous button label */
    previousLabel: PropTypes.element,
     /** Icon used as next button label */
    nextLabel: PropTypes.element,
    /** Override or extend the styles applied to the component. */
    elementClasses: PropTypes.shape({
        page: PropTypes.string,
        pageLink: PropTypes.string,
        activePage: PropTypes.string,
        previous: PropTypes.string,
        next: PropTypes.string,
        disabled: PropTypes.string
    })
}

export default Pageing
