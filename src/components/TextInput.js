import React from 'react'
import { ThemeProvider, makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import CMTheme from '../theme'

const useStyles = makeStyles((theme) => ({
    inputLabel: {
        transition: 'none',
        color: theme.palette.text.primary,
        fontSize: 18,
    },
    inputRoot: {
        fontSize: 15,
        border: '1px solid',
        borderColor: theme.palette.action.active,
        '&:hover': {
            border: '1.4px solid',
            borderColor: theme.palette.primary.main
        },
        '&:focus': {
            border: '1.4px solid',
            borderColor: theme.palette.primary.main
        },
        '&.Mui-error': {
            border: '1.4px solid',
            borderColor: theme.palette.error.main,
            '& input': {

            },
            '& textarea': {

            }
        },
        '&.Mui-disabled': {
            backgroundColor: theme.palette.background.paper,
            cursor: 'not-allowed',
            '&:hover': {
                border: '1px solid',
                border: theme.palette.action.disabled
            },
        },
        '&.MuiInputBase-multiline': {
            padding: 0,
        },
    },
    inputRootWithLabel: {
        marginTop: '18px !important'
    },
    input: {
        padding: '8px 10px',
        backgroundColor: theme.palette.background.paper,
        '&::placeholder': { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: theme.palette.text.placeholder,
            opacity: 1 /* Firefox */
        },
        '&:-ms-input-placeholder': { /* Internet Explorer 10-11 */
            color: theme.palette.text.placeholder,
        },
        '&::-ms-input-placeholder': { /* Microsoft Edge */
            color: theme.palette.text.placeholder,
        },
    },
}))

const CMTextInput = (props) => {
    const classes = useStyles()
    const { children, InputLabelProps, InputProps, ...others } = props

    const customInputLabelProps = {
        shrink: true,
        classes: {
            shrink: classes.inputLabel
        },
        ...InputLabelProps
    }

    const customInputProps = {
        disableUnderline: true,
        classes: {
            root: clsx(classes.inputRoot, { [classes.inputRootWithLabel]: !!props.label }),
            input: classes.input,
        },
        ...props.InputProps
    }

    // const customSelectProps = {
    //     classes: {
    //         select: classes.select,
    //         icon: classes.selectIcon,
    //         selectMenu: classes.selectMenu
    //     }
    // }

    return (
        <TextField
            variant="standard"
            InputLabelProps={customInputLabelProps}
            InputProps={customInputProps}
            // SelectProps={customSelectProps}
            {...others}
        >
            {children}
        </TextField>
    )
}

const TextInput = (props) => (
    <ThemeProvider theme={props.theme || CMTheme}>
        <CMTextInput {...props} />
    </ThemeProvider>
)

TextInput.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Props for input element */
    inputProps: PropTypes.any,
    /** Label text. */
    label: PropTypes.string,
    /** Field value. */
    value: PropTypes.any,
    /** Placeholder text. */
    placeholder: PropTypes.string,
    /** Helper text of input. */
    helperText: PropTypes.string,
    /** Disabled. */
    disabled: PropTypes.bool,
    /** Text area. */
    multiline: PropTypes.bool,
    /** Input with error. */
    error: PropTypes.bool,
}

export default TextInput