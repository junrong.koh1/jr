import React from 'react';
import PropTypes from 'prop-types'
import { ThemeProvider, withStyles } from '@material-ui/core/styles'
import MuiRadio from '@material-ui/core/Radio'
import CMTheme from '../../theme'

const StyledRadio = withStyles(theme => ({
    root: {
        '&:hover': {
            backgroundColor: 'transparent',
            '& span': {
                color: theme.palette.primary.main
            },
        },
    },
    checked: {
        '& span': {
            color: theme.palette.primary.main
        },
    },
    disabled: {
        '& span': {
            color: theme.palette.action.disabled,
        },
    }
}))(MuiRadio)

const CMRadio = (props) => {
    return (
        <ThemeProvider theme={props.theme || CMTheme}>
            <StyledRadio
                disableRipple
                {...props}
            />
        </ThemeProvider>
    )
}

CMRadio.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Callback function when select box. */
    onChange: PropTypes.func,
    option: PropTypes.shape({
        disabled: PropTypes.bool,
        checked: PropTypes.bool,
        name: PropTypes.any,
    }),
    index: PropTypes.number
}

CMRadio.defaultProps = {
    className: '',
    onChange: null,
    option: null,
    index: 0
}

export default CMRadio