import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types'
import CMRadio from './index';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CMTheme from '../../theme'

const CMRadioGroup = (props) => {
    const { options, horizontal, defaultValue, disabled } = props
    const [value, setValue] = useState(defaultValue)
    const theme = props.theme || CMTheme

    const handleChange = (event) => {
        const result = event?.target?.value
        setValue(result)

        if (props.onChange) {
            props.onChange(result)
        }
    }

    const renderOptions = useCallback(() => (
        options.map((option, index) => (
            <FormControlLabel
                key={index}
                value={option.value}
                label={option.label}
                disabled={disabled}
                control={<CMRadio theme={theme} />}
            />
        ))
    ), [options, disabled, theme])

    return (
        <FormControl component="fieldset">
            <RadioGroup value={value} onChange={handleChange}>
                {
                    horizontal
                        ? <div>
                            {renderOptions()}
                        </div>
                        : renderOptions()
                }
            </RadioGroup>
        </FormControl>
    )
}

CMRadioGroup.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** ClassName can be used for @material-ui/styles to customise style. */
    className: PropTypes.string,
    /** Callback function when select box. */
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.any,
    })),
    /** Disabled this group. */
    disabled: PropTypes.bool,
    horizontal: PropTypes.bool,
    /** Default checked option. */
    defaultValue: PropTypes.any,
}
CMRadioGroup.defaultProps = {
    className: '',
    onChange: null,
    options: [],
    horizontal: false,
    defaultValue: null
}

export default CMRadioGroup