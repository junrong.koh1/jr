import PropTypes from 'prop-types'
import React, { useState, useMemo } from 'react'
import CreatableSelect from 'react-select/creatable'
import { makeStyles } from '@material-ui/core/styles'
import CMTheme from '../theme'
import InputLabel from '@material-ui/core/InputLabel';

const components = {
    DropdownIndicator: null
}

const useStyles = makeStyles(theme => ({
    inputLabel: {
        transition: 'none',
        color: theme.palette.text.primary,
        fontSize: 18,
    },
    filter: {
        width: 165,
        marginLeft: theme.spacing(3)
    }
}))

const makeInlineStyle = (theme, invalid) => ({
    container: (base, state) => ({
        ...base,
        pointerEvents: 'auto',
        cursor: (state.isDisabled && 'not-allowed') || 'auto',
    }),
    control: (base, state) => ({
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 2,
        outline: 0,
        border: ((state.isFocused || invalid) && '1.4px solid')
            || '1px solid'
        ,
        borderColor: (state.isFocused && theme.palette.primary.main)
            || ((state.isDisabled && theme.palette.action.disabled))
            || (invalid && theme.palette.error.main)
            || theme.palette.action.active
        ,
        position: 'relative',
        // boxSizing: 'border-box',
        '&:hover': {
            border: '1.4px solid',
            borderColor: theme.palette.primary.main,
        }
    }),
    valueContainer: (base, state) => ({
        ...base,
        padding: '2px 10px',
    }),
    indicatorSeparator: () => null,
    indicatorsContainer: (base, state) => ({
        ...base,
        paddingRight: 10,
        maxHeight: 34,
    }),
    placeholder: (base) => ({
        ...base,
        color: theme.palette.text.placeholder,
    }),
    input: (base) => ({
        ...base,
        color: theme.palette.text.primary
    }),
    menu: (base) => null,
    menuList: (base) => null,
    option: (base, state) => null,
    multiValue: (base) => ({
        ...base,
        borderRadius: 5,
        overflow: 'hidden',
        marginLeft: '5px',
        '&:first-of-type': {
            marginLeft: '8px'
        },
    }),
    multiValueLabel: (base) => ({
        ...base,
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        fontSize: 12,
        padding: '1.5px 10px',
        paddingLeft: 10,
        backgroundColor: theme.palette.primary.main
    }),
    multiValueRemove: (base) => ({
        ...base,
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.dark,
        cursor: 'pointer',
        padding: 0,
        '& svg': {
            width: 16,
            height: 16
        },
        ':hover': {
            ...base,
            padding: 0,
            color: theme.palette.error.light
        }
    })
})

const createOption = (label) => ({
    label,
    value: label
})

const TagsInput = (props) => {
    const classes = useStyles()
    const theme = props.theme || CMTheme

    const {
        label,
        placeholder,
        defaultValues,
        invalid,
        disabled,
        onChange,
        className,
        classNamePrefix
    } = props
    const customStyles = useMemo(() => makeInlineStyle(theme, invalid), [theme, invalid])
    const [inputValue, setInputValue] = useState('')
    const [values, setValues] = useState(
        (defaultValues || []).map(v => createOption(v))
    )

    const handleChange = (value, actionMeta) => {
        setValues(value || [])
        onChange(value || [])
    }

    const handleInputChange = (txt) => {
        setInputValue(txt)
    }

    const handleKeyDown = (event) => {
        if (!inputValue) return
        switch (event.key) {
            case 'Enter':
            case 'Tab':
                event.preventDefault()
                setInputValue('')
                if (values.find(v => v.label === inputValue) === undefined) {
                    const result = [...values, createOption(inputValue)]
                    setValues(result)
                    onChange(result)
                }
        }
    }

    return (
        <>
            {
                label ?
                    <InputLabel shrink={true} className={classes.inputLabel} style={{
                        color: (invalid && theme.palette.error.main)
                            || (disabled && theme.palette.text.disabled)
                            || 'inherit'
                    }}>
                        {label}
                    </InputLabel>
                    : null
            }
            <CreatableSelect
                isMulti
                placeholder={placeholder}
                value={values}
                styles={customStyles}
                components={components}
                inputValue={inputValue}
                isDisabled={disabled}
                onChange={handleChange}
                onInputChange={handleInputChange}
                onKeyDown={handleKeyDown}
                menuIsOpen={false}
                className={className}
                classNamePrefix={classNamePrefix}
            />
        </>
    )
}

TagsInput.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Label of input. */
    label: PropTypes.string,
    /** Placeholder of input. */
    placeholder: PropTypes.string,
    /** Input with error. */
    invalid: PropTypes.bool,
    /** Disbaled. */
    disabled: PropTypes.bool,
    /** Default value of input. */
    defaultValues: PropTypes.arrayOf(PropTypes.string),
    /** Callback function when value of input changed. */
    onChange: PropTypes.func,
    //** Sets a className attribute on the outer component */
    className: PropTypes.string,
    //** If provided, all inner components will be given a prefixed className attribute. */
    classNamePrefix: PropTypes.string
}
TagsInput.defaultProps = {
    label: '',
    placeholder: 'Type something and press enter...',
    invalid: false,
    disabled: false,
    defaultValues: [],
    onChange: null,
    className: null,
    classNamePrefix: null
}

export default TagsInput
