import React, { useState, forwardRef } from 'react'
import PropTypes from 'prop-types'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Popper from '@material-ui/core/Popper'
import Fade from '@material-ui/core/Fade'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import CheckboxGroup from '../Checkbox/CheckboxGroup'
import Button from '../Button'

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: 15,
        marginLeft: 40,
    },
    arrowUpContainer: {
        position: 'absolute',
        top: 0,
        left: 90
    },
    arrowUp: {
        width: 0,
        height: 0,
        borderLeft: '10px solid transparent',
        borderRight: '10px solid transparent',
        borderBottom: '17px solid white',
    },
    optionsContainer: {
        padding: 20,
    },
    actionContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: 20,
        borderTop: '0.5px solid',
        borderColor: theme.palette.divider
    },
    button: {
        minWidth: 65,
        '&:first-of-type': {
            marginRight: 5
        },
    }
}))

const FilterPopper = (props) => {
    const classes = useStyles()
    const { anchorEl, filters } = props

    const show = Boolean(anchorEl)
    const [checkOptions, setCheckOptions] = useState(filters)

    const onClose = () => {
        if (show) {
            setCheckOptions(filters)
            props.onClose()
        }
    }

    if (show) {
        return (
            <ClickAwayListener onClickAway={onClose}>
                <Popper open={true} anchorEl={anchorEl} transition>
                    {({ TransitionProps }) => (
                        <Fade {...TransitionProps} timeout={350}>
                            <>
                                <div className={classes.arrowUpContainer}>
                                    <div className={classes.arrowUp} />
                                </div>
                                <Paper className={classes.paper}>
                                    <div className={classes.optionsContainer}>
                                        <CheckboxGroup
                                            options={checkOptions}
                                            onChange={(result) => {
                                                setCheckOptions(result)
                                            }}
                                        />
                                    </div>
                                    <div className={classes.actionContainer}>
                                        <Button
                                            className={classes.button}
                                            variant='text'
                                            label='Cancel'
                                            onClick={onClose}
                                        />
                                        <Button
                                            className={classes.button}
                                            label='Filter'
                                            onClick={() => {
                                                props.onFilter(checkOptions)
                                            }}
                                        />
                                    </div>
                                </Paper>
                            </>
                        </Fade>
                    )}
                </Popper>
            </ClickAwayListener>

        )
    }
    return null
}

FilterPopper.propTypes = {
    anchorEl: PropTypes.object,
    filters: PropTypes.arrayOf(PropTypes.shape({
        selected: PropTypes.bool,
        label: PropTypes.string,
        value: PropTypes.any
    })).isRequired,
    onFilter: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default FilterPopper