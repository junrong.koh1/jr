import React, { useState, forwardRef } from 'react'
import PropTypes from 'prop-types'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Badge from '@material-ui/core/Badge'
import { ReactComponent as TableFilterImage } from '../../static/images/table_filter.svg';
import FilterPopper from './FilterPopper'

const useStyles = makeStyles(theme => ({
    root: {
        cursor: 'pointer',
        '&:hover': {
            '& path': {
                fill: theme.palette.primary.main
            }
        }
    },
    active: {
        cursor: 'pointer',
        '& path': {
            fill: theme.palette.primary.main
        }
    }
}))

const StyledBadge = withStyles(() => ({
    badge: {
        transform: 'scale(0.8)',
        right: -12,
        top: -12,
        padding: '0',
    },
}))(Badge);

const Filter = (props) => {
    const classes = useStyles()
    const { show, options } = props
    const [anchorEl, setAnchorEl] = useState()
    const [filters, setFilters] = useState((options || []).map(o => ({
        checked: !!(o.selected),
        label: o.label,
        value: o.value
    })))

    const handleClick = (e) => {
        setAnchorEl(anchorEl ? null : e.currentTarget)
        e.stopPropagation()
    }

    if (show) {
        const badge = filters.filter(o => !!o.checked).length
        return (
            <>
                <StyledBadge badgeContent={badge} color="error">
                    <TableFilterImage className={badge ? classes.active : classes.root} onClick={handleClick} />
                </StyledBadge>
                <FilterPopper
                    anchorEl={anchorEl}
                    filters={filters}
                    onClose={() => {
                        setAnchorEl(null)
                    }}
                    onFilter={(result) => {
                        setFilters(result)
                        setAnchorEl(null)
                        props.onFilter(result)
                    }} />
            </>
        )
    }
    return null
}

Filter.propTypes = {
    show: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.shape({
        selected: PropTypes.bool,
        label: PropTypes.string,
        value: PropTypes.any
    })).isRequired,
    onFilter: PropTypes.func.isRequired,
}

export default Filter