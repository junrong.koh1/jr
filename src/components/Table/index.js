/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect, useCallback } from 'react'
import { makeStyles, ThemeProvider } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'
import Paginate from '../Pagination'
import CMTableCell from './TableCell'
import CMTableFilter from './Filter'
import CMTableSorting from './Sorting'
import CMCheckbox from '../Checkbox'
import _ from 'lodash'
import { ReactComponent as EmptyTableImage } from '../../static/images/empty_table.svg'
import CMTheme from '../../theme'

const useStyles = makeStyles(theme => ({
    infiniteTableBody: {
        display: 'block',
        overflow: 'auto',
        '& tr': {
            display: 'table',
            width: '100%',
            tableLayout: 'fixed',
        }
    },
    infiniteTableHead: {
        display: 'table',
        width: '100%',
        tableLayout: 'fixed'
    },
    headerRow: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        fontFamily: 'Noto Sans TC',
        fontWeight: 'bold',
        fontSize: '14px',
        lineHeight: '20px',
        color: theme.palette.text.primary,
        '&:first-of-type': {
            paddingLeft: '32px'
        },
        '&:last-of-type': {
            paddingRight: '32px'
        }
    },
    itemRow: {
        backgroundColor: theme.palette.background.default,
        '&:hover': {
            backgroundColor: theme.palette.background.paper,
        },
        '& td': {
            color: theme.palette.text.primary,
            '&:first-of-type': {
                paddingLeft: '32px'
            },
            '&:last-of-type': {
                paddingRight: '32px'
            }
        }
    },
    emptyTable: {
        height: 520,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        '& h6': {
            marginTop: theme.spacing(3)
        }
    },
    loading: {
        margin: 10,
        display: 'flex',
        width: 'inherit',
        justifyContent: 'center',
        '& .MuiCircularProgress-svg': {
            color: theme.palette.action.active
        }
    },
    checkboxCell: {
        width: 10,
        '& span': {
            padding: 0
        }
    }
}))

const CMTableHeader = (props) => {
    const classes = useStyles()
    const { headers, sorters, filters, checkAll } = props

    if (!headers || !headers.length) {
        return null
    }

    const isScrollY = (!!props.infiniteScrollHeight)
    return (
        <TableHead
            className={isScrollY ? classes.infiniteTableHead : ''}
        >
            <TableRow className={classes.headerRow}>
                {
                    props.onCheckAll
                        ? <CMTableCell key='check-box' className={classes.checkboxCell}>
                            <CMCheckbox
                                theme={props.theme}
                                option={{ checked: !!checkAll }} onChange={() => {
                                    props.onCheckAll(!checkAll)
                                }} />
                        </CMTableCell>
                        : null
                }
                {
                    headers.map((header, idx) => {
                        const filter = filters[idx] || null
                        const sorter = sorters[idx] || null
                        if (!header) {
                            return
                        }

                        const { title, component, sticky } = header
                        if (component) {
                            return (
                                <CMTableCell className={classes.headerCell} key={idx} sticky={sticky}>
                                    {component}
                                </CMTableCell>
                            )
                        }
                        return (
                            <CMTableCell className={classes.headerCell} key={idx} style={header.style || null} sticky={sticky}>
                                <div style={{ display: 'flex' }}>
                                    {header.title}
                                    {
                                        sorter
                                            ? <div style={{ marginLeft: 5 }}>
                                                <CMTableSorting order={sorter.order} onSort={(order) => {
                                                    sorter.onSort(order)
                                                }} />
                                            </div>
                                            : null
                                    }
                                    {
                                        filter
                                            ? <span style={{ marginLeft: 5 }}>
                                                <CMTableFilter show={true} options={filter.options} onFilter={filter.onFilter} />
                                            </span>
                                            : null
                                    }
                                </div>
                            </CMTableCell>)
                    })
                }
            </TableRow>
        </TableHead>
    )
}

const CMTable = (props) => {
    const classes = useStyles()
    const { headers, datalist, pageLength, totalDataCount } = props

    const [pageIndex, setPageIndex] = useState()
    const [selected, setSelected] = useState({})
    const [loading, setLoading] = useState(false)
    const [checkAll, setCheckAll] = useState(
        props.onCheckItem ? false : undefined
    )
    const [checkList, setCheckList] = useState(
        props.onCheckItem ? [] : undefined
    )

    const totalPage = Math.ceil((totalDataCount || datalist.length) / pageLength)
    const startIndex = pageLength * (pageIndex - 1)
    let endIndex = startIndex + pageLength - 1
    if (endIndex >= datalist.length) {
        endIndex = datalist.length - 1
    }
    const showList = (datalist.length)
        ? datalist.slice(startIndex, endIndex + 1)
        : null

    useEffect(() => {
        if (loading) {
            setLoading(false)
        }

        if (showList) {
            if (!showList[0]) {
                setLoading(true)
            }

            if (props.readMoreData) {
                let limit, offset
                if (showList.length === 0) {
                    offset = startIndex
                    limit = pageLength
                } else if (showList.length < pageLength) {
                    offset = startIndex + showList.length
                    limit = (pageLength - showList.length) + pageLength
                }

                if (offset && limit) {
                    setLoading(true)
                    props.readMoreData(limit, offset)
                }
            }
        }
    }, [showList])

    useEffect(() => {
        if (props.currentPage) {
            setPageIndex(props.currentPage)
        }
    }, [props.currentPage])

    useEffect(() => {
        if (props.checkItems) {
            setCheckList(props.checkItems)
        }
    }, [props.checkItems])

    useEffect(() => {
        if (checkList?.length) {
            let check = true
            for (let i = startIndex; i <= endIndex; i++) {
                check = check && !!(checkList[i])
            }
            setCheckAll(check)
        } else {
            setCheckAll(false)
        }
        if (!props.checkItems && props.onCheckItem) {
            props.onCheckItem(checkList)
        }
    }, [checkList])

    useEffect(() => {
        if (checkList?.length) {
            let check = true
            for (let i = startIndex; i <= endIndex; i++) {
                check = check && !!(checkList[i])
            }
            setCheckAll(check)
        } else {
            setCheckAll(false)
        }
        if (!props.currentPage && props.onChangePage) {
            props.onChangePage(pageIndex)
        }
    }, [pageIndex])

    useEffect(() => {
        if (pageIndex > totalPage) {
            if (props.currentPage && props.onChangePage) {
                props.onChangePage(1)
            } else {
                setPageIndex(1)
            }
        }
    }, [pageIndex, totalPage])

    const handlePageClick = (data) => {
        const page = data.selected + 1
        if (props.currentPage && props.onChangePage) {
            props.onChangePage(page)
        } else {
            setPageIndex(page)
        }
    }

    const onClickRow = (item) => {
        if (props.onClickItem) {
            props.onClickItem(item)
        }
        if (selected.id === item.id) {
            setSelected({})
        } else {
            setSelected(item)
        }
    }

    const onCheckAll = (checkAll) => {
        if (showList) {
            for (let i = startIndex; i <= endIndex; i++) {
                checkList[i] = !!(checkAll)
            }
            if (props.checkItems && props.onCheckItem) {
                props.onCheckItem([...checkList])
            } else {
                setCheckList([...checkList])
            }
        }
    }

    const isScrollY = (!!props.infiniteScrollHeight)
    const theme = props.theme || CMTheme
    return (
        <ThemeProvider theme={theme}>
            <Table className={props.className}>
                <CMTableHeader {...props} onCheckAll={checkList ? onCheckAll : null} checkAll={checkAll} />
                <TableBody
                    className={isScrollY ? classes.infiniteTableBody : ''}
                    style={{ maxHeight: isScrollY ? props.infiniteScrollHeight : 'default' }}
                >
                    {
                        (!showList)
                            ?
                            <TableRow className={classes.itemRow}>
                                <TableCell colSpan={headers ? headers.length : 1}>
                                    <div className={classes.emptyTable}>
                                        {props.emptyImage || <EmptyTableImage />}
                                        <Typography variant="h6">
                                            {props.emptyMessage || 'Oops! Something wrong happen.'}
                                        </Typography>
                                    </div>
                                </TableCell>
                            </TableRow>
                            : showList.map((item, index) => {
                                if (!item) {
                                    return null
                                }
                                if (props.renderRow) {
                                    return props.renderRow(item)
                                }
                                if (props.renderCells) {
                                    return (
                                        <TableRow
                                            className={classes.itemRow}
                                            key={item.id || index}
                                            onClick={() => { onClickRow(item) }}
                                            style={{
                                                cursor: (props.onClickItem) ? 'pointer' : 'default',
                                                backgroundColor: (checkList && checkList[startIndex + index])
                                                    ? theme.palette.action.selected
                                                    : theme.palette.background.paper
                                            }}
                                        >
                                            {
                                                checkList
                                                    ? <TableCell key='check-box' className={classes.checkboxCell}>
                                                        <CMCheckbox
                                                            theme={props.theme}
                                                            option={{ checked: checkList[startIndex + index] }} onChange={(e) => {
                                                                e.stopPropagation()
                                                                checkList[startIndex + index] = !(checkList[startIndex + index])
                                                                if (props.checkItems && props.onCheckItem) {
                                                                    props.onCheckItem([...checkList])
                                                                } else {
                                                                    setCheckList([...checkList])
                                                                }
                                                            }} />
                                                    </TableCell>
                                                    : null
                                            }
                                            {props.renderCells(item)}
                                        </TableRow>
                                    )
                                }
                            })
                    }
                </TableBody>
            </Table>
            {
                loading
                    ? <div className={classes.loading}>
                        <CircularProgress color='inherit' />
                    </div>
                    : null
            }

            <Grid container justify="flex-end">
                <Grid item sm={12} md={7}>
                    <Grid container justify="flex-end">
                        {
                            props.pagination || (
                                (totalPage > 1)
                                    ? <Paginate
                                        theme={props.theme || CMTheme}
                                        pageCount={totalPage}
                                        forcePage={pageIndex - 1}
                                        onPageChange={handlePageClick}
                                        {...props.paginationProps}
                                    />
                                    : null
                            )
                        }
                    </Grid>
                </Grid>
            </Grid>
        </ThemeProvider>
    )
}

CMTable.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Table headers */
    headers: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        style: PropTypes.object,
        component: PropTypes.node,
    })).isRequired,
    /** Source data array. */
    datalist: PropTypes.arrayOf(PropTypes.object).isRequired,
    /** Render function of customized row */
    renderRow: PropTypes.func,
    /** Render function of cells in row */
    renderCells: PropTypes.func,
    /** Fetch function to read more data. */
    readMoreData: PropTypes.func,
    /** How many rows to show in one page. */
    pageLength: PropTypes.number,
    /** The amount of ALL data for rendering pagination and show total page. */
    totalDataCount: PropTypes.number,
    /** Force table to show specified page. */
    currentPage: PropTypes.number,
    /** Force check box of row. */
    checkItems: PropTypes.arrayOf(PropTypes.bool),
    /** Callback function when page changed. */
    onChangePage: PropTypes.func,
    /** Callback function when click row. */
    onClickItem: PropTypes.func,
    /** Callback function when check row. */
    onCheckItem: PropTypes.func,
    /** Sorter for columns */
    sorters: PropTypes.arrayOf(PropTypes.shape({
        onSort: PropTypes.func,
        order: PropTypes.oneOf(['asc', 'desc', '']),
    })),
    /** Filters for columns */
    filters: PropTypes.arrayOf(PropTypes.shape({
        onFilter: PropTypes.func,
        options: PropTypes.arrayOf(PropTypes.shape({
            selected: PropTypes.bool,
            label: PropTypes.string,
            value: PropTypes.any
        }))
    })),
    /** Message when table is empty */
    emptyMessage: PropTypes.string,
    /** Image of empty table */
    emptyImage: PropTypes.node,
    /** Materail UI class from makeStyles for customized style */
    className: PropTypes.string,
    /** Customized pagination*/
    pagination: PropTypes.node,
    /** Props applied to the pagination component */
    paginationProps: PropTypes.object,
    /** Max height of infinite scroll table body */
    infiniteScrollHeight: PropTypes.number
}

CMTable.defaultProps = {
    headers: [],
    datalist: [],
    pageLength: 10,
    currentPage: 1,
    sorters: [],
    filters: []
}

export default CMTable
