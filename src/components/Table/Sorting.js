import React, { useEffect, useCallback, useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { ReactComponent as OrderUpIcon } from '../../static/images/order_up.svg';
import { ReactComponent as OrderDownIcon } from '../../static/images/order_down.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        minHeight: 20,
    },
    icon: {
        cursor: 'pointer',
        '&:first-of-type': {
            marginBottom: 3
        },
    },
    activeIcon: {
        cursor: 'defualt',
        pointerEvents: 'none',
        '&:first-of-type': {
            marginBottom: 3
        },
        color: theme.palette.primary.main,
        '& path': {
            fill: theme.palette.primary.main,
        }
    }
}))

const Sorting = (props) => {
    const classes = useStyles()
    const { order, onSort } = props

    const [currentOrder, setCurrentOrder] = useState(order)

    const handleAsc = useCallback((e) => {
        e.stopPropagation()
        setCurrentOrder('asc')
        onSort && onSort('asc')
    }, [onSort])

    const handleDesc = useCallback((e) => {
        e.stopPropagation()
        setCurrentOrder('desc')
        onSort && onSort('desc')
    }, [onSort])

    useEffect(() => {
        if (currentOrder != order) {
            setCurrentOrder(order)
        }
    }, [order])

    return (
        <div className={classes.root}>
            <OrderUpIcon className={currentOrder === 'asc' ? classes.activeIcon : classes.icon} onClick={handleAsc} />
            <OrderDownIcon className={currentOrder === 'desc' ? classes.activeIcon : classes.icon} onClick={handleDesc} />
        </div>
    )

}

Sorting.propTypes = {
    order: PropTypes.oneOf([null, undefined, '', 'asc', 'desc']),
    onSort: PropTypes.func.isRequired
}

Sorting.defaultProps = {
    order: null,
    onSort: null,
}

export default Sorting