import ReactDatePicker, { registerLocale } from 'react-datepicker'
import PropTypes from 'prop-types'
import React, { forwardRef, createRef } from 'react'
import { makeStyles, ThemeProvider } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import Typography from '@material-ui/core/Typography'
import { ReactComponent as AngleLeftIcon } from '../static/images/angle_left.svg';
import { ReactComponent as DoubleAngleLeftIcon } from '../static/images/double_angle_left.svg';
import { ReactComponent as AngleRightIcon } from '../static/images/angle_right.svg';
import { ReactComponent as DoubleAngleRightIcon } from '../static/images/double_angle_right.svg';
import clsx from 'clsx'
import CMTheme from '../theme'
import TextInput from './TextInput'
import IconButton from './IconButton'
import moment from 'moment'
import zhTW from 'date-fns/locale/zh-TW'
registerLocale('zh-TW', zhTW)

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 180,
        display: 'inline-block',
        '& .react-datepicker-popper': {
            zIndex: 10
        },
        '& .react-datepicker__input-container': {
            '& .react-datepicker__close-icon::after': {
                color: theme.palette.primary.main,
                backgroundColor: theme.palette.background.default,
                borderRadius: 5
            }
        },
        '& .react-datepicker': {
            border: 'none',
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.background.default,
            fontSize: '0.8125rem',
            fontFamily: '"Noto Sans TC", sans-serif',
            boxShadow: '0 0 20px 0 rgba(0, 0, 0, 0.1)',
            '& .react-datepicker__header': {
                backgroundColor: theme.palette.background.default,
                borderBottom: 'none',
                padding: 0,
                '& .react-datepicker__day-name': {
                    width: '2rem',
                    lineHeight: '2rem',
                    margin: '0.15625rem',
                    color: theme.palette.text.secondary
                }
            },
            '& .react-datepicker__year': {
                margin: '0 0.84375rem 0.84375rem 0.84375rem',
                '& .react-datepicker__year-wrapper': {
                    maxWidth: '15rem',
                    textAlign: 'center',
                    display: 'flex',
                    flexWrap: 'wrap',
                    '& .react-datepicker__year-text': {
                        cursor: 'pointer',
                        borderRadius: '3.4375rem',
                        width: '3.4375rem',
                        boxSizing: 'content-box',
                        margin: '0.15625rem',
                        padding: '0.4375rem 0.625rem',
                        '&:hover': {
                            backgroundColor: theme.palette.action.hover
                        },
                        '&.react-datepicker__year-text--today': {
                            color: theme.palette.primary.main
                        },
                        '&.react-datepicker__year-text--selected': {
                            backgroundColor: theme.palette.primary.main,
                            color: theme.palette.primary.contrastText,
                            '&.react-datepicker__year-text--keyboard-selected': {
                                backgroundColor: theme.palette.primary.main
                            }
                        },
                        '&.react-datepicker__year-text--disabled': {
                            color: '#CBCCD0',
                            pointerEvents: 'none'
                        },
                        '&.react-datepicker__year-text--keyboard-selected': {
                            color: theme.palette.primary.contrastText,
                            backgroundColor: theme.palette.primary.light,
                            '&:hover': {
                                backgroundColor: theme.palette.primary.dark
                            }
                        }
                    }
                }
            },
            '& .react-datepicker__month': {
                margin: '0 0.84375rem 0.84375rem 0.84375rem',
                '& .react-datepicker__month-wrapper': {
                    marginBottom: theme.spacing(1),
                    '&:last-of-type': {
                        marginBottom: 0
                    },
                    '& .react-datepicker__month-text': {
                        borderRadius: '3.4375rem',
                        width: '3.4375rem',
                        boxSizing: 'content-box',
                        margin: '0.15625rem',
                        padding: '0.4375rem 0.625rem',
                        '&:hover': {
                            backgroundColor: theme.palette.action.hover
                        },
                        '&.react-datepicker__month--selected': {
                            backgroundColor: theme.palette.primary.main,
                            color: theme.palette.primary.contrastText,
                            '&.react-datepicker__month-text--keyboard-selected': {
                                backgroundColor: theme.palette.primary.main
                            }
                        },
                        '&.react-datepicker__month--disabled': {
                            color: theme.palette.text.disabled,
                            pointerEvents: 'none'
                        },
                        '&.react-datepicker__month-text--keyboard-selected': {
                            color: theme.palette.primary.contrastText,
                            backgroundColor: theme.palette.primary.light,
                            '&:hover': {
                                backgroundColor: theme.palette.primary.dark
                            }
                        }
                    }
                },
                '& .react-datepicker__quarter-wrapper': {
                    '& .react-datepicker__quarter-text': {
                        borderRadius: '2.25rem',
                        width: '2.25rem',
                        boxSizing: 'content-box',
                        margin: '0.15625rem',
                        padding: '0.4375rem 0.625rem',
                        '&:hover': {
                            backgroundColor: theme.palette.action.hover
                        },
                        '&.react-datepicker__quarter--selected': {
                            backgroundColor: theme.palette.primary.main,
                            color: theme.palette.primary.contrastText,
                            '&.react-datepicker__quarter-text--keyboard-selected': {
                                backgroundColor: theme.palette.primary.main
                            }
                        },
                        '&.react-datepicker__quarter--disabled': {
                            color: theme.palette.text.disabled,
                            pointerEvents: 'none'
                        },
                        '&.react-datepicker__quarter-text--keyboard-selected': {
                            color: theme.palette.primary.contrastText,
                            backgroundColor: theme.palette.primary.light,
                            '&:hover': {
                                backgroundColor: theme.palette.primary.dark
                            }
                        }
                    }
                },
                '& .react-datepicker__week': {
                    '& .react-datepicker__day': {
                        width: '2rem',
                        lineHeight: '2rem',
                        margin: '0.15625rem',
                        borderRadius: '50%',
                        color: theme.palette.text.primary,
                        outline: 'none',
                        '&:hover': {
                            backgroundColor: theme.palette.action.hover
                        },
                        '&.react-datepicker__day--keyboard-selected': {
                            backgroundColor: theme.palette.primary.light,
                            '&:hover': {
                                color: theme.palette.primary.contrastText,
                                backgroundColor: theme.palette.primary.dark
                            }
                        },
                        '&.react-datepicker__day--today': {
                            color: theme.palette.primary.main,
                            '&.react-datepicker__day--in-selecting-range': {
                                color: theme.palette.primary.contrastText
                            },
                            '&.react-datepicker__day--keyboard-selected': {
                                backgroundColor: 'transparent'
                            }
                        },
                        '&.react-datepicker__day--selected, &.react-datepicker__day--in-range': {
                            color: theme.palette.primary.contrastText,
                            backgroundColor: theme.palette.primary.main
                        },
                        '&.react-datepicker__day--in-selecting-range': {
                            '&.react-datepicker__day--selecting-range-start, &.react-datepicker__day--selecting-range-end': {
                                backgroundColor: theme.palette.primary.main
                            },
                            backgroundColor: theme.palette.primary.light
                        },
                        '&.react-datepicker__day--outside-month': {
                            color: '#B3B3B3',
                            '&.react-datepicker__day--selected': {
                                color: theme.palette.primary.contrastText,
                                backgroundColor: theme.palette.primary.light
                            }
                        },
                        '&.react-datepicker__day--disabled': {
                            color: theme.palette.text.disabled,
                            pointerEvents: 'none'
                        }
                    }
                },
                '&.react-datepicker__month--selecting-range': {
                    '& .react-datepicker__week': {
                        '& .react-datepicker__day': {
                            '&.react-datepicker__day--in-range': {
                                backgroundColor: theme.palette.primary.light
                            },
                            '&.react-datepicker__day--in-range:not(.react-datepicker__day--in-selecting-range)': {
                                color: theme.palette.common.black,
                                backgroundColor: theme.palette.primary.light
                            }
                        }
                    }
                }
            },
            '& .react-datepicker__time-container': {
                width: 90,
                '& .react-datepicker__header--time': {
                    paddingTop: 6,
                    paddingBottom: 6,
                    '& .react-datepicker-time__header': {
                        fontSize: '0.9375rem'
                    }
                },
                '& .react-datepicker__time': {
                    '& .react-datepicker__time-box': {
                        width: 90,
                        '& .react-datepicker__time-list': {
                            '&::-webkit-scrollbar': {
                                display: 'none'
                            },
                            '-ms-overflow-style': 'none',
                            '& .react-datepicker__time-list-item': {
                                height: 25,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                '&:hover': {
                                    backgroundColor: theme.palette.primary.light
                                },
                                '&.react-datepicker__time-list-item--selected': {
                                    backgroundColor: theme.palette.primary.main
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    header: {
        margin: '0.625rem 1.285rem 0.25rem 1.285rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& h6': {
            color: theme.palette.text.primary,
            fontSize: '0.875rem',
            fontWeight: 500,
            fontFamily: '"Noto Sans TC", sans-serif',
            padding: '0 30px',
            flexGrow: 1
        },
        '& .icon': {
            height: 10,
            width: 10,
            '& path': {
                fill: theme.palette.primary.main
            }
        }
    },
    arrowButton: {
        minWidth: 20,
        '&:hover': {
            backgroundColor: theme.palette.action.hover
        },
    },
    inputLabel: {
        transition: 'none',
        color: theme.palette.text.primary,
        fontSize: 18,
    },
}))

const CMDatePicker = (props) => {
    const classes = useStyles()
    const theme = props.theme || CMTheme

    const {
        label,
        className,
        textInputProps,
        selected,
        startDate,
        endDate,
        maxDate,
        minDate,
        onChange,
        dateFormat,
        showMonthYearPicker,
        showQuarterYearPicker,
        showYearPicker,
        arrowLabels,
        yearItemNumber = 12,
        ...others
    } = props

    const CustomInput = forwardRef(({ value, onClick }, _ref) => (
        <TextInput
            defaultValue={value}
            theme={theme}
            inputProps={{
                ref: _ref,
                style: { marginTop: 0, textAlign: 'center', cursor: 'pointer' }
            }}
            onFocus={onClick}
            {...textInputProps}
        />
    ))

    const getYearsPeriod = (date, itemNumber) => {
        const currentDate = new Date(date)
        const endYear = Math.ceil(currentDate.getFullYear() / itemNumber) * itemNumber
        const startYear = endYear - (itemNumber - 1);
        return { startYear, endYear }
    }

    const customHeader = (props) => {
        const { date, decreaseMonth, increaseMonth, prevMonthButtonDisabled, nextMonthButtonDisabled } = props
        const { decreaseYear, increaseYear, prevYearButtonDisabled, nextYearButtonDisabled } = props
        const d = moment(date)
        const varianted = (showMonthYearPicker || showQuarterYearPicker || showYearPicker)
        const yearsPeriod = getYearsPeriod(date, yearItemNumber)

        return (
            <div className={classes.header}>
                <IconButton
                    theme={theme}
                    variant='text'
                    onClick={decreaseYear}
                    disabled={prevYearButtonDisabled}
                    className={classes.arrowButton}
                >
                    {prevYearButtonDisabled ? null : varianted
                        ? arrowLabels?.left || <AngleLeftIcon className='icon' />
                        : arrowLabels?.doubleLeft || <DoubleAngleLeftIcon className='icon' />
                    }
                </IconButton>
                {
                    varianted
                        ? <Typography variant='h6'>{showYearPicker ? `${yearsPeriod.startYear} - ${yearsPeriod.endYear}` : d.year()}</Typography>
                        : <>
                            <IconButton
                                theme={theme}
                                variant='text'
                                onClick={decreaseMonth}
                                disabled={prevMonthButtonDisabled}
                                className={classes.arrowButton}
                            >
                                {prevMonthButtonDisabled ? null
                                    : arrowLabels?.left || <AngleLeftIcon className='icon' />}
                            </IconButton>
                            <Typography variant='h6'>{d.format('MMM YYYY')}</Typography>
                            <IconButton
                                theme={theme}
                                variant='text'
                                onClick={increaseMonth}
                                disabled={nextMonthButtonDisabled}
                                className={classes.arrowButton}
                            >
                                {nextMonthButtonDisabled ? null
                                    : arrowLabels?.right || <AngleRightIcon className='icon' />}
                            </IconButton>
                        </>
                }
                <IconButton
                    theme={theme}
                    variant='text'
                    disabled={nextYearButtonDisabled}
                    onClick={increaseYear}
                    className={classes.arrowButton}
                >
                    {nextYearButtonDisabled ? null : varianted
                        ? arrowLabels?.right || <AngleRightIcon className='icon' />
                        : arrowLabels?.doubleRight || <DoubleAngleRightIcon className='icon' />
                    }
                </IconButton>
            </div>
        )
    }

    return (
        <>
            {
                label
                    ? <InputLabel shrink={true} className={classes.inputLabel}>{label}</InputLabel>
                    : null
            }
            <div className={clsx(classes.root, { [className]: !!className })}>
                <ReactDatePicker
                    showPopperArrow={false}
                    customInput={
                        <CustomInput />
                    }
                    renderCustomHeader={customHeader}
                    popperPlacement="bottom-start"
                    popperModifiers={{
                        offset: {
                            enabled: true,
                            offset: '0px, 5px'
                        }
                    }}
                    onChange={date => {
                        if (onChange) {
                            onChange(date)
                        }
                    }}
                    dateFormat={dateFormat || "yyyy/MM/dd"}
                    selected={selected}
                    startDate={startDate}
                    endDate={endDate}
                    maxDate={maxDate}
                    minDate={minDate}
                    showMonthYearPicker={showMonthYearPicker}
                    showQuarterYearPicker={showQuarterYearPicker}
                    showYearPicker={showYearPicker}
                    {...others}
                />
            </div>
        </>
    )
}

const DatePicker = (props) => (
    <ThemeProvider theme={props.theme || CMTheme}>
        <CMDatePicker {...props} />
    </ThemeProvider>
)

DatePicker.propTypes = {
    /** Inject theme object of createMuiTheme. */
    theme: PropTypes.object,
    /** Label text. */
    label: PropTypes.string,
    /** Add style to DatePicker container **/
    className: PropTypes.string,
    /** Add style to DatePicker input **/
    textInputProps: PropTypes.shape({
        /** Input with error. */
        error: PropTypes.bool,
        /** Helper text of input. */
        helperText: PropTypes.string,
    }),
    /** Selected date. */
    selected: PropTypes.instanceOf(Date),
    /** Used for setting start date of selected range. */
    startDate: PropTypes.instanceOf(Date),
    /** Used for setting end date of selected range. */
    endDate: PropTypes.instanceOf(Date),
    /** Used for setting start date of available date range. */
    minDate: PropTypes.instanceOf(Date),
    /** Used for setting end of available date range. */
    maxDate: PropTypes.instanceOf(Date),
    /** Format of selected date on input. */
    dateFormat: PropTypes.string,
    /** Callback function when click date. */
    onChange: PropTypes.func.isRequired,
    /** Used for setting variant of DatePicker **/
    showMonthYearPicker: PropTypes.bool,
    /** Used for setting variant of DatePicker **/
    showQuarterYearPicker: PropTypes.bool,
    /** Used for setting variant of DatePicker **/
    showYearPicker: PropTypes.bool,
    /** Used for setting custom arrow labels */
    arrowLabels: PropTypes.shape({
        left: PropTypes.element,
        doubleLeft: PropTypes.element,
        right: PropTypes.element,
        doubleRight: PropTypes.element
    }),
    /** Used for setting number of year items per page in YearPicker */
    yearItemNumber: PropTypes.number
}
DatePicker.defaultProps = {
    dateFormat: "yyyy/MM/dd",
    yearItemNumber: 12
}

export default DatePicker
