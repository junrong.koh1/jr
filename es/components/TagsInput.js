function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import PropTypes from 'prop-types';
import React, { useState, useMemo } from 'react';
import CreatableSelect from 'react-select/creatable';
import { makeStyles } from '@material-ui/core/styles';
import CMTheme from '../theme';
import InputLabel from '@material-ui/core/InputLabel';
var components = {
  DropdownIndicator: null
};
var useStyles = makeStyles(function (theme) {
  return {
    inputLabel: {
      transition: 'none',
      color: theme.palette.text.primary,
      fontSize: 18
    },
    filter: {
      width: 165,
      marginLeft: theme.spacing(3)
    }
  };
});

var makeInlineStyle = function makeInlineStyle(theme, invalid) {
  return {
    container: function container(base, state) {
      return _extends({}, base, {
        pointerEvents: 'auto',
        cursor: state.isDisabled && 'not-allowed' || 'auto'
      });
    },
    control: function control(base, state) {
      return {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 2,
        outline: 0,
        border: (state.isFocused || invalid) && '1.4px solid' || '1px solid',
        borderColor: state.isFocused && theme.palette.primary.main || state.isDisabled && theme.palette.action.disabled || invalid && theme.palette.error.main || theme.palette.action.active,
        position: 'relative',
        // boxSizing: 'border-box',
        '&:hover': {
          border: '1.4px solid',
          borderColor: theme.palette.primary.main
        }
      };
    },
    valueContainer: function valueContainer(base, state) {
      return _extends({}, base, {
        padding: '2px 10px'
      });
    },
    indicatorSeparator: function indicatorSeparator() {
      return null;
    },
    indicatorsContainer: function indicatorsContainer(base, state) {
      return _extends({}, base, {
        paddingRight: 10,
        maxHeight: 34
      });
    },
    placeholder: function placeholder(base) {
      return _extends({}, base, {
        color: theme.palette.text.placeholder
      });
    },
    input: function input(base) {
      return _extends({}, base, {
        color: theme.palette.text.primary
      });
    },
    menu: function menu(base) {
      return null;
    },
    menuList: function menuList(base) {
      return null;
    },
    option: function option(base, state) {
      return null;
    },
    multiValue: function multiValue(base) {
      return _extends({}, base, {
        borderRadius: 5,
        overflow: 'hidden',
        marginLeft: '5px',
        '&:first-of-type': {
          marginLeft: '8px'
        }
      });
    },
    multiValueLabel: function multiValueLabel(base) {
      return _extends({}, base, {
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        fontSize: 12,
        padding: '1.5px 10px',
        paddingLeft: 10,
        backgroundColor: theme.palette.primary.main
      });
    },
    multiValueRemove: function multiValueRemove(base) {
      return _extends({}, base, {
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.dark,
        cursor: 'pointer',
        padding: 0,
        '& svg': {
          width: 16,
          height: 16
        },
        ':hover': _extends({}, base, {
          padding: 0,
          color: theme.palette.error.light
        })
      });
    }
  };
};

var createOption = function createOption(label) {
  return {
    label: label,
    value: label
  };
};

var TagsInput = function TagsInput(props) {
  var classes = useStyles();
  var theme = props.theme || CMTheme;
  var label = props.label,
      placeholder = props.placeholder,
      defaultValues = props.defaultValues,
      invalid = props.invalid,
      disabled = props.disabled,
      onChange = props.onChange,
      className = props.className,
      classNamePrefix = props.classNamePrefix;
  var customStyles = useMemo(function () {
    return makeInlineStyle(theme, invalid);
  }, [theme, invalid]);

  var _useState = useState(''),
      inputValue = _useState[0],
      setInputValue = _useState[1];

  var _useState2 = useState((defaultValues || []).map(function (v) {
    return createOption(v);
  })),
      values = _useState2[0],
      setValues = _useState2[1];

  var handleChange = function handleChange(value, actionMeta) {
    setValues(value || []);
    onChange(value || []);
  };

  var handleInputChange = function handleInputChange(txt) {
    setInputValue(txt);
  };

  var handleKeyDown = function handleKeyDown(event) {
    if (!inputValue) return;

    switch (event.key) {
      case 'Enter':
      case 'Tab':
        event.preventDefault();
        setInputValue('');

        if (values.find(function (v) {
          return v.label === inputValue;
        }) === undefined) {
          var result = [].concat(values, [createOption(inputValue)]);
          setValues(result);
          onChange(result);
        }

    }
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, label ? /*#__PURE__*/React.createElement(InputLabel, {
    shrink: true,
    className: classes.inputLabel,
    style: {
      color: invalid && theme.palette.error.main || disabled && theme.palette.text.disabled || 'inherit'
    }
  }, label) : null, /*#__PURE__*/React.createElement(CreatableSelect, {
    isMulti: true,
    placeholder: placeholder,
    value: values,
    styles: customStyles,
    components: components,
    inputValue: inputValue,
    isDisabled: disabled,
    onChange: handleChange,
    onInputChange: handleInputChange,
    onKeyDown: handleKeyDown,
    menuIsOpen: false,
    className: className,
    classNamePrefix: classNamePrefix
  }));
};

TagsInput.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** Label of input. */
  label: PropTypes.string,

  /** Placeholder of input. */
  placeholder: PropTypes.string,

  /** Input with error. */
  invalid: PropTypes.bool,

  /** Disbaled. */
  disabled: PropTypes.bool,

  /** Default value of input. */
  defaultValues: PropTypes.arrayOf(PropTypes.string),

  /** Callback function when value of input changed. */
  onChange: PropTypes.func,
  //** Sets a className attribute on the outer component */
  className: PropTypes.string,
  //** If provided, all inner components will be given a prefixed className attribute. */
  classNamePrefix: PropTypes.string
} : {};
TagsInput.defaultProps = {
  label: '',
  placeholder: 'Type something and press enter...',
  invalid: false,
  disabled: false,
  defaultValues: [],
  onChange: null,
  className: null,
  classNamePrefix: null
};
export default TagsInput;