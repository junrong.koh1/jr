function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { useState, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CheckboxGroup from '../Checkbox/CheckboxGroup';
import Button from '../Button';
var useStyles = makeStyles(function (theme) {
  return {
    paper: {
      marginTop: 15,
      marginLeft: 40
    },
    arrowUpContainer: {
      position: 'absolute',
      top: 0,
      left: 90
    },
    arrowUp: {
      width: 0,
      height: 0,
      borderLeft: '10px solid transparent',
      borderRight: '10px solid transparent',
      borderBottom: '17px solid white'
    },
    optionsContainer: {
      padding: 20
    },
    actionContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      padding: 20,
      borderTop: '0.5px solid',
      borderColor: theme.palette.divider
    },
    button: {
      minWidth: 65,
      '&:first-of-type': {
        marginRight: 5
      }
    }
  };
});

var FilterPopper = function FilterPopper(props) {
  var classes = useStyles();
  var anchorEl = props.anchorEl,
      filters = props.filters;
  var show = Boolean(anchorEl);

  var _useState = useState(filters),
      checkOptions = _useState[0],
      setCheckOptions = _useState[1];

  var onClose = function onClose() {
    if (show) {
      setCheckOptions(filters);
      props.onClose();
    }
  };

  if (show) {
    return /*#__PURE__*/React.createElement(ClickAwayListener, {
      onClickAway: onClose
    }, /*#__PURE__*/React.createElement(Popper, {
      open: true,
      anchorEl: anchorEl,
      transition: true
    }, function (_ref) {
      var TransitionProps = _ref.TransitionProps;
      return /*#__PURE__*/React.createElement(Fade, _extends({}, TransitionProps, {
        timeout: 350
      }), /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: classes.arrowUpContainer
      }, /*#__PURE__*/React.createElement("div", {
        className: classes.arrowUp
      })), /*#__PURE__*/React.createElement(Paper, {
        className: classes.paper
      }, /*#__PURE__*/React.createElement("div", {
        className: classes.optionsContainer
      }, /*#__PURE__*/React.createElement(CheckboxGroup, {
        options: checkOptions,
        onChange: function onChange(result) {
          setCheckOptions(result);
        }
      })), /*#__PURE__*/React.createElement("div", {
        className: classes.actionContainer
      }, /*#__PURE__*/React.createElement(Button, {
        className: classes.button,
        variant: "text",
        label: "Cancel",
        onClick: onClose
      }), /*#__PURE__*/React.createElement(Button, {
        className: classes.button,
        label: "Filter",
        onClick: function onClick() {
          props.onFilter(checkOptions);
        }
      })))));
    }));
  }

  return null;
};

FilterPopper.propTypes = process.env.NODE_ENV !== "production" ? {
  anchorEl: PropTypes.object,
  filters: PropTypes.arrayOf(PropTypes.shape({
    selected: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.any
  })).isRequired,
  onFilter: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
} : {};
export default FilterPopper;