import React, { useState, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';

var TableFilterImage = function TableFilterImage(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M0 0h9L6.833 6H2.167L0 0zM2 7h5v3H2z",
    fill: "#A6A6A6"
  }));
};

TableFilterImage.defaultProps = {
  width: "9",
  height: "10",
  viewBox: "0 0 9 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
import FilterPopper from './FilterPopper';
var useStyles = makeStyles(function (theme) {
  return {
    root: {
      cursor: 'pointer',
      '&:hover': {
        '& path': {
          fill: theme.palette.primary.main
        }
      }
    },
    active: {
      cursor: 'pointer',
      '& path': {
        fill: theme.palette.primary.main
      }
    }
  };
});
var StyledBadge = withStyles(function () {
  return {
    badge: {
      transform: 'scale(0.8)',
      right: -12,
      top: -12,
      padding: '0'
    }
  };
})(Badge);

var Filter = function Filter(props) {
  var classes = useStyles();
  var show = props.show,
      options = props.options;

  var _useState = useState(),
      anchorEl = _useState[0],
      setAnchorEl = _useState[1];

  var _useState2 = useState((options || []).map(function (o) {
    return {
      checked: !!o.selected,
      label: o.label,
      value: o.value
    };
  })),
      filters = _useState2[0],
      setFilters = _useState2[1];

  var handleClick = function handleClick(e) {
    setAnchorEl(anchorEl ? null : e.currentTarget);
    e.stopPropagation();
  };

  if (show) {
    var badge = filters.filter(function (o) {
      return !!o.checked;
    }).length;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(StyledBadge, {
      badgeContent: badge,
      color: "error"
    }, /*#__PURE__*/React.createElement(TableFilterImage, {
      className: badge ? classes.active : classes.root,
      onClick: handleClick
    })), /*#__PURE__*/React.createElement(FilterPopper, {
      anchorEl: anchorEl,
      filters: filters,
      onClose: function onClose() {
        setAnchorEl(null);
      },
      onFilter: function onFilter(result) {
        setFilters(result);
        setAnchorEl(null);
        props.onFilter(result);
      }
    }));
  }

  return null;
};

Filter.propTypes = process.env.NODE_ENV !== "production" ? {
  show: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape({
    selected: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.any
  })).isRequired,
  onFilter: PropTypes.func.isRequired
} : {};
export default Filter;