import React, { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

var OrderUpIcon = function OrderUpIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M4.5 0l3.897 5.25H.603L4.5 0z",
    fill: "#A6A6A6"
  }));
};

OrderUpIcon.defaultProps = {
  width: "9",
  height: "6",
  viewBox: "0 0 9 6",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var OrderDownIcon = function OrderDownIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M4.5 6L.603.75h7.794L4.5 6z",
    fill: "#A6A6A6"
  }));
};

OrderDownIcon.defaultProps = {
  width: "9",
  height: "6",
  viewBox: "0 0 9 6",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
var useStyles = makeStyles(function (theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      minHeight: 20
    },
    icon: {
      cursor: 'pointer',
      '&:first-of-type': {
        marginBottom: 3
      }
    },
    activeIcon: {
      cursor: 'defualt',
      pointerEvents: 'none',
      '&:first-of-type': {
        marginBottom: 3
      },
      color: theme.palette.primary.main,
      '& path': {
        fill: theme.palette.primary.main
      }
    }
  };
});

var Sorting = function Sorting(props) {
  var classes = useStyles();
  var order = props.order,
      onSort = props.onSort;

  var _useState = useState(order),
      currentOrder = _useState[0],
      setCurrentOrder = _useState[1];

  var handleAsc = useCallback(function (e) {
    e.stopPropagation();
    setCurrentOrder('asc');
    onSort && onSort('asc');
  }, [onSort]);
  var handleDesc = useCallback(function (e) {
    e.stopPropagation();
    setCurrentOrder('desc');
    onSort && onSort('desc');
  }, [onSort]);
  useEffect(function () {
    if (currentOrder != order) {
      setCurrentOrder(order);
    }
  }, [order]);
  return /*#__PURE__*/React.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/React.createElement(OrderUpIcon, {
    className: currentOrder === 'asc' ? classes.activeIcon : classes.icon,
    onClick: handleAsc
  }), /*#__PURE__*/React.createElement(OrderDownIcon, {
    className: currentOrder === 'desc' ? classes.activeIcon : classes.icon,
    onClick: handleDesc
  }));
};

Sorting.propTypes = process.env.NODE_ENV !== "production" ? {
  order: PropTypes.oneOf([null, undefined, '', 'asc', 'desc']),
  onSort: PropTypes.func.isRequired
} : {};
Sorting.defaultProps = {
  order: null,
  onSort: null
};
export default Sorting;