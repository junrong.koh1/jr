function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paginate from '../Pagination';
import CMTableCell from './TableCell';
import CMTableFilter from './Filter';
import CMTableSorting from './Sorting';
import CMCheckbox from '../Checkbox';
import _ from 'lodash';

var EmptyTableImage = function EmptyTableImage(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("rect", {
    width: "118",
    height: "118",
    rx: "2",
    fill: "#F7F7F7"
  }), /*#__PURE__*/React.createElement("path", {
    d: "M39.78 58.476L20.97 81.883c-1.052 1.31-.12 3.253 1.558 3.253h73.535c1.565 0 2.523-1.717 1.702-3.05l-23.18-37.613a2 2 0 0 0-3.404-.002l-14.23 23.02a2 2 0 0 1-3.008.463l-11.299-9.74a2 2 0 0 0-2.865.262z",
    fill: "#DEDBDD"
  }), /*#__PURE__*/React.createElement("circle", {
    cx: "43.601",
    cy: "40.2",
    r: "8.078",
    fill: "#DEDBDD"
  }));
};

EmptyTableImage.defaultProps = {
  width: "118",
  height: "118",
  viewBox: "0 0 118 118",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
import CMTheme from '../../theme';
var useStyles = makeStyles(function (theme) {
  return {
    infiniteTableBody: {
      display: 'block',
      overflow: 'auto',
      '& tr': {
        display: 'table',
        width: '100%',
        tableLayout: 'fixed'
      }
    },
    infiniteTableHead: {
      display: 'table',
      width: '100%',
      tableLayout: 'fixed'
    },
    headerRow: {
      backgroundColor: theme.palette.primary.light
    },
    headerCell: {
      fontFamily: 'Noto Sans TC',
      fontWeight: 'bold',
      fontSize: '14px',
      lineHeight: '20px',
      color: theme.palette.text.primary,
      '&:first-of-type': {
        paddingLeft: '32px'
      },
      '&:last-of-type': {
        paddingRight: '32px'
      }
    },
    itemRow: {
      backgroundColor: theme.palette.background["default"],
      '&:hover': {
        backgroundColor: theme.palette.background.paper
      },
      '& td': {
        color: theme.palette.text.primary,
        '&:first-of-type': {
          paddingLeft: '32px'
        },
        '&:last-of-type': {
          paddingRight: '32px'
        }
      }
    },
    emptyTable: {
      height: 520,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      '& h6': {
        marginTop: theme.spacing(3)
      }
    },
    loading: {
      margin: 10,
      display: 'flex',
      width: 'inherit',
      justifyContent: 'center',
      '& .MuiCircularProgress-svg': {
        color: theme.palette.action.active
      }
    },
    checkboxCell: {
      width: 10,
      '& span': {
        padding: 0
      }
    }
  };
});

var CMTableHeader = function CMTableHeader(props) {
  var classes = useStyles();
  var headers = props.headers,
      sorters = props.sorters,
      filters = props.filters,
      checkAll = props.checkAll;

  if (!headers || !headers.length) {
    return null;
  }

  var isScrollY = !!props.infiniteScrollHeight;
  return /*#__PURE__*/React.createElement(TableHead, {
    className: isScrollY ? classes.infiniteTableHead : ''
  }, /*#__PURE__*/React.createElement(TableRow, {
    className: classes.headerRow
  }, props.onCheckAll ? /*#__PURE__*/React.createElement(CMTableCell, {
    key: "check-box",
    className: classes.checkboxCell
  }, /*#__PURE__*/React.createElement(CMCheckbox, {
    theme: props.theme,
    option: {
      checked: !!checkAll
    },
    onChange: function onChange() {
      props.onCheckAll(!checkAll);
    }
  })) : null, headers.map(function (header, idx) {
    var filter = filters[idx] || null;
    var sorter = sorters[idx] || null;

    if (!header) {
      return;
    }

    var title = header.title,
        component = header.component,
        sticky = header.sticky;

    if (component) {
      return /*#__PURE__*/React.createElement(CMTableCell, {
        className: classes.headerCell,
        key: idx,
        sticky: sticky
      }, component);
    }

    return /*#__PURE__*/React.createElement(CMTableCell, {
      className: classes.headerCell,
      key: idx,
      style: header.style || null,
      sticky: sticky
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        display: 'flex'
      }
    }, header.title, sorter ? /*#__PURE__*/React.createElement("div", {
      style: {
        marginLeft: 5
      }
    }, /*#__PURE__*/React.createElement(CMTableSorting, {
      order: sorter.order,
      onSort: function onSort(order) {
        sorter.onSort(order);
      }
    })) : null, filter ? /*#__PURE__*/React.createElement("span", {
      style: {
        marginLeft: 5
      }
    }, /*#__PURE__*/React.createElement(CMTableFilter, {
      show: true,
      options: filter.options,
      onFilter: filter.onFilter
    })) : null));
  })));
};

var CMTable = function CMTable(props) {
  var classes = useStyles();
  var headers = props.headers,
      datalist = props.datalist,
      pageLength = props.pageLength,
      totalDataCount = props.totalDataCount;

  var _useState = useState(),
      pageIndex = _useState[0],
      setPageIndex = _useState[1];

  var _useState2 = useState({}),
      selected = _useState2[0],
      setSelected = _useState2[1];

  var _useState3 = useState(false),
      loading = _useState3[0],
      setLoading = _useState3[1];

  var _useState4 = useState(props.onCheckItem ? false : undefined),
      checkAll = _useState4[0],
      setCheckAll = _useState4[1];

  var _useState5 = useState(props.onCheckItem ? [] : undefined),
      checkList = _useState5[0],
      setCheckList = _useState5[1];

  var totalPage = Math.ceil((totalDataCount || datalist.length) / pageLength);
  var startIndex = pageLength * (pageIndex - 1);
  var endIndex = startIndex + pageLength - 1;

  if (endIndex >= datalist.length) {
    endIndex = datalist.length - 1;
  }

  var showList = datalist.length ? datalist.slice(startIndex, endIndex + 1) : null;
  useEffect(function () {
    if (loading) {
      setLoading(false);
    }

    if (showList) {
      if (!showList[0]) {
        setLoading(true);
      }

      if (props.readMoreData) {
        var limit, offset;

        if (showList.length === 0) {
          offset = startIndex;
          limit = pageLength;
        } else if (showList.length < pageLength) {
          offset = startIndex + showList.length;
          limit = pageLength - showList.length + pageLength;
        }

        if (offset && limit) {
          setLoading(true);
          props.readMoreData(limit, offset);
        }
      }
    }
  }, [showList]);
  useEffect(function () {
    if (props.currentPage) {
      setPageIndex(props.currentPage);
    }
  }, [props.currentPage]);
  useEffect(function () {
    if (props.checkItems) {
      setCheckList(props.checkItems);
    }
  }, [props.checkItems]);
  useEffect(function () {
    if (checkList === null || checkList === void 0 ? void 0 : checkList.length) {
      var check = true;

      for (var i = startIndex; i <= endIndex; i++) {
        check = check && !!checkList[i];
      }

      setCheckAll(check);
    } else {
      setCheckAll(false);
    }

    if (!props.checkItems && props.onCheckItem) {
      props.onCheckItem(checkList);
    }
  }, [checkList]);
  useEffect(function () {
    if (checkList === null || checkList === void 0 ? void 0 : checkList.length) {
      var check = true;

      for (var i = startIndex; i <= endIndex; i++) {
        check = check && !!checkList[i];
      }

      setCheckAll(check);
    } else {
      setCheckAll(false);
    }

    if (!props.currentPage && props.onChangePage) {
      props.onChangePage(pageIndex);
    }
  }, [pageIndex]);
  useEffect(function () {
    if (pageIndex > totalPage) {
      if (props.currentPage && props.onChangePage) {
        props.onChangePage(1);
      } else {
        setPageIndex(1);
      }
    }
  }, [pageIndex, totalPage]);

  var handlePageClick = function handlePageClick(data) {
    var page = data.selected + 1;

    if (props.currentPage && props.onChangePage) {
      props.onChangePage(page);
    } else {
      setPageIndex(page);
    }
  };

  var onClickRow = function onClickRow(item) {
    if (props.onClickItem) {
      props.onClickItem(item);
    }

    if (selected.id === item.id) {
      setSelected({});
    } else {
      setSelected(item);
    }
  };

  var onCheckAll = function onCheckAll(checkAll) {
    if (showList) {
      for (var i = startIndex; i <= endIndex; i++) {
        checkList[i] = !!checkAll;
      }

      if (props.checkItems && props.onCheckItem) {
        props.onCheckItem([].concat(checkList));
      } else {
        setCheckList([].concat(checkList));
      }
    }
  };

  var isScrollY = !!props.infiniteScrollHeight;
  var theme = props.theme || CMTheme;
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/React.createElement(Table, {
    className: props.className
  }, /*#__PURE__*/React.createElement(CMTableHeader, _extends({}, props, {
    onCheckAll: checkList ? onCheckAll : null,
    checkAll: checkAll
  })), /*#__PURE__*/React.createElement(TableBody, {
    className: isScrollY ? classes.infiniteTableBody : '',
    style: {
      maxHeight: isScrollY ? props.infiniteScrollHeight : 'default'
    }
  }, !showList ? /*#__PURE__*/React.createElement(TableRow, {
    className: classes.itemRow
  }, /*#__PURE__*/React.createElement(TableCell, {
    colSpan: headers ? headers.length : 1
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.emptyTable
  }, props.emptyImage || /*#__PURE__*/React.createElement(EmptyTableImage, null), /*#__PURE__*/React.createElement(Typography, {
    variant: "h6"
  }, props.emptyMessage || 'Oops! Something wrong happen.')))) : showList.map(function (item, index) {
    if (!item) {
      return null;
    }

    if (props.renderRow) {
      return props.renderRow(item);
    }

    if (props.renderCells) {
      return /*#__PURE__*/React.createElement(TableRow, {
        className: classes.itemRow,
        key: item.id || index,
        onClick: function onClick() {
          onClickRow(item);
        },
        style: {
          cursor: props.onClickItem ? 'pointer' : 'default',
          backgroundColor: checkList && checkList[startIndex + index] ? theme.palette.action.selected : theme.palette.background.paper
        }
      }, checkList ? /*#__PURE__*/React.createElement(TableCell, {
        key: "check-box",
        className: classes.checkboxCell
      }, /*#__PURE__*/React.createElement(CMCheckbox, {
        theme: props.theme,
        option: {
          checked: checkList[startIndex + index]
        },
        onChange: function onChange(e) {
          e.stopPropagation();
          checkList[startIndex + index] = !checkList[startIndex + index];

          if (props.checkItems && props.onCheckItem) {
            props.onCheckItem([].concat(checkList));
          } else {
            setCheckList([].concat(checkList));
          }
        }
      })) : null, props.renderCells(item));
    }
  }))), loading ? /*#__PURE__*/React.createElement("div", {
    className: classes.loading
  }, /*#__PURE__*/React.createElement(CircularProgress, {
    color: "inherit"
  })) : null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "flex-end"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    sm: 12,
    md: 7
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "flex-end"
  }, props.pagination || (totalPage > 1 ? /*#__PURE__*/React.createElement(Paginate, _extends({
    theme: props.theme || CMTheme,
    pageCount: totalPage,
    forcePage: pageIndex - 1,
    onPageChange: handlePageClick
  }, props.paginationProps)) : null)))));
};

CMTable.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** Table headers */
  headers: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    style: PropTypes.object,
    component: PropTypes.node
  })).isRequired,

  /** Source data array. */
  datalist: PropTypes.arrayOf(PropTypes.object).isRequired,

  /** Render function of customized row */
  renderRow: PropTypes.func,

  /** Render function of cells in row */
  renderCells: PropTypes.func,

  /** Fetch function to read more data. */
  readMoreData: PropTypes.func,

  /** How many rows to show in one page. */
  pageLength: PropTypes.number,

  /** The amount of ALL data for rendering pagination and show total page. */
  totalDataCount: PropTypes.number,

  /** Force table to show specified page. */
  currentPage: PropTypes.number,

  /** Force check box of row. */
  checkItems: PropTypes.arrayOf(PropTypes.bool),

  /** Callback function when page changed. */
  onChangePage: PropTypes.func,

  /** Callback function when click row. */
  onClickItem: PropTypes.func,

  /** Callback function when check row. */
  onCheckItem: PropTypes.func,

  /** Sorter for columns */
  sorters: PropTypes.arrayOf(PropTypes.shape({
    onSort: PropTypes.func,
    order: PropTypes.oneOf(['asc', 'desc', ''])
  })),

  /** Filters for columns */
  filters: PropTypes.arrayOf(PropTypes.shape({
    onFilter: PropTypes.func,
    options: PropTypes.arrayOf(PropTypes.shape({
      selected: PropTypes.bool,
      label: PropTypes.string,
      value: PropTypes.any
    }))
  })),

  /** Message when table is empty */
  emptyMessage: PropTypes.string,

  /** Image of empty table */
  emptyImage: PropTypes.node,

  /** Materail UI class from makeStyles for customized style */
  className: PropTypes.string,

  /** Customized pagination*/
  pagination: PropTypes.node,

  /** Props applied to the pagination component */
  paginationProps: PropTypes.object,

  /** Max height of infinite scroll table body */
  infiniteScrollHeight: PropTypes.number
} : {};
CMTable.defaultProps = {
  headers: [],
  datalist: [],
  pageLength: 10,
  currentPage: 1,
  sorters: [],
  filters: []
};
export default CMTable;