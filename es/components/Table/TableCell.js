import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
var StickyTableCell = withStyles(function () {
  return {
    root: {
      top: 0,
      left: 0,
      position: "sticky",
      // position: '-webkit-sticky',
      zIndex: 3,
      padding: 0,
      '& th': {
        border: 'none'
      },
      '& td': {
        border: 'none'
      },
      boxShadow: '2px 0px 4px rgba(0, 0, 0, 0.15)',
      backgroundColor: 'inherit'
    }
  };
})(TableCell);

var CMTableCell = function CMTableCell(props) {
  if (props.sticky) {
    return /*#__PURE__*/React.createElement(StickyTableCell, props);
  }

  return /*#__PURE__*/React.createElement(TableCell, props);
};

export default CMTableCell;