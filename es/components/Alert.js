function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider, withStyles, makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faQuestionCircle, faExclamationCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import Button from './Button';
import CMTheme from '../theme';
import CloseButton from '@material-ui/core/Button';

var CloseIcon = function CloseIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M8.022 6.002l3.032-3.032a.953.953 0 0 0 0-1.348l-.673-.674a.953.953 0 0 0-1.348 0L6 3.981 2.969.947a.953.953 0 0 0-1.348 0l-.674.673a.953.953 0 0 0 0 1.349L3.98 6.002.946 9.034a.953.953 0 0 0 0 1.348l.673.674a.953.953 0 0 0 1.348 0l3.034-3.033 3.032 3.033a.953.953 0 0 0 1.348 0l.673-.674a.954.954 0 0 0 0-1.348L8.022 6.002z",
    fill: "#000910"
  }));
};

CloseIcon.defaultProps = {
  width: "12",
  height: "12",
  viewBox: "0 0 12 12",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
import Box from '@material-ui/core/Box';
var useStyles = makeStyles(function (theme) {
  return {
    root: {
      '& .MuiDialog-paper': {
        padding: 8
      }
    },
    successTitle: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      '& svg': {
        fontSize: 25,
        marginRight: theme.spacing(3),
        color: theme.palette.success.main
      }
    },
    errorTitle: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      '& svg': {
        fontSize: 25,
        marginRight: theme.spacing(3),
        color: theme.palette.error.main
      }
    },
    closeButton: {
      backgroundColor: '#F2F2F2',
      maxWidth: 30,
      maxHeight: 30,
      minWidth: 30,
      minHeight: 30
    },
    dialogHead: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  };
});

var DialogCloseButton = function DialogCloseButton(props) {
  var onClose = props.onClose;
  var classes = useStyles();
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(CloseButton, {
    className: classes.closeButton,
    onClick: onClose
  }, /*#__PURE__*/React.createElement(CloseIcon, null)));
};

var DialogTitle = function DialogTitle(props) {
  var classes = useStyles();

  var type = props.type,
      children = props.children,
      other = _objectWithoutPropertiesLoose(props, ["type", "children"]);

  var getIcon = function getIcon(type) {
    switch (type) {
      case 'error':
        return /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: faTimesCircle,
          className: "icon"
        });

      case 'warning':
        return /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: faExclamationCircle,
          className: "icon"
        });

      case 'question':
        return /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: faQuestionCircle,
          className: "icon"
        });

      case 'success':
      default:
        return /*#__PURE__*/React.createElement(FontAwesomeIcon, {
          icon: faCheckCircle,
          className: "icon"
        });
    }
  };

  return /*#__PURE__*/React.createElement(MuiDialogTitle, _extends({
    disableTypography: true,
    className: type === 'success' ? classes.successTitle : classes.errorTitle
  }, other), getIcon(type), /*#__PURE__*/React.createElement(Typography, {
    variant: "h4"
  }, children));
};

var DialogContentText = withStyles(function (theme) {
  return {
    root: {
      fontSize: '15px',
      fontWeight: 'normal',
      lineHeight: 'normal',
      letterSpacing: 'normal',
      color: theme.palette.text.primary,
      marginBottom: 0,
      marginLeft: theme.spacing(8)
    }
  };
})(MuiDialogContentText);

var Alert = function Alert(props) {
  var theme = props.theme;
  var classes = useStyles();
  var type = props.type,
      title = props.title,
      content = props.content,
      show = props.show,
      loading = props.loading,
      confirmText = props.confirmText,
      cancelText = props.cancelText,
      onConfirm = props.onConfirm,
      onClose = props.onClose,
      onCancel = props.onCancel;

  var handleClose = function handleClose() {
    onClose && onClose();
  };

  var handleConfirm = function handleConfirm() {
    onConfirm && onConfirm();
  };

  var handleCancel = function handleCancel() {
    onCancel && onCancel();
  };

  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: theme || CMTheme
  }, /*#__PURE__*/React.createElement(Dialog, {
    onClose: handleClose,
    open: show,
    className: classes.root
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.dialogHead
  }, /*#__PURE__*/React.createElement(DialogTitle, {
    id: "customized-dialog-title",
    onClose: handleClose,
    type: type
  }, title), onClose && /*#__PURE__*/React.createElement(DialogCloseButton, {
    onClose: handleClose
  })), /*#__PURE__*/React.createElement(MuiDialogContent, null, /*#__PURE__*/React.createElement(DialogContentText, null, content)), /*#__PURE__*/React.createElement(MuiDialogActions, null, onCancel && /*#__PURE__*/React.createElement(Button, {
    theme: theme || CMTheme,
    variant: "outlined",
    disabled: loading,
    onClick: handleCancel,
    label: cancelText
  }), onConfirm && /*#__PURE__*/React.createElement(Button, {
    theme: theme || CMTheme,
    variant: "contained",
    disabled: loading,
    onClick: handleConfirm,
    label: confirmText,
    loading: loading
  }))));
};

Alert.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** Define title icon. */
  type: PropTypes.oneOf(['success', 'error', 'warning', 'question']).isRequired,

  /** Title text. */
  title: PropTypes.string,

  /** Content text. */
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  /** Confirm button label. */
  confirmText: PropTypes.string,

  /** Cancel button label. */
  cancelText: PropTypes.string,

  /** Show alert. */
  show: PropTypes.bool,

  /** Disabled and show loading icon on confirm button. */
  loading: PropTypes.bool,

  /** Callback function when click confirm button. */
  onConfirm: PropTypes.func,

  /** Callback function when click close button (x icon). */
  onClose: PropTypes.func,

  /** Callback function when click cancel button. */
  onCancel: PropTypes.func
} : {};
Alert.defaultProps = {
  type: 'success',
  title: '',
  content: '',
  confirmText: 'OK',
  cancelText: 'Close',
  show: false,
  loading: false,
  onConfirm: null,
  onClose: null,
  onCancel: null
};
export default Alert;