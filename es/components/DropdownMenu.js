function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import Select, { components } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import React, { useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import CMTheme from '../theme';
var useStyles = makeStyles(function (theme) {
  return {
    inputLabel: {
      transition: 'none',
      color: theme.palette.text.primary,
      fontSize: 18
    },
    arrowDown: {
      width: 0,
      height: 0,
      borderLeft: '4px solid transparent',
      borderRight: '4px solid transparent',
      borderTop: "8.2px solid",
      borderTopColor: theme.palette.text.primary
    },
    disabledArrowDown: {
      width: 0,
      height: 0,
      borderLeft: '4px solid transparent',
      borderRight: '4px solid transparent',
      borderTop: "8.2px solid",
      borderTopColor: theme.palette.action.disabled
    }
  };
});

var DropdownIndicator = function DropdownIndicator(props) {
  var classes = useStyles();
  return /*#__PURE__*/React.createElement(components.DropdownIndicator, props, /*#__PURE__*/React.createElement("div", {
    className: props.isDisabled ? classes.disabledArrowDown : classes.arrowDown
  }));
};

var makeInlineStyle = function makeInlineStyle(theme, invalid) {
  return {
    container: function container(base, state) {
      return _extends({}, base, {
        pointerEvents: 'auto',
        cursor: state.isDisabled ? 'not-allowed' : 'auto'
      });
    },
    control: function control(base, state) {
      return {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 2,
        outline: 0,
        border: (state.isFocused || invalid) && '1.4px solid' || '1px solid',
        borderColor: state.isFocused && theme.palette.primary.main || state.isDisabled && theme.palette.action.disabled || invalid && theme.palette.error.main || theme.palette.action.active,
        position: 'relative',
        // boxSizing: 'border-box',
        '&:hover': {
          border: '1.4px solid',
          borderColor: theme.palette.primary.main
        }
      };
    },
    valueContainer: function valueContainer(base, state) {
      return _extends({}, base, {
        padding: '6px 10px'
      });
    },
    indicatorSeparator: function indicatorSeparator() {
      return null;
    },
    indicatorsContainer: function indicatorsContainer(base, state) {
      return _extends({}, base, {
        paddingRight: 10
      });
    },
    placeholder: function placeholder(base) {
      return _extends({}, base, {
        color: theme.palette.text.placeholder
      });
    },
    input: function input(base) {
      return _extends({}, base, {
        color: theme.palette.text.primary,
        margin: 0,
        paddingBottom: 0,
        paddingTop: 0
      });
    },
    menu: function menu(base) {
      return _extends({}, base, {
        borderLeft: "1.4px solid " + theme.palette.primary.main,
        borderRight: "1.4px solid " + theme.palette.primary.main,
        borderBottom: "1.4px solid " + theme.palette.primary.main,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2,
        zIndex: 99,
        marginTop: -3,
        boxShadow: 'none',
        backgroundColor: theme.palette.background.paper
      });
    },
    menuList: function menuList(base) {
      return _extends({}, base, {
        marginTop: 2,
        marginBottom: 3,
        padding: 0
      });
    },
    option: function option(base, state) {
      return {
        // ...base,
        borderTop: '1px solid',
        borderTopColor: theme.palette.divider,
        padding: '8px 15px',
        backgroundColor: state.isFocused && theme.palette.action.hover || state.isSelected && theme.palette.primary.light || theme.palette.background.paper,
        color: state.isDisabled ? theme.palette.text.placeholder : theme.palette.text.primary,
        cursor: state.isDisabled ? 'not-allowed' : 'default'
      };
    },
    singleValue: function singleValue(base, state) {
      return _extends({}, base, {
        color: state.isDisabled ? theme.palette.text.placeholder : theme.palette.text.primary
      });
    },
    multiValue: function multiValue(base) {
      return _extends({}, base, {
        borderRadius: 5,
        overflow: 'hidden',
        marginLeft: '5px',
        '&:first-of-type': {
          marginLeft: '8px'
        }
      });
    },
    multiValueLabel: function multiValueLabel(base) {
      return _extends({}, base, {
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        fontSize: 12,
        padding: '1.5px 10px',
        paddingLeft: 10,
        backgroundColor: theme.palette.primary.main
      });
    },
    multiValueRemove: function multiValueRemove(base) {
      return _extends({}, base, {
        borderRadius: 0,
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.dark,
        cursor: 'pointer',
        padding: 0,
        '& svg': {
          width: 16,
          height: 16
        },
        ':hover': _extends({}, base, {
          padding: 0,
          color: theme.palette.error.light
        })
      });
    }
  };
};

var DropdownMenu = function DropdownMenu(props) {
  var theme = props.theme || CMTheme;
  var classes = useStyles();

  var label = props.label,
      required = props.required,
      menuItems = props.menuItems,
      defaultValue = props.defaultValue,
      disabled = props.disabled,
      multiple = props.multiple,
      invalid = props.invalid,
      creatable = props.creatable,
      onReachEnd = props.onReachEnd,
      onClickItem = props.onClickItem,
      components = props.components,
      others = _objectWithoutPropertiesLoose(props, ["label", "required", "menuItems", "defaultValue", "disabled", "multiple", "invalid", "creatable", "onReachEnd", "onClickItem", "components"]);

  var customStyles = useMemo(function () {
    return makeInlineStyle(theme, invalid);
  }, [theme, invalid]);
  var options = useMemo(function () {
    return menuItems.map(function (item) {
      if (item.value && item.label) {
        return item;
      }

      return {
        value: item,
        label: item
      };
    });
  }, [menuItems]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, label ? /*#__PURE__*/React.createElement(InputLabel, {
    shrink: true,
    required: required,
    className: classes.inputLabel
  }, label) : null, creatable ? /*#__PURE__*/React.createElement(CreatableSelect, _extends({
    placeholder: "Select or enter new value...",
    options: options,
    styles: customStyles,
    defaultValue: defaultValue,
    isDisabled: disabled,
    isMulti: multiple,
    components: _extends({
      DropdownIndicator: DropdownIndicator
    }, components),
    onChange: function onChange(items) {
      if (onClickItem) {
        onClickItem(items || []);
      }
    },
    onMenuScrollToBottom: function onMenuScrollToBottom() {
      if (onReachEnd) {
        onReachEnd();
      }
    }
  }, others)) : /*#__PURE__*/React.createElement(Select, _extends({
    options: options,
    styles: customStyles,
    defaultValue: defaultValue,
    isDisabled: disabled,
    isMulti: multiple,
    components: _extends({
      DropdownIndicator: DropdownIndicator
    }, components),
    onChange: function onChange(items) {
      if (onClickItem) {
        onClickItem(items || []);
      }
    },
    onMenuScrollToBottom: function onMenuScrollToBottom() {
      if (onReachEnd) {
        onReachEnd();
      }
    },
    isClearable: false
  }, others)));
};

DropdownMenu.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** Label text. */
  label: PropTypes.string,

  /** Set true if want to show required indicator in label */
  required: PropTypes.bool,

  /** Set options in menu */
  menuItems: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.string
  })),

  /** Show default value. */
  defaultValue: PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.string
  }),

  /** Set true if allowed select multiple options. */
  multiple: PropTypes.bool,

  /** Set true if disabled. */
  disabled: PropTypes.bool,

  /** Set true if invalid. */
  invalid: PropTypes.bool,

  /** Set true if allow to create new option. */
  creatable: PropTypes.bool,

  /** Callback function when click option in menu. */
  onClickItem: PropTypes.func,

  /** Callback function when a menu reach the end. */
  onReachEnd: PropTypes.func,

  /** Customized component. detailed in https://react-select.com/components#components*/
  components: PropTypes.object
} : {};
DropdownMenu.defaultProps = {
  menuItems: [],
  defaultValue: null,
  disabled: false,
  multiple: false,
  invalid: false,
  creatable: false,
  onReachEnd: null,
  onClickItem: null,
  components: {}
};
export default DropdownMenu;