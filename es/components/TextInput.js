function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React from 'react';
import { ThemeProvider, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import CMTheme from '../theme';
var useStyles = makeStyles(function (theme) {
  var _hover;

  return {
    inputLabel: {
      transition: 'none',
      color: theme.palette.text.primary,
      fontSize: 18
    },
    inputRoot: {
      fontSize: 15,
      border: '1px solid',
      borderColor: theme.palette.action.active,
      '&:hover': {
        border: '1.4px solid',
        borderColor: theme.palette.primary.main
      },
      '&:focus': {
        border: '1.4px solid',
        borderColor: theme.palette.primary.main
      },
      '&.Mui-error': {
        border: '1.4px solid',
        borderColor: theme.palette.error.main,
        '& input': {},
        '& textarea': {}
      },
      '&.Mui-disabled': {
        backgroundColor: theme.palette.background.paper,
        cursor: 'not-allowed',
        '&:hover': (_hover = {
          border: '1px solid'
        }, _hover["border"] = theme.palette.action.disabled, _hover)
      },
      '&.MuiInputBase-multiline': {
        padding: 0
      }
    },
    inputRootWithLabel: {
      marginTop: '18px !important'
    },
    input: {
      padding: '8px 10px',
      backgroundColor: theme.palette.background.paper,
      '&::placeholder': {
        /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: theme.palette.text.placeholder,
        opacity: 1
        /* Firefox */

      },
      '&:-ms-input-placeholder': {
        /* Internet Explorer 10-11 */
        color: theme.palette.text.placeholder
      },
      '&::-ms-input-placeholder': {
        /* Microsoft Edge */
        color: theme.palette.text.placeholder
      }
    }
  };
});

var CMTextInput = function CMTextInput(props) {
  var _clsx;

  var classes = useStyles();

  var children = props.children,
      InputLabelProps = props.InputLabelProps,
      InputProps = props.InputProps,
      others = _objectWithoutPropertiesLoose(props, ["children", "InputLabelProps", "InputProps"]);

  var customInputLabelProps = _extends({
    shrink: true,
    classes: {
      shrink: classes.inputLabel
    }
  }, InputLabelProps);

  var customInputProps = _extends({
    disableUnderline: true,
    classes: {
      root: clsx(classes.inputRoot, (_clsx = {}, _clsx[classes.inputRootWithLabel] = !!props.label, _clsx)),
      input: classes.input
    }
  }, props.InputProps); // const customSelectProps = {
  //     classes: {
  //         select: classes.select,
  //         icon: classes.selectIcon,
  //         selectMenu: classes.selectMenu
  //     }
  // }


  return /*#__PURE__*/React.createElement(TextField, _extends({
    variant: "standard",
    InputLabelProps: customInputLabelProps,
    InputProps: customInputProps // SelectProps={customSelectProps}

  }, others), children);
};

var TextInput = function TextInput(props) {
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: props.theme || CMTheme
  }, /*#__PURE__*/React.createElement(CMTextInput, props));
};

TextInput.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Props for input element */
  inputProps: PropTypes.any,

  /** Label text. */
  label: PropTypes.string,

  /** Field value. */
  value: PropTypes.any,

  /** Placeholder text. */
  placeholder: PropTypes.string,

  /** Helper text of input. */
  helperText: PropTypes.string,

  /** Disabled. */
  disabled: PropTypes.bool,

  /** Text area. */
  multiline: PropTypes.bool,

  /** Input with error. */
  error: PropTypes.bool
} : {};
export default TextInput;