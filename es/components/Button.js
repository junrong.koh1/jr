function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import { ThemeProvider, makeStyles, withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import MuiButton from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import CMTheme from '../theme';
var ButtonBase = withStyles(function (theme) {
  return {
    root: {
      borderRadius: 5,
      minWidth: 120,
      minHeight: 36,
      textTransform: 'none'
    },
    contained: {
      boxShadow: 'none',
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      '&.Mui-disabled': {
        backgroundColor: theme.palette.action.disabled,
        color: theme.palette.text.disabled,
        cursor: 'not-allowed',
        pointerEvents: 'initial',
        '&:hover': {
          boxShadow: 'none'
        }
      },
      '&:hover': {
        backgroundColor: theme.palette.primary.main,
        boxShadow: '2px 4px 6px',
        color: theme.palette.primary.light
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText
      }
    },
    containedSecondary: {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText,
      '&:hover': {
        backgroundColor: theme.palette.secondary.main,
        boxShadow: '2px 4px 6px rgba(14, 124, 244, 0.3)'
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.secondary.contrastText
      }
    },
    outlined: {
      boxShadow: 'none',
      backgroundColor: 'transparent',
      border: '1px solid',
      borderColor: theme.palette.primary.main,
      color: theme.palette.primary.main,
      '&.Mui-disabled': {
        color: theme.palette.text.disabled,
        cursor: 'not-allowed',
        pointerEvents: 'initial',
        '&:active': {
          color: theme.palette.text.disabled
        },
        '&:hover': {
          boxShadow: 'none'
        }
      },
      '&:hover': {
        boxShadow: '2px 4px 6px',
        backgroundColor: 'transparent'
      },
      '&:active': {
        boxShadow: 'none',
        color: theme.palette.primary.main,
        backgroundColor: 'tranparent'
      }
    },
    outlinedSecondary: {
      borderColor: theme.palette.secondary.main,
      color: theme.palette.secondary.main,
      '&:active': {
        boxShadow: 'none',
        color: theme.palette.secondary.main,
        backgroundColor: 'tranparent'
      }
    }
  };
})(MuiButton);
var useStyles = makeStyles(function () {
  return {
    inProgress: {
      zIndex: 100,
      position: 'absolute'
    }
  };
});

var CMButton = function CMButton(props) {
  var classes = useStyles();
  var loading = props.loading;

  var newProps = _extends({}, props, {
    loading: loading ? loading.toString() : 'false'
  });

  return /*#__PURE__*/React.createElement(ButtonBase, _extends({
    disableRipple: props.variant !== 'contained'
  }, newProps), loading ? /*#__PURE__*/React.createElement(CircularProgress, {
    className: classes.inProgress,
    color: "inherit",
    size: 15
  }) : props.label || props.children);
};

var Button = function Button(props) {
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: props.theme || CMTheme
  }, /*#__PURE__*/React.createElement(CMButton, props));
};

Button.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Button container style. */
  variant: PropTypes.oneOf(['text', 'outlined', 'contained']),

  /** Label on button. */
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  /** Show loading icon after label. */
  loading: PropTypes.bool,

  /** Disable button. */
  disabled: PropTypes.bool,

  /** Callback function when click this button. */
  onClick: PropTypes.func
} : {};
Button.defaultProps = {
  className: '',
  variant: 'contained',
  label: '',
  loading: false,
  disabled: false,
  onClick: null
};
export default Button;