import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider, withStyles } from '@material-ui/core/styles';
import MuiCheckbox from '@material-ui/core/Checkbox';

var CheckboxIcon = function CheckboxIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("rect", {
    x: ".5",
    y: ".5",
    width: "17",
    height: "17",
    rx: "1.5",
    fill: "#fff",
    stroke: "#CECECE"
  }));
};

CheckboxIcon.defaultProps = {
  width: "18",
  height: "18",
  viewBox: "0 0 18 18",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
import CMTheme from '../../theme';
var StyledCheckbox = withStyles(function (theme) {
  return {
    root: {
      '&:hover': {
        backgroundColor: 'transparent',
        '& rect': {
          stroke: theme.palette.primary.main
        }
      }
    },
    checked: {
      '& span': {
        color: theme.palette.primary.main
      }
    },
    disabled: {
      '& span': {
        color: theme.palette.action.disabled,
        '&.MuiIconButton-label': {
          pointerEvents: 'initial',
          cursor: 'not-allowed',
          '&:hover': {
            '& rect': {
              stroke: theme.palette.action.disabled
            }
          }
        }
      }
    }
  };
})(MuiCheckbox);

var CMCheckbox = function CMCheckbox(props) {
  var option = props.option,
      index = props.index;

  if (!option) {
    return null;
  }

  return /*#__PURE__*/React.createElement(StyledCheckbox, {
    className: props.className,
    icon: /*#__PURE__*/React.createElement(CheckboxIcon, {
      style: {
        margin: 3
      }
    }),
    disableRipple: true,
    onChange: props.onChange,
    onClick: function onClick(e) {
      e.stopPropagation();
    },
    checked: !!option.checked,
    disabled: option.disabled,
    name: option.name,
    id: index.toString()
  });
};

var Checkbox = function Checkbox(props) {
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: props.theme || CMTheme
  }, /*#__PURE__*/React.createElement(CMCheckbox, props));
};

Checkbox.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Callback function when select box. */
  onChange: PropTypes.func,
  option: PropTypes.shape({
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    name: PropTypes.any
  }),
  index: PropTypes.number
} : {};
Checkbox.defaultProps = {
  className: '',
  onChange: null,
  option: null,
  index: 0
};
export default Checkbox;