function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from './index';
import CMTheme from '../../theme';

var CheckboxGroup = function CheckboxGroup(props) {
  var options = props.options,
      horizontal = props.horizontal;
  var theme = props.theme || CMTheme;

  var handleChange = function handleChange(event) {
    var id = event.target.id;
    var newOptions = [].concat(options);
    newOptions[id] = _extends({}, newOptions[id], {
      checked: !newOptions[id].checked
    });

    if (props.onChange) {
      props.onChange([].concat(newOptions));
    }
  };

  var renderOptions = useCallback(function () {
    return options.map(function (option, index) {
      return /*#__PURE__*/React.createElement(FormControlLabel, {
        key: index,
        control: /*#__PURE__*/React.createElement(Checkbox, {
          theme: theme,
          option: option,
          index: index,
          onChange: handleChange
        }),
        label: option.label
      });
    });
  }, [options, theme]);
  return /*#__PURE__*/React.createElement(FormControl, {
    component: "fieldset"
  }, /*#__PURE__*/React.createElement(FormGroup, null, horizontal ? /*#__PURE__*/React.createElement("div", null, renderOptions()) : renderOptions()));
};

CheckboxGroup.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Callback function when select box. */
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    checked: PropTypes.bool,
    label: PropTypes.string
  })),
  horizontal: PropTypes.bool
} : {};
CheckboxGroup.defaultProps = {
  className: '',
  onChange: null,
  options: [],
  horizontal: false
};
export default CheckboxGroup;