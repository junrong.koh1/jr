function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider, withStyles } from '@material-ui/core/styles';
import MuiRadio from '@material-ui/core/Radio';
import CMTheme from '../../theme';
var StyledRadio = withStyles(function (theme) {
  return {
    root: {
      '&:hover': {
        backgroundColor: 'transparent',
        '& span': {
          color: theme.palette.primary.main
        }
      }
    },
    checked: {
      '& span': {
        color: theme.palette.primary.main
      }
    },
    disabled: {
      '& span': {
        color: theme.palette.action.disabled
      }
    }
  };
})(MuiRadio);

var CMRadio = function CMRadio(props) {
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: props.theme || CMTheme
  }, /*#__PURE__*/React.createElement(StyledRadio, _extends({
    disableRipple: true
  }, props)));
};

CMRadio.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Callback function when select box. */
  onChange: PropTypes.func,
  option: PropTypes.shape({
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    name: PropTypes.any
  }),
  index: PropTypes.number
} : {};
CMRadio.defaultProps = {
  className: '',
  onChange: null,
  option: null,
  index: 0
};
export default CMRadio;