import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import CMRadio from './index';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CMTheme from '../../theme';

var CMRadioGroup = function CMRadioGroup(props) {
  var options = props.options,
      horizontal = props.horizontal,
      defaultValue = props.defaultValue,
      disabled = props.disabled;

  var _useState = useState(defaultValue),
      value = _useState[0],
      setValue = _useState[1];

  var theme = props.theme || CMTheme;

  var handleChange = function handleChange(event) {
    var _event$target;

    var result = event === null || event === void 0 ? void 0 : (_event$target = event.target) === null || _event$target === void 0 ? void 0 : _event$target.value;
    setValue(result);

    if (props.onChange) {
      props.onChange(result);
    }
  };

  var renderOptions = useCallback(function () {
    return options.map(function (option, index) {
      return /*#__PURE__*/React.createElement(FormControlLabel, {
        key: index,
        value: option.value,
        label: option.label,
        disabled: disabled,
        control: /*#__PURE__*/React.createElement(CMRadio, {
          theme: theme
        })
      });
    });
  }, [options, disabled, theme]);
  return /*#__PURE__*/React.createElement(FormControl, {
    component: "fieldset"
  }, /*#__PURE__*/React.createElement(RadioGroup, {
    value: value,
    onChange: handleChange
  }, horizontal ? /*#__PURE__*/React.createElement("div", null, renderOptions()) : renderOptions()));
};

CMRadioGroup.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** ClassName can be used for @material-ui/styles to customise style. */
  className: PropTypes.string,

  /** Callback function when select box. */
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.any
  })),

  /** Disabled this group. */
  disabled: PropTypes.bool,
  horizontal: PropTypes.bool,

  /** Default checked option. */
  defaultValue: PropTypes.any
} : {};
CMRadioGroup.defaultProps = {
  className: '',
  onChange: null,
  options: [],
  horizontal: false,
  defaultValue: null
};
export default CMRadioGroup;