import Paginate from 'react-paginate';
import React from 'react';
import { ThemeProvider, makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

var AngleLeftIcon = function AngleLeftIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M.186 4.642L5.614.154c.248-.205.64-.205.867 0 .248.204.248.529 0 .716L1.486 5l4.995 4.13c.248.204.248.529 0 .716a.7.7 0 0 1-.434.154.7.7 0 0 1-.433-.154L.186 5.358c-.248-.204-.248-.512 0-.716z",
    fill: "#000910"
  }));
};

AngleLeftIcon.defaultProps = {
  width: "7",
  height: "10",
  viewBox: "0 0 7 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var AngleRightIcon = function AngleRightIcon(props) {
  return /*#__PURE__*/React.createElement("svg", props, /*#__PURE__*/React.createElement("path", {
    d: "M6.48 5.358L1.054 9.846c-.248.205-.64.205-.867 0-.248-.204-.248-.529 0-.716L5.18 5 .186.87C-.062.666-.062.341.186.154A.7.7 0 0 1 .619 0a.7.7 0 0 1 .434.154L6.48 4.642c.248.204.248.512 0 .716z",
    fill: "#000910"
  }));
};

AngleRightIcon.defaultProps = {
  width: "7",
  height: "10",
  viewBox: "0 0 7 10",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};
import DropdownMenu from './DropdownMenu';
import CMTheme from '../theme';
import clsx from 'clsx';
var useStyles = makeStyles(function (theme) {
  return {
    container: {
      display: 'flex',
      listStyle: 'none',
      borderRadius: 5,
      // marginBottom: 0,
      paddingLeft: 0
    },
    page: {
      width: 36,
      height: 36,
      fontSize: '0.9375rem',
      border: 'solid 1px',
      borderColor: theme.palette.action.active,
      marginLeft: -1,
      boxSizing: 'border-box',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      outline: 'none',
      cursor: 'pointer',
      '&:hover': {
        border: '1px solid',
        borderColor: theme.palette.primary.main
      }
    },
    pageLink: {
      outline: 'none',
      color: theme.palette.text.primary,
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      textDecoration: 'none',
      '&:hover': {
        color: theme.palette.primary.main
      }
    },
    activePage: {
      backgroundColor: theme.palette.primary.main,
      '& $pageLink': {
        color: theme.palette.background.paper
      }
    },
    previous: {
      width: 36,
      height: 36,
      boxSizing: 'border-box',
      outline: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 10,
      border: 'solid 1px',
      borderColor: theme.palette.action.active,
      '& svg path': {
        fill: theme.palette.action.active
      },
      borderTopLeftRadius: 5,
      borderBottomLeftRadius: 5,
      '&:hover': {
        borderColor: theme.palette.primary.main,
        '& svg path': {
          fill: theme.palette.primary.main
        }
      }
    },
    previousLink: {
      '&:focus': {
        outline: 'none'
      }
    },
    next: {
      width: 36,
      height: 36,
      boxSizing: 'border-box',
      outline: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: 10,
      border: 'solid 1px',
      borderColor: theme.palette.action.active,
      '& svg path': {
        fill: theme.palette.action.active
      },
      borderTopRightRadius: 5,
      borderBottomRightRadius: 5,
      '&:hover': {
        borderColor: theme.palette.primary.main,
        '& svg path': {
          fill: theme.palette.primary.main
        }
      }
    },
    nextLink: {
      '&:focus': {
        outline: 'none'
      }
    },
    disabled: {
      pointerEvents: 'none',
      '& svg path': {
        fill: theme.palette.action.disabled
      }
    }
  };
});

var CMPageing = function CMPageing(props) {
  var _clsx, _clsx2, _clsx3, _clsx4, _clsx5, _clsx6;

  var forcePage = props.forcePage,
      pageCount = props.pageCount,
      onPageChange = props.onPageChange,
      rowsPerPage = props.rowsPerPage,
      onRowsPerPageChange = props.onRowsPerPageChange,
      previousLabel = props.previousLabel,
      nextLabel = props.nextLabel,
      elementClasses = props.elementClasses;
  var classes = useStyles();
  var rowPerPageOptions = rowsPerPage ? rowsPerPage.map(function (rows) {
    return {
      label: rows + " Rows/Page",
      value: rows
    };
  }) : null;
  return /*#__PURE__*/React.createElement("div", {
    style: {
      display: 'flex',
      alignItems: 'center'
    }
  }, rowPerPageOptions ? /*#__PURE__*/React.createElement("div", {
    style: {
      marginRight: 10,
      minWidth: 180
    }
  }, /*#__PURE__*/React.createElement(DropdownMenu, {
    theme: props.theme || CMTheme,
    menuItems: rowPerPageOptions,
    defaultValue: rowPerPageOptions[0],
    onClickItem: function onClickItem(item) {
      onRowsPerPageChange(item.value);
    }
  })) : null, /*#__PURE__*/React.createElement(Paginate, {
    containerClassName: classes.container,
    pageClassName: clsx(classes.page, (_clsx = {}, _clsx[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.page] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.page), _clsx)),
    pageLinkClassName: clsx(classes.pageLink, (_clsx2 = {}, _clsx2[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.pageLink] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.pageLink), _clsx2)),
    activeClassName: clsx(classes.activePage, (_clsx3 = {}, _clsx3[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.activePage] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.activePage), _clsx3)),
    previousClassName: clsx(classes.previous, (_clsx4 = {}, _clsx4[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.previous] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.previous), _clsx4)),
    previousLinkClassName: classes.previousLink,
    nextClassName: clsx(classes.next, (_clsx5 = {}, _clsx5[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.next] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.next), _clsx5)),
    nextLinkClassName: classes.nextLink,
    disabledClassName: clsx(classes.disabled, (_clsx6 = {}, _clsx6[elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.disabled] = Boolean(elementClasses === null || elementClasses === void 0 ? void 0 : elementClasses.disabled), _clsx6)),
    previousLabel: previousLabel || /*#__PURE__*/React.createElement(AngleLeftIcon, null),
    nextLabel: nextLabel || /*#__PURE__*/React.createElement(AngleRightIcon, null),
    pageCount: pageCount,
    onPageChange: onPageChange,
    forcePage: forcePage
  }));
};

var Pageing = function Pageing(props) {
  return /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: props.theme || CMTheme
  }, /*#__PURE__*/React.createElement(CMPageing, props));
};

Pageing.propTypes = process.env.NODE_ENV !== "production" ? {
  /** Inject theme object of createMuiTheme. */
  theme: PropTypes.object,

  /** The total number of pages. */
  pageCount: PropTypes.number.isRequired,

  /** Set current page forcely */
  forcePage: PropTypes.number,

  /** Select for rows per page. */
  rowsPerPage: PropTypes.arrayOf(PropTypes.number),

  /** Callback function when a page is clicked. */
  onPageChange: PropTypes.func,

  /** Callback function when a rows per page is changed. */
  onRowsPerPageChange: PropTypes.func,

  /** Icon used as previous button label */
  previousLabel: PropTypes.element,

  /** Icon used as next button label */
  nextLabel: PropTypes.element,

  /** Override or extend the styles applied to the component. */
  elementClasses: PropTypes.shape({
    page: PropTypes.string,
    pageLink: PropTypes.string,
    activePage: PropTypes.string,
    previous: PropTypes.string,
    next: PropTypes.string,
    disabled: PropTypes.string
  })
} : {};
export default Pageing;